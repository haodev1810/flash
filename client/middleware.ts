import { isValidStoryRedirectUrl } from '@client/utils/auth';
import { JwtCookieToken, LoginQueryParam } from '@libs/constants/auth';
import { NextResponse } from 'next/server';
import type { NextRequest } from 'next/server';

// This function can be marked `async` if using `await` inside
export function middleware(request: NextRequest) {
  const response = NextResponse.next();
  if (
    ['/500', '/405', '/404', '/401', '/400', '/403'].includes(
      request.nextUrl.pathname
    )
  ) {
    return response;
  }
  if (request.nextUrl.pathname.startsWith('/login')) {
    if (request.cookies.has(JwtCookieToken)) {
      const clientId = new URL(request.url).searchParams.get(
        LoginQueryParam.clientId
      );
      if (!clientId) {
        let redirectUrl: string | null =
          request.nextUrl.searchParams.get(LoginQueryParam.redirectUrl) ?? null;
        if (!isValidStoryRedirectUrl(redirectUrl)) {
          redirectUrl = null;
        }
        if (
          redirectUrl === process.env.NEXT_PUBLIC_BASE_URL ||
          redirectUrl === `${process.env.NEXT_PUBLIC_BASE_URL}/`
        ) {
          redirectUrl = null;
        }
        return NextResponse.redirect(new URL(redirectUrl ?? '/', request.url), {
          status: 303,
        });
      }
    }
    return response;
  }
  if (request.cookies.has(JwtCookieToken)) {
    return response;
  } else {
    let redirectUrl: string | null = request.url ?? null;
    if (!isValidStoryRedirectUrl(redirectUrl)) {
      redirectUrl = null;
    }
    if (
      redirectUrl === process.env.NEXT_PUBLIC_BASE_URL ||
      redirectUrl === `${process.env.NEXT_PUBLIC_BASE_URL}/`
    ) {
      redirectUrl = null;
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const queryParams: Record<string, any> = {};
    if (redirectUrl) {
      queryParams[LoginQueryParam.redirectUrl] = redirectUrl;
    }
    const queryString = new URLSearchParams(queryParams);
    return NextResponse.redirect(
      new URL(`/login${queryString ? `?${queryString}` : ''}`, request.url)
    );
  }
}

// See "Matching Paths" below to learn more
export const config = {
  matcher: [
    '/collections/me/:path*',
    '/login',
    '/collections/create',
    '/collections/:path*/edit',
  ],
};
