import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getListItems } from '@client/apis/hooks/items/useGetListItem';
import { LoginPage } from '@client/components/pages/login/LoginPage';
import { ItemsApiListsRequest } from '@libs/openapi-generator/generated';
import {
  HydrationBoundary,
  QueryClient,
  dehydrate,
} from '@tanstack/react-query';

export default async function Index() {
  const queryClient = new QueryClient();
  const initQuery: ItemsApiListsRequest = {
    image: 'medium',
    limit: 20,
    page: Math.ceil(Math.random() * 10),
  };
  await queryClient.prefetchQuery({
    queryKey: [QueryKeys.ListItems, initQuery],
    queryFn: () => getListItems(initQuery),
  });
  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <LoginPage initQuery={initQuery} />
    </HydrationBoundary>
  );
}
