import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getItem } from '@client/apis/hooks/items/useGetItemQuery';
import { getListItems } from '@client/apis/hooks/items/useGetListItem';
import { AppLayout } from '@client/components/layouts/AppLayout';
import { Photo } from '@client/components/pages/photos';
import { ItemsApiListsRequest } from '@libs/openapi-generator/generated';
import {
  HydrationBoundary,
  QueryClient,
  dehydrate,
} from '@tanstack/react-query';
import { Metadata, ResolvingMetadata } from 'next';

interface Props {
  params: { slug: string };
}

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const slug = params.slug;
  const queryclient = new QueryClient();

  // fetch data
  const item = await queryclient.fetchQuery({
    queryKey: [QueryKeys.GetItem, { slug }],
    queryFn: () => getItem({ slug }),
  });
  // optionally access and extend (rather than replace) parent metadata
  const previousImages = (await parent).openGraph?.images || [];

  return {
    title: item.data.title,
    description: item.data.description || item.data.title,
    openGraph: {
      images: [
        item.data.images?.medium?.path as string,
        item.data.images?.large?.path as string,
        item.data.images?.small?.path as string,
        ...previousImages,
      ],
    },
  };
}

export default async function Index({ params }: Props) {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery({
    queryKey: [QueryKeys.GetItem, { slug: params.slug }],
    queryFn: () => getItem({ slug: params.slug }),
  });
  const initQueryItems: ItemsApiListsRequest = {
    page: 1,
    limit: 30,
    image: 'medium',
    similar: params.slug,
  };
  await queryClient.prefetchInfiniteQuery({
    queryKey: [QueryKeys.GetItemSimilar],
    queryFn: ({ pageParam }) =>
      getListItems({ ...initQueryItems, page: Number(pageParam) }),
    initialPageParam: 1,
  });
  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <AppLayout custom={false}>
        <Photo slug={params.slug} initQueryItems={initQueryItems} />
      </AppLayout>
    </HydrationBoundary>
  );
}
