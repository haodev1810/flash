import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getProfileAuthor } from '@client/apis/hooks/authors/useGetProfileAuthor';
import { getListItems } from '@client/apis/hooks/items/useGetListItem';
import { AppLayout } from '@client/components/layouts/AppLayout';
import { AuthorPage } from '@client/components/pages/authors/AuthorPage';
import { ItemsApiListsRequest } from '@libs/openapi-generator/generated';
import {
  HydrationBoundary,
  QueryClient,
  dehydrate,
} from '@tanstack/react-query';
import { Metadata, ResolvingMetadata } from 'next';

interface Props {
  params: { username: string };
}

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  const username = params.username;
  const queryclient = new QueryClient();
  const author = await queryclient.fetchQuery({
    queryKey: [QueryKeys.GetProfileAuthor, { username }],
    queryFn: () => getProfileAuthor({ username }),
  });
  const previousImages = (await parent).openGraph?.images || [];

  return {
    title: author.data.name,
    description: author.data.name,
    openGraph: {
      images: [author.data.avatar_large?.path as string, ...previousImages],
    },
  };
}

export default async function Index({ params }: Props) {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery({
    queryKey: [QueryKeys.GetProfileAuthor, { username: params.username }],
    queryFn: () => getProfileAuthor({ username: params.username }),
  });
  const initQueryItems: ItemsApiListsRequest = {
    page: 1,
    limit: 30,
    image: 'medium',
    author: params.username,
  };
  await queryClient.prefetchInfiniteQuery({
    queryKey: [QueryKeys.GetItemSimilar],
    queryFn: ({ pageParam }) =>
      getListItems({ ...initQueryItems, page: Number(pageParam) }),
    initialPageParam: 1,
  });
  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <AppLayout custom={false}>
        <AuthorPage
          initQueryItems={initQueryItems}
          username={params.username}
        />
      </AppLayout>
    </HydrationBoundary>
  );
}
