import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getItem } from '@client/apis/hooks/items/useGetItemQuery';
import { getListItems } from '@client/apis/hooks/items/useGetListItem';
import { getTags } from '@client/apis/hooks/tags';
import { AppLayout } from '@client/components/layouts/AppLayout';
import { SearchPage } from '@client/components/pages/searchs/SearchPage';
import { ItemsApiListsRequest } from '@libs/openapi-generator/generated';
import {
  HydrationBoundary,
  QueryClient,
  dehydrate,
} from '@tanstack/react-query';
import { Metadata, ResolvingMetadata } from 'next';

interface Props {
  params: { query: string };
}

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const query = params.query;
  const queryclient = new QueryClient();

  // fetch data
  const items = await queryclient.fetchQuery({
    queryKey: [QueryKeys.GetItemsSearch, {}],
    queryFn: () =>
      getListItems({ image: 'medium', limit: 30, query: query, page: 1 }),
  });
  // optionally access and extend (rather than replace) parent metadata
  const previousImages = (await parent).openGraph?.images || [];
  const listImages = items.data.map((i) => i.images?.medium?.path || '');
  return {
    title: 'Animals Image Search',
    description: 'Animals Image Search',
    openGraph: {
      images: [...listImages, ...previousImages],
    },
  };
}

export default async function Index({ params }: Props) {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery({
    queryKey: [QueryKeys.GetItem, { slug: params.query }],
    queryFn: () => getItem({ slug: params.query }),
  });
  const initQueryItems: ItemsApiListsRequest = {
    page: 1,
    limit: 30,
    image: 'medium',
    query: params.query,
  };
  await queryClient.prefetchQuery({
    queryKey: [QueryKeys.GetTags, { query: params.query, page: 1, limit: 20 }],
    queryFn: () => getTags({ query: params.query, page: 1, limit: 20 }),
  });
  await queryClient.prefetchInfiniteQuery({
    queryKey: [QueryKeys.GetItemSimilar],
    queryFn: ({ pageParam }) =>
      getListItems({ ...initQueryItems, page: Number(pageParam) }),
    initialPageParam: 1,
  });
  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <AppLayout custom={false}>
        <SearchPage query={params.query} initQueryItems={initQueryItems} />
      </AppLayout>
    </HydrationBoundary>
  );
}
