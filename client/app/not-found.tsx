'use client';
import React from 'react';
import { Button, Typography, Container, CssBaseline } from '@mui/material';
import Link from 'next/link';

const NotFoundPage = () => {
  return (
    <Container
      component="main"
      maxWidth="xs"
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100vh',
      }}
    >
      <CssBaseline />
      <Typography
        variant="h1"
        sx={{
          marginBottom: 4,
          color: (theme) => theme.palette.error.main,
        }}
      >
        404
      </Typography>
      <Typography variant="h5" sx={{ marginBottom: 2 }}>
        Oops! Page not found.
      </Typography>
      <Typography
        variant="body1"
        align="center"
        paragraph
        sx={{ marginBottom: 2 }}
      >
        The page you are looking for might be temporarily unavailable or does
        not exist.
      </Typography>
      <Button
        variant="contained"
        color="primary"
        component={Link}
        href="/"
        size="large"
        sx={{ marginTop: 2 }}
      >
        Go to Home
      </Button>
    </Container>
  );
};

export default NotFoundPage;
