import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getLeaderboard } from '@client/apis/hooks/authors/useGetLeaderboardAuthor';
import { AppLayout } from '@client/components/layouts/AppLayout';
import { LeaderboardPage } from '@client/components/pages/leaderboard/LeaderboardPage';
import { RecentPage } from '@client/components/pages/leaderboard/RecentPage';
import { AuthorsApiListLeaderboardRequest } from '@libs/openapi-generator/generated';
import {
  HydrationBoundary,
  QueryClient,
  dehydrate,
} from '@tanstack/react-query';

export default async function Index() {
  const queryClient = new QueryClient();
  const initParamsQuery: AuthorsApiListLeaderboardRequest = {
    limit: 10,
    page: 1,
    type: 'recent',
  };
  await queryClient.prefetchInfiniteQuery({
    queryKey: [QueryKeys.GetCollectionPublic],
    queryFn: ({ pageParam }) =>
      getLeaderboard({ ...initParamsQuery, page: Number(pageParam) }),
    initialPageParam: 1,
  });
  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <AppLayout custom={false}>
        <LeaderboardPage>
          <RecentPage initParamsQuery={initParamsQuery} />
        </LeaderboardPage>
      </AppLayout>
    </HydrationBoundary>
  );
}
