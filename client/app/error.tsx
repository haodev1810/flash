'use client'; // Error components must be Client Components
import React from 'react';
import { Button, Typography, Container } from '@mui/material';
import { useEffect } from 'react';
import Link from 'next/link';

export default function Error({
  error,
  reset,
}: {
  error: Error & { digest?: string };
  reset: () => void;
}) {
  useEffect(() => {
    // Log the error to an error reporting service
    console.error(error);
  }, [error]);

  return (
    <Container>
      <Typography variant="h1" color="error" align="center" gutterBottom>
        Oops!
      </Typography>
      <Typography variant="h4" align="center" paragraph>
        Something went wrong.
      </Typography>
      <Typography variant="body1" align="center" paragraph>
        The page you are looking for might be temporarily unavailable or does
        not exist.
      </Typography>
      <Button
        variant="contained"
        color="primary"
        component={Link}
        href="/"
        size="large"
      >
        Go to Home
      </Button>
    </Container>
  );
}
