import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getListItemVideo } from '@client/apis/hooks/videos/useGetListItemVideo';
import { AppLayout } from '@client/components/layouts/AppLayout';
import { VideosPage } from '@client/components/pages/videos/VideosPage';
import { VideosApiListRequest } from '@libs/openapi-generator/generated';
import {
  HydrationBoundary,
  QueryClient,
  dehydrate,
} from '@tanstack/react-query';

export default async function Index() {
  const queryClient = new QueryClient();
  const initQueryVideo: VideosApiListRequest = {
    page: 1,
    limit: 15,
  };
  await queryClient.prefetchInfiniteQuery({
    queryKey: [QueryKeys.GetListItemVideo],
    queryFn: ({ pageParam }) =>
      getListItemVideo({ ...initQueryVideo, page: Number(pageParam) }),
    initialPageParam: 1,
  });
  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <AppLayout custom={true}>
        <VideosPage initQueryVideo={initQueryVideo} />
      </AppLayout>
    </HydrationBoundary>
  );
}
