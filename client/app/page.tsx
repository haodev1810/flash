import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getListItems } from '@client/apis/hooks/items/useGetListItem';
import { AppLayout } from '@client/components/layouts/AppLayout';
import { Home } from '@client/components/pages/Home';
import { ItemsApiListsRequest } from '@libs/openapi-generator/generated';
import {
  HydrationBoundary,
  QueryClient,
  dehydrate,
} from '@tanstack/react-query';

export default async function Index() {
  const queryClient = new QueryClient();
  const initQueryItems: ItemsApiListsRequest = {
    page: 1,
    limit: 30,
    image: 'medium',
  };
  await queryClient.prefetchInfiniteQuery({
    queryKey: [QueryKeys.ListItems],
    queryFn: ({ pageParam }) =>
      getListItems({ ...initQueryItems, page: Number(pageParam) }),
    initialPageParam: 1,
  });
  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <AppLayout custom={true}>
        <Home initQueryItems={initQueryItems} />
      </AppLayout>
    </HydrationBoundary>
  );
}
