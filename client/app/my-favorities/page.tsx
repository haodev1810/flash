import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getListItems } from '@client/apis/hooks/items/useGetListItem';
import { AppLayout } from '@client/components/layouts/AppLayout';
import { MyFavorities } from '@client/components/pages/favorities/MyFavorities';
import { ItemsApiListsRequest } from '@libs/openapi-generator/generated';
import {
  HydrationBoundary,
  QueryClient,
  dehydrate,
} from '@tanstack/react-query';

export default async function Index() {
  const queryClient = new QueryClient();
  const initQueryItems: ItemsApiListsRequest = {
    page: 1,
    limit: 30,
    image: 'medium',
  };
  await queryClient.prefetchInfiniteQuery({
    queryKey: [QueryKeys.ListItems],
    queryFn: ({ pageParam }) =>
      getListItems({ ...initQueryItems, page: Number(pageParam) }),
    initialPageParam: 1,
  });
  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <AppLayout custom={false}>
        <MyFavorities />
      </AppLayout>
    </HydrationBoundary>
  );
}
