import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getListMyCollection } from '@client/apis/hooks/collections/useGetListMyCollection';
import { AppLayout } from '@client/components/layouts/AppLayout';
import { ListMyCollectionPage } from '@client/components/pages/collections/ListMyCollectionPage';
import { CollectionsApiListRequest } from '@libs/openapi-generator/generated';
import {
  HydrationBoundary,
  QueryClient,
  dehydrate,
} from '@tanstack/react-query';

export default async function Index() {
  const queryClient = new QueryClient();
  const initParamsQuery: CollectionsApiListRequest = {
    image: 'medium',
    page: 1,
    limit: 30,
  };
  await queryClient.prefetchInfiniteQuery({
    queryKey: [QueryKeys.GetMyCollection],
    queryFn: ({ pageParam }) =>
      getListMyCollection({ ...initParamsQuery, page: Number(pageParam) }),
    initialPageParam: 1,
  });
  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <AppLayout custom={false}>
        <ListMyCollectionPage initParamsQuery={initParamsQuery} />
      </AppLayout>
    </HydrationBoundary>
  );
}
