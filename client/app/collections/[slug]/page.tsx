import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getCollectionDetail } from '@client/apis/hooks/collections/useGetCollectionDetail';
import { getListItemCollection } from '@client/apis/hooks/collections/useGetListItemCollection';
import { AppLayout } from '@client/components/layouts/AppLayout';
import { CollectionDetail } from '@client/components/pages/collections/CollectionDetail';
import { CollectionsApiListImagesCollectionRequest } from '@libs/openapi-generator/generated';
import {
  HydrationBoundary,
  QueryClient,
  dehydrate,
} from '@tanstack/react-query';
import { Metadata, ResolvingMetadata } from 'next';

interface Props {
  params: { slug: string };
}

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const slug = params.slug;
  const queryclient = new QueryClient();

  // fetch data
  const collection = (
    await queryclient.fetchQuery({
      queryKey: [QueryKeys.GetCollectionDetail, { slug }],
      queryFn: () => getCollectionDetail({ slug }),
    })
  ).data;
  // optionally access and extend (rather than replace) parent metadata
  const previousImages = (await parent).openGraph?.images || [];

  return {
    title: collection.title,
    description: collection.description || collection.title,
    openGraph: {
      images: [
        collection.main?.images?.large?.path as string,
        collection.main?.images?.medium?.path as string,
        collection.main?.images?.small?.path as string,
        collection.sub?.images?.large?.path as string,
        collection.sub?.images?.medium?.path as string,
        collection.sub?.images?.small?.path as string,
        collection.sub2?.images?.large?.path as string,
        collection.sub2?.images?.medium?.path as string,
        collection.sub2?.images?.small?.path as string,
        ...previousImages,
      ],
    },
  };
}

export default async function Index({ params }: Props) {
  const queryClient = new QueryClient();
  const initQueryItems: CollectionsApiListImagesCollectionRequest = {
    page: 1,
    limit: 30,
    image: 'medium',
    slug: params.slug,
    sort: 'DESC',
  };
  await queryClient.prefetchQuery({
    queryKey: [QueryKeys.GetCollectionDetail, { slug: params.slug }],
    queryFn: () => getCollectionDetail({ slug: params.slug }),
  });
  await queryClient.prefetchInfiniteQuery({
    queryKey: [QueryKeys.GetListItemCollection],
    queryFn: ({ pageParam }) =>
      getListItemCollection({ ...initQueryItems, page: Number(pageParam) }),
    initialPageParam: 1,
  });
  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <AppLayout custom={false}>
        <CollectionDetail slug={params.slug} initQueryItems={initQueryItems} />
      </AppLayout>
    </HydrationBoundary>
  );
}
