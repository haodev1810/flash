import { FormCreateCollection } from '@client/components/forms/FormCreateCollection';
import { AppLayout } from '@client/components/layouts/AppLayout';
import {
  HydrationBoundary,
  QueryClient,
  dehydrate,
} from '@tanstack/react-query';

export default async function Index() {
  const queryClient = new QueryClient();
  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <AppLayout custom={false}>
        <FormCreateCollection />
      </AppLayout>
    </HydrationBoundary>
  );
}
