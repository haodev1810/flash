import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getListCollectionPublic } from '@client/apis/hooks/collections/useGetListCollectionPublic';
import { AppLayout } from '@client/components/layouts/AppLayout';
import { ListCollectionPage } from '@client/components/pages/collections/ListCollectionPage';
import { CollectionsApiListRequest } from '@libs/openapi-generator/generated';
import {
  HydrationBoundary,
  QueryClient,
  dehydrate,
} from '@tanstack/react-query';

export default async function Index() {
  const queryClient = new QueryClient();
  const initParamsQuery: CollectionsApiListRequest = {
    image: 'medium',
    page: 1,
    limit: 30,
  };
  await queryClient.prefetchInfiniteQuery({
    queryKey: [QueryKeys.GetCollectionPublic],
    queryFn: ({ pageParam }) =>
      getListCollectionPublic({ ...initParamsQuery, page: Number(pageParam) }),
    initialPageParam: 1,
  });
  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <AppLayout custom={false}>
        <ListCollectionPage initParamsQuery={initParamsQuery} />
      </AppLayout>
    </HydrationBoundary>
  );
}
