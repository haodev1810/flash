import { InternalSecretHeader } from '@libs/constants/auth';
import { AxiosRequestConfig } from 'axios';

export const initOptions: AxiosRequestConfig = {
  headers: {
    [InternalSecretHeader]: process.env.NEXT_PUBLIC_INTERNAL_SECRET as string,
  },
};
