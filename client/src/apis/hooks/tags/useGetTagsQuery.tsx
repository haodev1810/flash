import { createApiFactory } from '@client/libs/functions';
import { TagsApi, TagsApiListRequest } from '@libs/openapi-generator/generated';
import { useQuery } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const getTags = async (requestParameters: TagsApiListRequest) => {
  return (await createApiFactory(TagsApi).list(requestParameters)).data;
};

export const useGetTagsQuery = (requestParameters: TagsApiListRequest) => {
  return useQuery({
    queryKey: [QueryKeys.GetTags, requestParameters],
    queryFn: () => getTags(requestParameters),
  });
};
