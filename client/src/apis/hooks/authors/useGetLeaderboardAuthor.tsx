import { createApiFactory } from '@client/libs/functions';
import {
  AuthorsApi,
  AuthorsApiListLeaderboardRequest,
} from '@libs/openapi-generator/generated';
import { useQuery } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const getLeaderboard = async (
  requestParameters: AuthorsApiListLeaderboardRequest
) => {
  return (await createApiFactory(AuthorsApi).listLeaderboard(requestParameters))
    .data;
};

export const useGeLeaderboard = (
  requestParameters: AuthorsApiListLeaderboardRequest
) => {
  return useQuery({
    queryKey: [QueryKeys.GetLeaderboard, requestParameters],
    queryFn: () => getLeaderboard(requestParameters),
  });
};
