import { createApiFactory } from '@client/libs/functions';
import {
  AuthorsApi,
  AuthorsApiProfileRequest,
} from '@libs/openapi-generator/generated';
import { useQuery } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const getProfileAuthor = async (
  requestParameters: AuthorsApiProfileRequest
) => {
  return (await createApiFactory(AuthorsApi).profile(requestParameters)).data;
};

export const useGetProfileAuthor = (
  requestParameters: AuthorsApiProfileRequest
) => {
  return useQuery({
    queryKey: [QueryKeys.GetProfileAuthor, requestParameters],
    queryFn: () => getProfileAuthor(requestParameters),
  });
};
