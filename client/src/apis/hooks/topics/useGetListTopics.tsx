import { createApiFactory } from '@client/libs/functions';
import {
  TopicsApi,
  TopicsApiListRequest,
} from '@libs/openapi-generator/generated';
import { useQuery } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const getListTopics = async (
  requestParameters: TopicsApiListRequest
) => {
  return (await createApiFactory(TopicsApi).list(requestParameters)).data;
};

export const useGetListTopics = (requestParameters: TopicsApiListRequest) => {
  return useQuery({
    queryKey: [QueryKeys.GetListTopic],
    queryFn: () => getListTopics(requestParameters),
  });
};
