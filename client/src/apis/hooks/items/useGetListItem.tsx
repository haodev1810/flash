import { createApiFactory } from '@client/libs/functions';
import {
  ItemsApi,
  ItemsApiListsRequest,
} from '@libs/openapi-generator/generated';
import { useQuery } from '@tanstack/react-query';

export const getListItems = async (requestParameters: ItemsApiListsRequest) => {
  return (await createApiFactory(ItemsApi).lists(requestParameters)).data;
};

export const useGetListItems = (requestParameters: ItemsApiListsRequest) => {
  return useQuery({
    queryKey: ['list-items', requestParameters],
    queryFn: () => getListItems(requestParameters),
  });
};
