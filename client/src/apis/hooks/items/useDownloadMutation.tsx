import { createApiFactory } from '@client/libs/functions';
import {
  ItemsApi,
  ItemsApiDownloadRequest,
} from '@libs/openapi-generator/generated';
import { useMutation } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';
import { initOptions } from '@client/apis/_init';
import { AxiosRequestConfig } from 'axios';

export const downloadItem = async (
  requestParameters: ItemsApiDownloadRequest,
  options?: AxiosRequestConfig
) => {
  return (
    await createApiFactory(ItemsApi).download(
      requestParameters,
      options || initOptions
    )
  ).data;
};

export const useDownloadItemMutaion = () => {
  return useMutation({
    mutationFn: downloadItem,
    mutationKey: [QueryKeys.DownloadItem],
  });
};
