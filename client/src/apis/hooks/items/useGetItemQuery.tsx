import { createApiFactory } from '@client/libs/functions';
import {
  ItemsApi,
  ItemsApiFindOneRequest,
} from '@libs/openapi-generator/generated';
import { useQuery } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const getItem = async (requestParameters: ItemsApiFindOneRequest) => {
  return (await createApiFactory(ItemsApi).findOne(requestParameters)).data;
};

export const useGetItem = (requestParameters: ItemsApiFindOneRequest) => {
  return useQuery({
    queryKey: [QueryKeys.GetItem, requestParameters],
    queryFn: () => getItem(requestParameters),
  });
};
