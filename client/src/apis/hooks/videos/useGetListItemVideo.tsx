import { createApiFactory } from '@client/libs/functions';
import {
  VideosApi,
  VideosApiListRequest,
} from '@libs/openapi-generator/generated';
import { useQuery } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const getListItemVideo = async (
  requestParameters: VideosApiListRequest
) => {
  return (await createApiFactory(VideosApi).list(requestParameters)).data;
};

export const useGetListItemVideo = (
  requestParameters: VideosApiListRequest
) => {
  return useQuery({
    queryKey: [QueryKeys.GetListItemVideo, requestParameters],
    queryFn: () => getListItemVideo(requestParameters),
  });
};
