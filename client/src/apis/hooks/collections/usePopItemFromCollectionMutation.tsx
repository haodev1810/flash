import { createApiFactory } from '@client/libs/functions';
import {
  CollectionsApi,
  CollectionsApiPopRequest,
} from '@libs/openapi-generator/generated';
import { useMutation } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const popItemFromCollection = async (
  requestParameters: CollectionsApiPopRequest
) => {
  return (await createApiFactory(CollectionsApi).pop(requestParameters)).data;
};

export const usePopItemFromCollectionMutation = () => {
  return useMutation({
    mutationKey: [QueryKeys.PopItemFromCollection],
    mutationFn: popItemFromCollection,
  });
};
