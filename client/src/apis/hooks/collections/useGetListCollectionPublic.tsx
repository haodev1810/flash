import { createApiFactory } from '@client/libs/functions';
import {
  CollectionsApi,
  CollectionsApiListRequest,
} from '@libs/openapi-generator/generated';
import { useQuery } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const getListCollectionPublic = async (
  requestParameters: CollectionsApiListRequest
) => {
  return (await createApiFactory(CollectionsApi).list(requestParameters)).data;
};

export const useGetListCollectionPublic = (
  requestParameters: CollectionsApiListRequest
) => {
  return useQuery({
    queryKey: [QueryKeys.GetCollectionPublic],
    queryFn: () => getListCollectionPublic(requestParameters),
  });
};
