import { createApiFactory } from '@client/libs/functions';
import {
  CollectionsApi,
  CollectionsApiDownloadRequest,
} from '@libs/openapi-generator/generated';
import { useMutation } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const downloadCollection = async (
  requestParameters: CollectionsApiDownloadRequest
) => {
  return (await createApiFactory(CollectionsApi).download(requestParameters))
    .data;
};

export const useDownloadCollectionMutation = () => {
  return useMutation({
    mutationKey: [QueryKeys.DownloadCollection],
    mutationFn: downloadCollection,
  });
};
