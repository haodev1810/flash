import { createApiFactory } from '@client/libs/functions';
import {
  CollectionsApi,
  CollectionsApiFindOneRequest,
} from '@libs/openapi-generator/generated';
import { useQuery } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const getCollectionDetail = async (
  requestParameters: CollectionsApiFindOneRequest
) => {
  return (await createApiFactory(CollectionsApi).findOne(requestParameters))
    .data;
};

export const useGetCollectionDetail = (
  requestParameters: CollectionsApiFindOneRequest
) => {
  return useQuery({
    queryKey: [QueryKeys.GetCollectionDetail, requestParameters],
    queryFn: () => getCollectionDetail(requestParameters),
  });
};
