import { createApiFactory } from '@client/libs/functions';
import {
  CollectionsApi,
  CollectionsApiCreateRequest,
} from '@libs/openapi-generator/generated';
import { useMutation } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const createCollection = async (
  requestParameters: CollectionsApiCreateRequest
) => {
  return createApiFactory(CollectionsApi).create(requestParameters);
};

export const useCreateCollectionMutation = () => {
  return useMutation({
    mutationKey: [QueryKeys.CreateCollection],
    mutationFn: createCollection,
  });
};
