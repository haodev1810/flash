import { createApiFactory } from '@client/libs/functions';
import {
  CollectionsApi,
  CollectionsApiPushRequest,
} from '@libs/openapi-generator/generated';
import { useMutation } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const pushItemToCollection = async (
  requestParameters: CollectionsApiPushRequest
) => {
  return (await createApiFactory(CollectionsApi).push(requestParameters)).data;
};

export const usePushItemToCollectionMutation = () => {
  return useMutation({
    mutationKey: [QueryKeys.PushItemToCollection],
    mutationFn: pushItemToCollection,
  });
};
