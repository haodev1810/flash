import { createApiFactory } from '@client/libs/functions';
import {
  CollectionsApi,
  CollectionsApiUpdateRequest,
} from '@libs/openapi-generator/generated';
import { useMutation } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const updateCollection = async (
  requestParameters: CollectionsApiUpdateRequest
) => {
  return createApiFactory(CollectionsApi).update(requestParameters);
};

export const useUpdateCollectionMutation = () => {
  return useMutation({
    mutationKey: [QueryKeys.UpdateCollection],
    mutationFn: updateCollection,
  });
};
