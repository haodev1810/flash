import { createApiFactory } from '@client/libs/functions';
import {
  CollectionsApi,
  CollectionsApiListImagesCollectionRequest,
} from '@libs/openapi-generator/generated';
import { useQuery } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const getListItemCollection = async (
  requestParameters: CollectionsApiListImagesCollectionRequest
) => {
  return (
    await createApiFactory(CollectionsApi).listImagesCollection(
      requestParameters
    )
  ).data;
};

export const useGetListItemCollection = (
  requestParameters: CollectionsApiListImagesCollectionRequest
) => {
  return useQuery({
    queryKey: [QueryKeys.GetListItemCollection, requestParameters],
    queryFn: () => getListItemCollection(requestParameters),
  });
};
