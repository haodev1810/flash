import { createApiFactory } from '@client/libs/functions';
import {
  CollectionsApi,
  CollectionsApiListRequest,
} from '@libs/openapi-generator/generated';
import { useQuery } from '@tanstack/react-query';
import { QueryKeys } from '../QueryKeys';

export const getListMyCollection = async (
  requestParameters: CollectionsApiListRequest
) => {
  return (
    await createApiFactory(CollectionsApi).myCollection(requestParameters)
  ).data;
};

export const useGetListMyCollection = (
  requestParameters: CollectionsApiListRequest
) => {
  return useQuery({
    queryKey: [QueryKeys.GetMyCollection],
    queryFn: () => getListMyCollection(requestParameters),
  });
};
