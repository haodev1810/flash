import { createApiFactory } from '@client/libs/functions';
import { AccountApi } from '@libs/openapi-generator/generated/api';
import { useQuery } from '@tanstack/react-query';
import { AxiosInstance } from 'axios';
import { QueryKeys } from '../QueryKeys';

export const fetchAccount = (
  axiosInstance: AxiosInstance | undefined | null = null
) => {
  return createApiFactory(AccountApi, axiosInstance)
    .account()
    .then((res) => res.data);
};

export const useAccountQuery = () => {
  return useQuery({
    queryKey: [QueryKeys.GetAccount],
    queryFn: () => fetchAccount(),
  });
};
