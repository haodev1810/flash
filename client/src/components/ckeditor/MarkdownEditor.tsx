'use client';
import { Stack } from '@mui/material';
import dynamic from 'next/dynamic';
import React from 'react';
import { EditorConfig } from '@ckeditor/ckeditor5-core/src/editor/editorconfig';

interface Props {
  onChangeData: (value: string) => void;
  value: string;
  config?: EditorConfig | undefined;
}
const Editor = dynamic(() => import('./Editor'), { ssr: false });
export const MarkdownEditor = (props: Props) => {
  return (
    <Stack width={1} sx={{ fontSize: 14 }}>
      <Editor {...props} config={props.config} />
    </Stack>
  );
};
