/* eslint-disable @typescript-eslint/no-explicit-any */
'use client';
import React, { ComponentProps } from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import Editor from 'ckeditor5-custom-build/build/ckeditor';

// import { FileLoader, UploadAdapter } from '@ckeditor/ckeditor5-upload';

// class AppUploadAdapter implements UploadAdapter {
//   loader: FileLoader;

//   constructor(loader: FileLoader) {
//     // The file loader instance to use during the upload.
//     this.loader = loader;
//   }

//   // Starts the upload process.
//   async upload() {
//     const file = await this.loader.file;
//     // Return a promise that will be resolved when the file is uploaded.
//     if (!file) {
//       throw new Error('File is not exists.');
//     }
//     const uploadedFile = await uploadContentImageMutation({ file });
//     return { default: uploadedFile.data.path };
//   }
// }

// function UploadAdapterPlugin(editor: any) {
//   editor.plugins.get('FileRepository').createUploadAdapter = (
//     loader: FileLoader
//   ) => {
//     return new AppUploadAdapter(loader);
//   };
// }

interface Props extends ComponentProps<any> {
  onChangeData: (data: string) => void;
  value: string;
  inline?: boolean;
}

const Editor5 = ({ onChangeData, value, inline, ...props }: Props) => {
  return (
    <CKEditor
      onReady={(editor: {
        editing: {
          view: {
            change: (arg0: (writer: any) => void) => void;
            document: { getRoot: () => any };
          };
        };
      }) => {
        editor.editing.view.change((writer) => {
          writer.setStyle(
            {
              'min-height': '200px',
            },
            editor.editing.view.document.getRoot()
          );
        });
      }}
      editor={Editor}
      data={value}
      onChange={(event: any, editor: { getData: () => any }) => {
        onChangeData(editor.getData());
      }}
      config={{
        ...props.config,
      }}
      {...props}
    />
  );
};

export default React.memo(Editor5);
