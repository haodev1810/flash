'use client';
import React from 'react';
import { INavbarItem } from './hooks/useNavbarItem';
import { Stack, StackProps } from '@mui/material';
import { NavbarItem } from './NavbarItem';

interface Props extends StackProps {
  items: INavbarItem[];
  onClickLink?: () => void;
}
export const NavbarListItem = ({ items, onClickLink, ...props }: Props) => {
  return (
    <Stack
      flexDirection={'row'}
      alignItems={'center'}
      flexWrap={'wrap'}
      {...props}
    >
      {items.map((item) => (
        <NavbarItem
          flexGrow={1}
          key={`$${item.id}`}
          item={item}
          onClickLink={onClickLink}
        />
      ))}
    </Stack>
  );
};
