import { Stack } from '@mui/material';
import React from 'react';
import { NavbarListItem } from './NavbarListItem';
import { useNavbarItems } from './hooks/useNavbarItem';

export const Navbar = () => {
  const items = useNavbarItems();
  return (
    <Stack
      flexDirection={'row'}
      alignContent={'center'}
      justifyContent={'center'}
    >
      <NavbarListItem items={items} />
    </Stack>
  );
};
