'use client';
import { useIsLogin } from '@client/hooks/useIsLogin';
import { SvgIcon } from '@mui/material';
import { useMemo } from 'react';

export interface INavbarItem {
  id: string;
  link: string;
  title: string;
  permission: boolean;
  icon?: typeof SvgIcon;
  notify?: number;
  subText?: string;
}
export const useNavbarItems = () => {
  const isLogin = useIsLogin();
  return useMemo(() => {
    const items: INavbarItem[] = [
      {
        id: 'home',
        link: '/',
        title: 'Home',
        permission: true,
      },
      {
        id: 'collections',
        link: '/collections',
        title: 'Collections',
        permission: true,
      },
      {
        id: 'my-collections',
        link: '/collections/me',
        title: 'My Collections',
        permission: isLogin,
      },
      {
        id: 'Leaderboard',
        link: '/leaderboard',
        title: 'Leaderboard',
        permission: true,
      },
    ];
    const traverseItems = (items: INavbarItem[]): INavbarItem[] => {
      const canAccessItems = items.filter((item) => item.permission);
      return canAccessItems.map((item) => {
        return {
          ...item,
        };
      });
    };
    return traverseItems(items);
  }, [isLogin]);
};
