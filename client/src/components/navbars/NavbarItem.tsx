import { Stack, StackProps, Typography } from '@mui/material';
import React, { useMemo } from 'react';
import { INavbarItem } from './hooks/useNavbarItem';
import Link from '../ui/link/Link';
import { usePathname } from 'next/navigation';
import clsx from 'clsx';

interface NavbarItemProps extends StackProps {
  item: INavbarItem;
  onClickLink?: () => void;
}
export const NavbarItem = ({
  item,
  onClickLink,
  className,
  ...props
}: NavbarItemProps) => {
  const path = usePathname();
  const isRouting = useMemo(() => {
    const deepSearch = (searchItem: INavbarItem): boolean => {
      if (searchItem.link === path) {
        return true;
      }
      return false;
    };
    return deepSearch(item);
  }, [item, path]);
  return (
    <Stack {...props}>
      <Stack
        className={clsx({ active: isRouting }, className)}
        component={Link}
        href={item.link}
        flexDirection={'row'}
        alignItems={'center'}
        gap={1}
        sx={{
          textDecoration: 'none',
          borderRadius: 6,
          px: 2,
          py: 1,
          color: '#000',
          '&:hover, &.active': {
            bgcolor: '#000',
            '.AppNavbarItemText, .AppNavbarIcon': {
              color: '#FFF',
            },
          },
        }}
      >
        <Typography fontWeight={500} className="AppNavbarItemText">
          {item.title}
        </Typography>
        {item.subText && (
          <Typography fontSize={14} color="#7F7F7F" fontWeight={500}>
            {item.subText}
          </Typography>
        )}
      </Stack>
    </Stack>
  );
};
