import {
  genPathFileBlob,
  // useGenPathFileBlob,
} from '@client/hooks/useGenPathFile';
import { ItemVideoResponse } from '@libs/openapi-generator/generated';
import { IconButton, Stack } from '@mui/material';
import React, { useEffect, useRef, useState } from 'react';
import PlayCircleIcon from '@mui/icons-material/PlayCircle';
import StopCircleIcon from '@mui/icons-material/StopCircle';

interface Props {
  video: ItemVideoResponse;
}

export const Video = ({ video }: Props) => {
  const videoRef = useRef<HTMLVideoElement>(null);
  const [load, setLoad] = useState(false);
  const [play, setPlay] = useState(false);
  const [src, setSrc] = useState<string>();
  const thumbnail = genPathFileBlob(video.video.thumbnail);
  const onClickPlay = async () => {
    if (!play) {
      if (!src) {
        const res = await genPathFileBlob(video.video.file);
        if (res) {
          setLoad(true);
          setSrc(res);
        }
      }else{
        onPlay()
      }
    } else {
      onStop();
    }
  };
  useEffect(() => {
    if (load && src) {
      onPlay();
    }
  }, [load, src]);
  const onPlay = () => {
    if (videoRef.current) {
      setPlay(true);
      videoRef.current.play();
    }
  };
  const onStop = () => {
    if (videoRef.current) {
      setPlay(false);
      videoRef.current.pause();
    }
  };
  useEffect(() => {
    if (play && videoRef.current) {
      videoRef.current.play();
    }
  }, [play]);
  return (
    <Stack>
      {thumbnail && (
        <Stack position={'relative'}>
          <Stack position={'absolute'} zIndex={1}>
            <IconButton onClick={onClickPlay}>
              {!play ? (
                <PlayCircleIcon htmlColor={!play ? '#FFF' : '#0bc40b'} />
              ) : (
                <StopCircleIcon htmlColor="#FFF" />
              )}
            </IconButton>
          </Stack>
          <img
            loading="lazy"
            src={thumbnail}
            style={{
              display: load ? 'none' : 'block',
            }}
          />
          {src && load && (
            <video
              onEnded={onStop}
              ref={videoRef}
              style={{ display: load ? 'block' : 'none' }}
              controls
            >
              <source src={src} type="video/mp4" />
            </video>
          )}
        </Stack>
      )}
    </Stack>
  );
};
