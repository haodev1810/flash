'use client';
import { TagResponse } from '@libs/openapi-generator/generated';
import { Chip } from '@mui/material';
import Link from 'next/link';
import React from 'react';
import slugify from 'slugify';

interface Props {
  tag: TagResponse;
}
export const TagItem = ({ tag }: Props) => {
  return (
    <Link
      href={'/searchs/' + slugify(tag.search_term, { lower: true })}
      title={tag.name}
    >
      <Chip
        variant="outlined"
        label={tag.name}
        sx={{ fontWeight: 600, borderRadius: 2, minWidth: { md: 100 } }}
      />
    </Link>
  );
};
