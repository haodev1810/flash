'use client';
import { Stack } from '@mui/material';
import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import { TagItem } from './TagItem';
import { Pagination } from 'swiper/modules';
import 'swiper/css/navigation';
import { TagResponse } from '@libs/openapi-generator/generated';

interface Props {
  tags: TagResponse[];
}
export const SwiperTag = ({ tags }: Props) => {
  return (
    <Stack width={1} position={'relative'}>
      <Swiper
        slidesPerView={12}
        style={{ paddingLeft: '40px', paddingRight: '40px' }}
        spaceBetween={5}
        pagination={{
          clickable: true,
        }}
        modules={[Pagination]}
        className="mySwiper"
      >
        {tags.map((tag, i) => (
          <SwiperSlide key={i}>
            <TagItem tag={tag} />
          </SwiperSlide>
        ))}
      </Swiper>
    </Stack>
  );
};
