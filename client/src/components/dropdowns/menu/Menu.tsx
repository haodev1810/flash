'use client';
import {
  Badge,
  Button,
  IconButton,
  Stack,
  Typography,
  useMediaQuery,
  useTheme,
} from '@mui/material';
import React from 'react';
import { ActionUser } from '../user';
import { Explore } from '../explore';
import { NotificationDrop } from '../notifications/NotificationDrop';
import FileUploadOutlinedIcon from '@mui/icons-material/FileUploadOutlined';
import { useAppSelector } from '@client/stores';
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import Link from 'next/link';
import { RoundedButton } from '@client/components/buttons/RoundedButton';

interface Props {
  custom?: boolean;
}
export const Menu = ({ custom = false }: Props) => {
  const theme = useTheme();
  const isMatched = useMediaQuery(theme.breakpoints.down('sm'));
  const account = useAppSelector((state) => state.account.account);
  const items = useAppSelector((state) => state.collection.items);
  return (
    <Stack
      width={1}
      flex={1}
      gap={2}
      flexDirection={'row'}
      alignItems={'center'}
      justifyContent={'flex-end'}
    >
      <Stack flexDirection={'row'} alignItems={'center'} gap={2}>
        <Stack
          color={custom ? '#FFF' : undefined}
          display={{ md: 'none', lg: 'flex' }}
        >
          <Explore />
        </Stack>
        <Typography component={Link} href={'/my-favorities'}>
          <Badge badgeContent={items.length} color="primary">
            <BookmarkBorderIcon htmlColor={custom ? '#FFF' : '#000'} />
          </Badge>
        </Typography>
        {account ? (
          <>
            <NotificationDrop />
            <ActionUser />
            {isMatched ? (
              <IconButton sx={{ bgcolor: '#45b2fa' }}>
                <FileUploadOutlinedIcon htmlColor={'#FFF'} />
              </IconButton>
            ) : (
              <Button
                variant="contained"
                sx={{ px: 3, borderRadius: 8 }}
                startIcon={<FileUploadOutlinedIcon htmlColor={'#FFF'} />}
              >
                Upload
              </Button>
            )}
          </>
        ) : (
          <RoundedButton component={Link} href="/login" variant="outlined">
            Login
          </RoundedButton>
        )}
      </Stack>
    </Stack>
  );
};
