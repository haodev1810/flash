'use client';
import { IconButton } from '@mui/material';
import React from 'react';
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';

export const NotificationDrop = () => {
  return (
    <>
      <IconButton>
        <NotificationsNoneOutlinedIcon color="primary" />
      </IconButton>
    </>
  );
};
