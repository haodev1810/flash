'use client';
import { genFilePath } from '@client/utils/file';
import {
  CollectionResponse,
  StoredFileResponse,
} from '@libs/openapi-generator/generated';
import {
  Avatar,
  Container,
  Grid,
  Stack,
  StackProps,
  Typography,
} from '@mui/material';
import React, { useState } from 'react';
import { LikeButton } from '../buttons/LikeButton';
import { CollectionViewMedium } from '../ui/collections/CollectionViewMedium';
import { RoundedButton } from '../buttons/RoundedButton';
import InfoIcon from '@mui/icons-material/Info';
import { MoreInformationCollectionDialog } from '../dialogs/collections/MoreInformationCollectionDialog';
import DownloadForOfflineIcon from '@mui/icons-material/DownloadForOffline';
import { useDownloadCollectionMutation } from '@client/apis/hooks/collections/useDownloadCollectionMutation';
import { useNotify } from '../notifications/hooks';
import JSZip from 'jszip';
import { saveAs } from 'file-saver';
import { downloadFileFromUrl } from '@libs/utils/files';

interface InformationItemProps extends StackProps {
  collection: CollectionResponse;
}
export const InformationCollection = ({
  collection,
  ...props
}: InformationItemProps) => {
  const { notifyError } = useNotify();
  const downloadCollection = useDownloadCollectionMutation();
  const [open, setOpen] = useState<boolean>(false);
  const onDownload = async () => {
    try {
      const response = await downloadCollection
        .mutateAsync({
          downloadCollectionDto: {
            id: collection.id,
          },
        })
        .then((res) => res.data);
      if (response) {
        const zip = new JSZip();
        const images = zip.folder('images');
        if (images) {
          const items = response.items || [];
          await Promise.all(
            items.map(async (item) => {
              const folder = images.folder(item.slug);
              if (folder) {
                const data = await Promise.all([
                  downloadFileFromUrl(
                    genFilePath(item.images?.small as StoredFileResponse)
                  ),
                  downloadFileFromUrl(
                    genFilePath(item.images?.medium as StoredFileResponse)
                  ),
                  downloadFileFromUrl(
                    genFilePath(item.images?.large as StoredFileResponse)
                  ),
                ]);
                await Promise.all([
                  folder.file('small.jpg', data[0], { binary: true }),
                  folder.file('medium.jpg', data[1], { binary: true }),
                  folder.file('large.jpg', data[2], { binary: true }),
                ]);
              }
            })
          );
        }
        zip.generateAsync({ type: 'blob' }).then((content) => {
          saveAs(content, collection.slug);
        });
      }
    } catch (error) {
      notifyError({ error });
    }
  };
  return (
    <Stack width={1} gap={2} {...props}>
      <Container maxWidth={'xl'}>
        <Grid
          container
          boxSizing={'border-box'}
          columnSpacing={2}
          rowSpacing={2}
        >
          <Grid item xs={12} md={3}>
            <Stack width={1} flexDirection={'row'} gap={1}>
              <Avatar
                sx={{ height: 50, width: 50 }}
                src={genFilePath(collection.owner?.avatar)}
              />
              <Stack>
                <Typography
                  variant="h3"
                  fontWeight={500}
                  fontSize={20}
                  color="#2c343e"
                >
                  {collection?.owner?.name}
                </Typography>
                <Typography color={'#7F7F7F'}>Follow</Typography>
              </Stack>
            </Stack>
          </Grid>
          <Grid item xs={12} md={9}>
            <Stack
              width={1}
              flexDirection={'row'}
              gap={2}
              justifyContent={{ sm: 'flex-start', md: 'flex-end' }}
            >
              <LikeButton />
              <RoundedButton
                variant="contained"
                startIcon={<DownloadForOfflineIcon />}
                onClick={onDownload}
              >
                Download Free
              </RoundedButton>
            </Stack>
          </Grid>
        </Grid>
      </Container>
      <Stack alignItems={'center'} justifyContent={'center'}>
        <CollectionViewMedium
          sx={{
            maxWidth: 800,
            borderRadius: 5,
            '&:hover': {
              filter: 'brightness(.8)',
            },
          }}
          rowHeight={200}
          variant="quilted"
          collection={collection}
        />
      </Stack>
      <Container maxWidth={'xl'}>
        <Stack flexDirection={'row'} justifyContent={'flex-end'}>
          <RoundedButton
            onClick={() => {
              setOpen(true);
            }}
            sx={{
              borderColor: '#7f7f7f',
              color: '#7f7f7f',
              ':hover': { color: '#55b0fa' },
            }}
            startIcon={<InfoIcon />}
            variant="outlined"
          >
            More Information
          </RoundedButton>
        </Stack>
      </Container>
      {open && (
        <MoreInformationCollectionDialog
          collection={collection}
          open={open}
          onClose={() => {
            setOpen(false);
          }}
        />
      )}
    </Stack>
  );
};
