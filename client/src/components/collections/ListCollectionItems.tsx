'use client';
import { Box, Button, Stack, Typography } from '@mui/material';
import React, { useEffect, useMemo, useState } from 'react';
import { useScrollToLoadMoreBody } from '@client/hooks/useScrollToLoadMoreBody';
import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import {
  CollectionsApiListImagesCollectionRequest,
  Pagination,
} from '@libs/openapi-generator/generated';
import { useInfiniteQuery } from '@tanstack/react-query';
import { getListItemCollection } from '@client/apis/hooks/collections/useGetListItemCollection';
import { ListViewItems } from '../ui/list-view/ListViewItems';

interface Props {
  initQuery: CollectionsApiListImagesCollectionRequest;
}
export const ListCollectionItems = ({ initQuery }: Props) => {
  const [pagination, setPagination] = useState<Pagination>();
  const { shouldFetchNextPage } = useScrollToLoadMoreBody({
    threshold: 1000,
  });
  const { fetchNextPage, data } = useInfiniteQuery({
    queryKey: [QueryKeys.GetListItemCollection],
    queryFn: ({ pageParam }) =>
      getListItemCollection({ ...initQuery, page: Number(pageParam) }),
    initialPageParam: 1,
    getNextPageParam: (lastPage) => lastPage.pagination.page + 1,
    getPreviousPageParam: (firstPage) => firstPage.pagination.page,
  });
  const items = useMemo(() => {
    if (data?.pages) {
      setPagination(data.pages[0].pagination);
    }
    return data?.pages.map((page) => page.data).flat() || [];
  }, [data?.pages]);

  useEffect(() => {
    if (shouldFetchNextPage) {
      fetchNextPage().catch();
    }
  }, [fetchNextPage, shouldFetchNextPage]);
  return (
    <Stack gap={2}>
      <Stack flexDirection={'row'} alignItems={'center'} gap={2}>
        <Button
          startIcon={'Photos'}
          size="large"
          sx={{
            bgcolor: '#000',
            color: '#FFF',
            textTransform: 'none',
            borderRadius: 16,
            ':hover': {
              bgcolor: '#3a3a3a',
            },
            px: 2,
          }}
        >
          <Typography fontSize={14} color="#7F7F7F">
            {(pagination?.total || 0) / 1000 > 1
              ? ((pagination?.total || 0) / 1000).toFixed(2) + 'k'
              : pagination?.total}
          </Typography>
        </Button>
      </Stack>
      <Box>
        <ListViewItems items={items} column={3} />
      </Box>
    </Stack>
  );
};
