'use client';
import { Grid, Stack, Typography } from '@mui/material';
import React, { useMemo } from 'react';
import { useGetListCollectionPublic } from '@client/apis/hooks/collections/useGetListCollectionPublic';
import { CollectionViewSmall } from '../ui/collections/CollectionViewSmall';
import Link from 'next/link';
import slugify from 'slugify';

export const ListCollectionSmall = () => {
  const { data, error } = useGetListCollectionPublic({
    limit: 5,
    page: 1,
    image: 'small',
  });
  const collections = useMemo(() => {
    return data?.data ?? [];
  }, [data?.data]);
  return (
    <Stack gap={2}>
      {collections.length > 0 && (
        <Stack
          flexDirection={'row'}
          alignItems={'center'}
          justifyContent={'space-between'}
        >
          <Typography fontSize={20} fontWeight={500}>
            Collections
          </Typography>
        </Stack>
      )}
      <Grid container rowSpacing={2} columnSpacing={2} boxSizing={'border-box'}>
        {collections.map((collection, index) => (
          <Grid key={index} item xs={4} sm={4} md={2}>
            <Stack
              component={Link}
              href={'/collections/' + slugify(collection.slug)}
            >
              <CollectionViewSmall
                sx={{ maxWidth: 1, borderRadius: 4 }}
                rowHeight={80}
                collection={collection}
                variant="quilted"
              />
              <Typography
                fontSize={{ xs: 14, md: 16 }}
                textAlign={'center'}
                fontWeight={600}
              >
                {collection.title}
              </Typography>
            </Stack>
          </Grid>
        ))}
      </Grid>
    </Stack>
  );
};
