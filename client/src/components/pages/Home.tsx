'use client';
import { Stack, Typography } from '@mui/material';
import React from 'react';
import { ItemsApiListsRequest } from '@libs/openapi-generator/generated';
import { ListItems } from '../items/ListItem';
import { Navbar } from '../navbars/Navbar';

interface Props {
  initQueryItems: ItemsApiListsRequest;
}
export const Home = ({ initQueryItems }: Props) => {
  return (
    <Stack width={1} gap={2} py={2}>
      <Navbar />
      <Typography fontSize={30} variant="h1" fontWeight={600}>
        Free Stock Photos
      </Typography>
      <ListItems initQuery={initQueryItems} />
    </Stack>
  );
};
