'use client';
import { useGetItem } from '@client/apis/hooks/items/useGetItemQuery';
import { InformationItem } from '@client/components/items/InformationItem';
import { Stack, Typography } from '@mui/material';
import NotFoundPage from 'client/app/not-found';
import React, { useMemo } from 'react';
import Error from 'client/app/error';
import { ListSimilarItem } from '@client/components/items/ListSimilarItem';
import { SwiperPagination } from '@client/components/ui/swiper/SwiperPagination';
import { SwiperSlide } from 'swiper/react';
import { TagItem } from '@client/components/topics/TagItem';
import { ItemsApiListsRequest } from '@libs/openapi-generator/generated';
import { QueryKeys } from '@client/apis/hooks/QueryKeys';

interface Props {
  slug: string;
  initQueryItems: ItemsApiListsRequest;
}
export const Photo = ({ slug, initQueryItems }: Props) => {
  const { data, error } = useGetItem({ slug });
  const item = useMemo(() => {
    return data?.data;
  }, [data?.data]);
  if (!item) {
    return <NotFoundPage />;
  }
  if (error) {
    return <Error error={error} reset={() => {}} />;
  }
  return (
    <Stack width={1} gap={2} py={2}>
      <InformationItem item={item} />
      <Stack width={1}>
        <SwiperPagination style={{ width: '100%' }} spaceBetween={2}>
          {item.tags?.map((tag, i) => (
            <SwiperSlide key={i}>
              <TagItem tag={tag} />
            </SwiperSlide>
          ))}
        </SwiperPagination>
      </Stack>
      <Typography variant="h3" fontSize={24} fontWeight={600} color={'#7f7f7f'}>
        More Like This
      </Typography>
      <ListSimilarItem
        queryKey={QueryKeys.GetItemSimilar}
        initQuery={initQueryItems}
      />
    </Stack>
  );
};
