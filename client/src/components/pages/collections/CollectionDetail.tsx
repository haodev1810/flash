'use client';
import { Stack } from '@mui/material';
import NotFoundPage from 'client/app/not-found';
import React, { useMemo } from 'react';
import Error from 'client/app/error';
import { CollectionsApiListImagesCollectionRequest } from '@libs/openapi-generator/generated';
import { ListCollectionItems } from '@client/components/collections/ListCollectionItems';
import { InformationCollection } from '@client/components/collections/InformationCollection';
import { useGetCollectionDetail } from '@client/apis/hooks/collections/useGetCollectionDetail';
import { Navbar } from '@client/components/navbars/Navbar';

interface Props {
  slug: string;
  initQueryItems: CollectionsApiListImagesCollectionRequest;
}
export const CollectionDetail = ({ slug, initQueryItems }: Props) => {
  const { data, error } = useGetCollectionDetail({ slug });
  const collection = useMemo(() => {
    return data?.data;
  }, [data?.data]);
  if (!collection) {
    return <NotFoundPage />;
  }
  if (error) {
    return <Error error={error} reset={() => {}} />;
  }
  return (
    <Stack width={1} gap={2} py={2}>
      <Navbar />
      <InformationCollection collection={collection} />
      <ListCollectionItems initQuery={initQueryItems} />
    </Stack>
  );
};
