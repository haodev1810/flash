'use client';
import React, { useEffect, useMemo, useState } from 'react';
import { Stack, Typography } from '@mui/material';
import { Navbar } from '@client/components/navbars/Navbar';
import { getListCollectionPublic } from '@client/apis/hooks/collections/useGetListCollectionPublic';
import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import {
  CollectionsApiListRequest,
  Pagination,
} from '@libs/openapi-generator/generated';
import { useScrollToLoadMoreBody } from '@client/hooks/useScrollToLoadMoreBody';
import { useInfiniteQuery } from '@tanstack/react-query';
import { CollectionViewMedium } from '@client/components/ui/collections/CollectionViewMedium';
import Link from '@client/components/ui/link/Link';

interface Props {
  initParamsQuery: CollectionsApiListRequest;
}
export const ListCollectionPage = ({ initParamsQuery }: Props) => {
  const [pagination, setPagination] = useState<Pagination>();
  const { shouldFetchNextPage } = useScrollToLoadMoreBody({
    threshold: 1000,
  });
  const { fetchNextPage, data } = useInfiniteQuery({
    queryKey: [QueryKeys.GetCollectionPublic],
    queryFn: ({ pageParam }) =>
      getListCollectionPublic({ ...initParamsQuery, page: Number(pageParam) }),
    initialPageParam: 1,
    getNextPageParam: (lastPage) => lastPage.pagination.page + 1,
    getPreviousPageParam: (firstPage) => firstPage.pagination.page,
  });
  const collections = useMemo(() => {
    if (data?.pages) {
      setPagination(data.pages[0].pagination);
    }
    return data?.pages.map((page) => page.data).flat() ?? [];
  }, [data?.pages]);
  useEffect(() => {
    if (shouldFetchNextPage) {
      fetchNextPage().catch();
    }
  }, [fetchNextPage, shouldFetchNextPage]);
  return (
    <Stack width={1} gap={2} py={2}>
      <Stack alignItems={'center'} justifyContent={'center'}>
        <Navbar />
      </Stack>
      <Stack columnGap={3} rowGap={3} flexDirection={'row'} flexWrap={'wrap'}>
        {collections.map((collection, i) => (
          <Stack
            component={Link}
            href={'/collections/' + collection.slug}
            color={'#0b0b0b  '}
            underline="none"
            key={i}
            gap={1}
          >
            <CollectionViewMedium
              sx={{
                maxWidth: 400,
                borderRadius: 5,
                '&:hover': {
                  filter: 'brightness(.8)',
                },
              }}
              rowHeight={150}
              variant="quilted"
              collection={collection}
            />
            <Typography mt={-1} fontWeight={600} textAlign={'center'}>
              {collection.title}
            </Typography>
          </Stack>
        ))}
      </Stack>
    </Stack>
  );
};
