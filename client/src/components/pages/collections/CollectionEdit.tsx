'use client';
import { Stack } from '@mui/material';
import NotFoundPage from 'client/app/not-found';
import Error from 'client/app/error';
import { useGetCollectionDetail } from '@client/apis/hooks/collections/useGetCollectionDetail';
import { Navbar } from '@client/components/navbars/Navbar';
import { FormUpdateCollection } from '@client/components/forms/FormUpdateCollection';
import React, { useEffect, useMemo } from 'react';
import { useScrollToLoadMoreBody } from '@client/hooks/useScrollToLoadMoreBody';
import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { CollectionsApiListImagesCollectionRequest } from '@libs/openapi-generator/generated';
import { useInfiniteQuery } from '@tanstack/react-query';
import { getListItemCollection } from '@client/apis/hooks/collections/useGetListItemCollection';

interface Props {
  slug: string;
  initQueryItems: CollectionsApiListImagesCollectionRequest;
}
export const CollectionEditPage = ({ slug, initQueryItems }: Props) => {
  const res = useGetCollectionDetail({ slug });
  const { shouldFetchNextPage } = useScrollToLoadMoreBody({
    threshold: 2000,
  });
  const { fetchNextPage, data } = useInfiniteQuery({
    queryKey: [QueryKeys.GetListItemCollection],
    queryFn: ({ pageParam }) =>
      getListItemCollection({ ...initQueryItems, page: Number(pageParam) }),
    initialPageParam: 1,
    getNextPageParam: (lastPage) => lastPage.pagination.page + 1,
    getPreviousPageParam: (firstPage) => firstPage.pagination.page,
  });
  const items = useMemo(() => {
    return data?.pages.map((page) => page.data).flat() || [];
  }, [data?.pages]);

  useEffect(() => {
    if (shouldFetchNextPage) {
      console.log('1');
      fetchNextPage().catch();
    }
  }, [fetchNextPage, shouldFetchNextPage]);
  const collection = useMemo(() => {
    return res.data?.data;
  }, [res.data?.data]);
  if (!collection) {
    return <NotFoundPage />;
  }
  if (res.error) {
    return <Error error={res.error} reset={() => {}} />;
  }
  return (
    <Stack width={1} gap={2} py={2}>
      <Navbar />
      <FormUpdateCollection items={items} collection={collection} />
    </Stack>
  );
};
