'use client';
import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getListItemVideo } from '@client/apis/hooks/videos/useGetListItemVideo';
import { Navbar } from '@client/components/navbars/Navbar';
import { NavbarItem } from '@client/components/navbars/NavbarItem';
import { ListViewVideo } from '@client/components/ui/list-view/ListViewVideo';
import { useScrollToLoadMoreBody } from '@client/hooks/useScrollToLoadMoreBody';
import { numberConvert } from '@client/utils/number';
import {
  Pagination,
  VideosApiListRequest,
} from '@libs/openapi-generator/generated';
import { Stack } from '@mui/material';
import { useInfiniteQuery } from '@tanstack/react-query';
import React, { useEffect, useMemo, useState } from 'react';

interface VideosPageProps {
  initQueryVideo: VideosApiListRequest;
}
export const VideosPage = ({ initQueryVideo }: VideosPageProps) => {
  const [pagination, setPagination] = useState<Pagination>();
  const { shouldFetchNextPage } = useScrollToLoadMoreBody({
    threshold: 10,
  });
  const { fetchNextPage, data } = useInfiniteQuery({
    queryKey: [QueryKeys.GetListItemVideo],
    queryFn: ({ pageParam }) =>
      getListItemVideo({ ...initQueryVideo, page: Number(pageParam) }),
    initialPageParam: 1,
    getNextPageParam: (lastPage) => lastPage.pagination.page + 1,
    getPreviousPageParam: (firstPage) => firstPage.pagination.page,
  });
  const items = useMemo(() => {
    if (data?.pages) {
      setPagination(data.pages[0].pagination);
    }
    return data?.pages.map((page) => page.data).flat() ?? [];
  }, [data?.pages]);
  useEffect(() => {
    if (shouldFetchNextPage) {
      fetchNextPage().catch();
    }
  }, [fetchNextPage, shouldFetchNextPage]);
  return (
    <Stack width={1} gap={2} py={2}>
      <Navbar />
      <Stack flexDirection={'row'} alignItems={'center'} gap={2}>
        <NavbarItem
          item={{
            id: 'photos',
            title: 'Photos',
            link: '/',
            permission: true,
          }}
        />
        <NavbarItem
          item={{
            id: 'videos',
            title: 'Videos',
            link: '/videos',
            permission: true,
            subText: numberConvert(pagination?.total ?? 0, 2),
          }}
        />
      </Stack>
      <ListViewVideo items={items} column={3} />
    </Stack>
  );
};
