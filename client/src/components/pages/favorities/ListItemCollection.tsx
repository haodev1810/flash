import { RoundedButton } from '@client/components/buttons/RoundedButton';
import { DialogCollection } from '@client/components/dialogs/collections/DialogCollection';
import { FavoriteItem } from '@client/components/items/FavoriteItem';
import { chunkArray } from '@client/utils/array';
import { ItemResponse } from '@libs/openapi-generator/generated';
import {
  Box,
  Button,
  Grid,
  Stack,
  StackProps,
  Typography,
} from '@mui/material';
import React, { useMemo, useState } from 'react';

interface Props extends StackProps {
  items: ItemResponse[];
}
export const ListItemCollection = ({ items }: Props) => {
  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [isChooseAll, setIsChooseAll] = useState<boolean>(false);
  const [idsSelected, setIdsSelected] = useState<number[]>([]);
  const arrItems = useMemo(() => {
    return chunkArray<ItemResponse>(items, 3);
  }, [items]);
  const onCreateCollection = async () => {
    setOpenDialog(true);
  };
  const onSelectAll = async () => {
    if (!isChooseAll) {
      setIdsSelected(items.map((i) => i.id));
      setIsChooseAll(true);
    } else {
      setIdsSelected([]);
      setIsChooseAll(false);
    }
  };
  const onCloseDialogCollection = () => {
    setOpenDialog(false);
  };
  return (
    <Stack gap={2}>
      <Grid container boxSizing="border-box">
        <Grid item xs={12} sm={6}>
          <Button
            startIcon={'Photos'}
            size="large"
            sx={{
              bgcolor: '#000',
              color: '#FFF',
              textTransform: 'none',
              borderRadius: 16,
              ':hover': {
                bgcolor: '#3a3a3a',
              },
              px: 2,
            }}
          >
            <Typography fontSize={14} color="#7F7F7F">
              {items.length}
            </Typography>
          </Button>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Stack
            flexDirection={'row'}
            alignItems={'center'}
            flexWrap={'wrap'}
            gap={1}
            justifyContent={{ xs: 'flex-start', md: 'flex-end' }}
          >
            <RoundedButton
              size="large"
              sx={{
                px: 2,
              }}
              onClick={onSelectAll}
            >
              {!isChooseAll ? 'Choose all' : 'Cancel'}
            </RoundedButton>
            <RoundedButton
              size="large"
              sx={{
                px: 2,
              }}
              onClick={onCreateCollection}
            >
              Create collection
            </RoundedButton>
          </Stack>
        </Grid>
      </Grid>
      <Box>
        <Grid container columnSpacing={2} boxSizing={'border-box'}>
          {arrItems.map((items, index) => (
            <Grid key={index} item xs={12} sm={6} md={4} rowSpacing={2}>
              <Grid container boxSizing={'border-box'} rowSpacing={2}>
                {items.map((item, index) => (
                  <Grid key={index} item xs={12}>
                    <FavoriteItem
                      idsSelected={idsSelected}
                      setIdsSelected={setIdsSelected}
                      item={item}
                    />
                  </Grid>
                ))}
              </Grid>
            </Grid>
          ))}
        </Grid>
      </Box>
      <DialogCollection
        itemIds={idsSelected}
        open={openDialog}
        onClose={onCloseDialogCollection}
      />
    </Stack>
  );
};
