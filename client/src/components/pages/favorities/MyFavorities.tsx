'use client';
import { Stack, Typography } from '@mui/material';
import React from 'react';
import { ListItemCollection } from './ListItemCollection';
import { useAppSelector } from '@client/stores';

export const MyFavorities = () => {
  const favoriteItems = useAppSelector((state) => state.collection.items);
  return (
    <Stack width={1} gap={2} py={2}>
      <Typography fontSize={30} variant="h1" fontWeight={600}>
        My Favorite Images
      </Typography>
      <ListItemCollection items={favoriteItems} />
    </Stack>
  );
};
