'use client';
import { Navbar } from '@client/components/navbars/Navbar';
import { NavbarItem } from '@client/components/navbars/NavbarItem';
import { Container, Stack, Typography } from '@mui/material';
import React, { PropsWithChildren } from 'react';

export const LeaderboardPage = ({ children }: PropsWithChildren) => {
  return (
    <Stack width={1} gap={2} py={2}>
      <Container maxWidth="xl">
        <Stack rowGap={4}>
          <Navbar />
          <Stack rowGap={2}>
            <Typography
              textAlign={'center'}
              variant="h3"
              fontSize={33}
              color={'#2c243e'}
              fontWeight={500}
            >
              Leaderboard
            </Typography>
            <Typography
              textAlign={'center'}
              variant="body1"
              color={'#4A4A4A'}
              fontSize={18}
            >
              Members with the most views on content added in the last 4 weeks.
            </Typography>
          </Stack>
        </Stack>
        <Stack rowGap={3}>
          <Stack flexDirection={'row'} gap={1}>
            <NavbarItem
              item={{
                id: '1',
                title: 'Recent',
                link: '/leaderboard',
                permission: true,
              }}
            />
          </Stack>
          {children}
        </Stack>
      </Container>
    </Stack>
  );
};
