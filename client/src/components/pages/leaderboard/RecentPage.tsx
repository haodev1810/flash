'use client';
import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getLeaderboard } from '@client/apis/hooks/authors/useGetLeaderboardAuthor';
import { LeaderboardAuthor } from '@client/components/authors/LeaderboardAuthor';
import { useScrollToLoadMoreBody } from '@client/hooks/useScrollToLoadMoreBody';
import { AuthorsApiListLeaderboardRequest } from '@libs/openapi-generator/generated';
import { Stack } from '@mui/material';
import { useInfiniteQuery } from '@tanstack/react-query';
import React, { useEffect, useMemo } from 'react';

interface Props {
  initParamsQuery: AuthorsApiListLeaderboardRequest;
}
export const RecentPage = ({ initParamsQuery }: Props) => {
  const { shouldFetchNextPage } = useScrollToLoadMoreBody({
    threshold: 200,
  });
  const { fetchNextPage, data } = useInfiniteQuery({
    queryKey: [QueryKeys.GetLeaderboard],
    queryFn: ({ pageParam }) =>
      getLeaderboard({ ...initParamsQuery, page: Number(pageParam) }),
    initialPageParam: 1,
    getNextPageParam: (lastPage) => lastPage.pagination.page + 1,
    getPreviousPageParam: (firstPage) => firstPage.pagination.page,
  });
  const rankeds = useMemo(() => {
    return data?.pages.map((page) => page.data).flat() ?? [];
  }, [data?.pages]);
  useEffect(() => {
    if (shouldFetchNextPage) {
      fetchNextPage().catch();
    }
  }, [fetchNextPage, shouldFetchNextPage]);
  return (
    <Stack gap={4}>
      {rankeds?.length > 0 &&
        rankeds?.map((ranked, index) => (
          <LeaderboardAuthor ranked={ranked} index={index + 1} key={index} />
        ))}
    </Stack>
  );
};
