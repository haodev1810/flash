'use client';
import { useGetListItems } from '@client/apis/hooks/items/useGetListItem';
import { GoogleSignInButton } from '@client/components/buttons/GoogleLogin';
import { RoundedButton } from '@client/components/buttons/RoundedButton';
import { Logo } from '@client/components/layouts/Logo';
import Link from '@client/components/ui/link/Link';
import { genFilePath } from '@client/utils/file';
import { ItemsApiListsRequest } from '@libs/openapi-generator/generated';
import {
  Box,
  Container,
  ImageList,
  ImageListItem,
  Paper,
  Stack,
  Typography,
} from '@mui/material';
import React, { useMemo } from 'react';

interface LoginPageProps {
  initQuery: ItemsApiListsRequest;
}
export const LoginPage = ({ initQuery }: LoginPageProps) => {
  const { data } = useGetListItems(initQuery);
  const items = useMemo(() => {
    return data?.data ?? [];
  }, [data?.data]);
  return (
    <Stack position={'absolute'} top={0} bottom={0} left={0} right={0}>
      <Container maxWidth="xl">
        <Stack
          height={80}
          flexDirection={'row'}
          alignItems={'center'}
          justifyContent={'space-between'}
        >
          <Logo />
          <RoundedButton component={Link} href="/">
            Join
          </RoundedButton>
        </Stack>
      </Container>
      <Stack flex={1} position={'relative'}>
        <Stack
          bgcolor={'#0b0b0b'}
          zIndex={1}
          position={'absolute'}
          top={0}
          bottom={0}
          left={0}
          right={0}
          sx={{ filter: 'brightness(.25)' }}
        >
          <Box flex={1} sx={{ overflowY: 'hidden' }}>
            <ImageList variant="masonry" cols={6} gap={12}>
              {items.map((item, index) => (
                <ImageListItem key={index}>
                  <img
                    width={100}
                    alt={item.alt}
                    src={genFilePath(item.images?.medium)}
                    loading="lazy"
                    style={{ borderRadius: '16px' }}
                    title={item.title}
                  />
                </ImageListItem>
              ))}
            </ImageList>
          </Box>
        </Stack>
        <Stack
          zIndex={2}
          component={Paper}
          position={'absolute'}
          top={'50%'}
          left={'50%'}
          sx={{ transform: 'translate(-50%, -50%)' }}
          maxWidth={400}
          maxHeight={400}
          height={1}
          width={1}
          borderRadius={5}
          justifyContent={'center'}
          alignItems={'center'}
          rowGap={3}
        >
          <Typography
            sx={{
              background: '-webkit-linear-gradient(#250202, #bf4af552)',
              WebkitBackgroundClip: 'text',
              WebkitTextFillColor: 'transparent',
            }}
            variant="h3"
            fontSize={30}
          >
            Welcome to FFlash
          </Typography>
          <Typography
            variant="body2"
            sx={{
              background: '-webkit-linear-gradient(#030225, #bf4af552)',
              WebkitBackgroundClip: 'text',
              WebkitTextFillColor: 'transparent',
            }}
          >
            Join to explore my service
          </Typography>
          <GoogleSignInButton />
          <Typography
            sx={{
              background: '-webkit-linear-gradient(#000, #bf4af552)',
              WebkitBackgroundClip: 'text',
              WebkitTextFillColor: 'transparent',
            }}
            variant="caption"
          >
            &copy; copyright by Flash
          </Typography>
        </Stack>
      </Stack>
    </Stack>
  );
};
