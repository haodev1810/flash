'use client';
import { useGetProfileAuthor } from '@client/apis/hooks/authors/useGetProfileAuthor';
import { ListItemAuthor } from '@client/components/authors/ListItemAuthor';
import { genFilePath } from '@client/utils/file';
import { Avatar, Container, Stack, Typography } from '@mui/material';
import NotFoundPage from 'client/app/not-found';
import React from 'react';
import InstagramIcon from '@mui/icons-material/Instagram';
import Link from '@client/components/ui/link/Link';
import { ItemsApiListsRequest } from '@libs/openapi-generator/generated';

interface Props {
  username: string;
  initQueryItems: ItemsApiListsRequest;
}
export const AuthorPage = ({ username, initQueryItems }: Props) => {
  const { data, error } = useGetProfileAuthor({ username });
  if (error) {
    return <NotFoundPage />;
  }
  return (
    <Stack width={1} gap={2} py={2}>
      <Container maxWidth={'xl'}>
        <Stack mt={2} alignItems={'center'} justifyContent={'center'} gap={2}>
          <Stack>
            <Avatar
              src={genFilePath(data?.data.avatar_large)}
              alt={data?.data.username}
              sx={{ width: 120, height: 120 }}
            />
          </Stack>
          <Typography
            variant="h1"
            fontSize={{ xs: 36, md: 60 }}
            fontWeight={400}
            color={'#2C343E'}
          >
            {data?.data.name}
          </Typography>
          <Typography>Photographer.</Typography>
          <Stack
            component={Link}
            target="_blank"
            underline="none"
            href={'https://www.instagram.com/' + data?.data.username}
            flexDirection={'row'}
            alignItems={'center'}
            gap={0.5}
          >
            <InstagramIcon htmlColor="#ff20bc" />
            <Typography
              fontWeight={500}
              color={'#7f7f7f'}
              sx={{ borderBottom: '1px dotted #7F7F7F' }}
            >
              {data?.data.username}
            </Typography>
          </Stack>
          <Stack
            flexDirection={'row'}
            sx={{
              '.after_slpit': {
                ':after': {
                  content: "''",
                  position: 'absolute',
                  right: 0,
                  top: '50%',
                  transform: 'translateY(-50%)',
                  width: '1px',
                  height: '20px',
                  bgcolor: '#7F7F7F',
                },
              },
            }}
          >
            <Stack px={4} className="after_slpit" position={'relative'}>
              <Typography textAlign={'center'} color={'#7F7F7F'}>
                Total views
              </Typography>
              <Typography textAlign={'center'} color={'#2C343E'} fontSize={21}>
                {data?.data.total_view}
              </Typography>
            </Stack>
            <Stack px={3} position={'relative'}>
              <Typography textAlign={'center'} color={'#7F7F7F'}>
                Total items
              </Typography>
              <Typography textAlign={'center'} color={'#2C343E'} fontSize={21}>
                {data?.data.total_item}
              </Typography>
            </Stack>
          </Stack>
        </Stack>
        <Stack mt={3}>
          <ListItemAuthor initQueryItems={initQueryItems} />
        </Stack>
      </Container>
    </Stack>
  );
};
