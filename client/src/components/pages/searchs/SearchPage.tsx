'use client';
import { Stack, Typography } from '@mui/material';
import React, { useMemo } from 'react';
import Error from 'client/app/error';
import { ListSimilarItem } from '@client/components/items/ListSimilarItem';
import { SwiperPagination } from '@client/components/ui/swiper/SwiperPagination';
import { SwiperSlide } from 'swiper/react';
import { TagItem } from '@client/components/topics/TagItem';
import { ItemsApiListsRequest } from '@libs/openapi-generator/generated';
import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { useGetTagsQuery } from '@client/apis/hooks/tags';

interface Props {
  query: string;
  initQueryItems: ItemsApiListsRequest;
}
export const SearchPage = ({ query, initQueryItems }: Props) => {
  const { data } = useGetTagsQuery({ query, page: 1, limit: 20 });
  const tags = useMemo(() => {
    return data?.data ?? [];
  }, [data?.data]);
  return (
    <Stack width={1} gap={2} py={2}>
      <Stack width={1}>
        <SwiperPagination style={{ width: '100%' }} spaceBetween={2}>
          {tags?.map((tag, i) => (
            <SwiperSlide key={i}>
              <TagItem tag={tag} />
            </SwiperSlide>
          ))}
        </SwiperPagination>
      </Stack>
      <Typography variant="h3" fontSize={24} fontWeight={600} color={'#7f7f7f'}>
        Result for <strong>#{query}</strong>
      </Typography>
      <ListSimilarItem
        queryKey={QueryKeys.GetItemSimilar}
        initQuery={initQueryItems}
      />
    </Stack>
  );
};
