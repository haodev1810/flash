'use client';
import { Stack } from '@mui/material';
import React, { useEffect, useMemo, useState } from 'react';
import { useScrollToLoadMoreBody } from '@client/hooks/useScrollToLoadMoreBody';
import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import {
  ItemsApiListsRequest,
  Pagination,
} from '@libs/openapi-generator/generated';
import { useInfiniteQuery } from '@tanstack/react-query';
import { getListItems } from '@client/apis/hooks/items/useGetListItem';
import { ListViewItems } from '../ui/list-view/ListViewItems';
import { NavbarItem } from '../navbars/NavbarItem';
import { numberConvert } from '@client/utils/number';

interface Props {
  initQuery: ItemsApiListsRequest;
}
export const ListItems = ({ initQuery }: Props) => {
  const [pagination, setPagination] = useState<Pagination>();
  const { shouldFetchNextPage } = useScrollToLoadMoreBody({
    threshold: 1000,
  });
  const { fetchNextPage, data } = useInfiniteQuery({
    queryKey: [QueryKeys.ListItems],
    queryFn: ({ pageParam }) =>
      getListItems({ ...initQuery, page: Number(pageParam) }),
    initialPageParam: 1,
    getNextPageParam: (lastPage) => lastPage.pagination.page + 1,
    getPreviousPageParam: (firstPage) => firstPage.pagination.page,
  });
  const items = useMemo(() => {
    if (data?.pages) {
      setPagination(data.pages[0].pagination);
    }
    return data?.pages.map((page) => page.data).flat() ?? [];
  }, [data?.pages]);
  useEffect(() => {
    if (shouldFetchNextPage) {
      fetchNextPage().catch();
    }
  }, [fetchNextPage, shouldFetchNextPage]);
  return (
    <Stack gap={2}>
      <Stack flexDirection={'row'} alignItems={'center'} gap={2}>
        <NavbarItem
          item={{
            id: 'photos',
            title: 'Photos',
            link: '/',
            permission: true,
            subText: numberConvert(pagination?.total ?? 0, 1),
          }}
        />
        <NavbarItem
          item={{
            id: 'videos',
            title: 'Videos',
            link: '/videos',
            permission: true,
            subText: '',
          }}
        />
      </Stack>
      <ListViewItems column={3} items={items} />
    </Stack>
  );
};
