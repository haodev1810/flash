'use client';
import { Button, Stack, Typography } from '@mui/material';
import React, { useEffect, useMemo, useState } from 'react';
// import { ItemView } from '../items/ItemView';
import { useScrollToLoadMoreBody } from '@client/hooks/useScrollToLoadMoreBody';
import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import {
  ItemsApiListsRequest,
  Pagination,
} from '@libs/openapi-generator/generated';
import { useInfiniteQuery } from '@tanstack/react-query';
import { getListItems } from '@client/apis/hooks/items/useGetListItem';
import { ListViewItems } from '../ui/list-view/ListViewItems';

interface Props {
  initQuery: ItemsApiListsRequest;
  queryKey: string;
}
export const ListSimilarItem = ({ initQuery }: Props) => {
  const [pagination, setPagination] = useState<Pagination>();
  const { shouldFetchNextPage } = useScrollToLoadMoreBody({
    threshold: 1000,
  });
  const { fetchNextPage, data } = useInfiniteQuery({
    queryKey: [QueryKeys.GetItemSimilar],
    queryFn: ({ pageParam }) =>
      getListItems({ ...initQuery, page: Number(pageParam) }),
    initialPageParam: 1,
    getNextPageParam: (lastPage) => lastPage.pagination.page + 1,
    getPreviousPageParam: (firstPage) => firstPage.pagination.page,
  });
  const items = useMemo(() => {
    if (data?.pages) {
      setPagination(data.pages[0].pagination);
    }
    return data?.pages.map((page) => page.data).flat() ?? [];
  }, [data?.pages]);
  // const arrItems = useMemo(() => {
  //   if (data?.pages) {
  //     setPagination(data.pages[0].pagination);
  //   }
  //   return chunkArray<ItemResponse>(
  //     data?.pages.map((page) => page.data).flat(),
  //     column || 3
  //   );
  // }, [column, data?.pages]);

  useEffect(() => {
    if (shouldFetchNextPage) {
      fetchNextPage().catch();
    }
  }, [fetchNextPage, shouldFetchNextPage]);
  return (
    <Stack gap={2}>
      <Stack flexDirection={'row'} alignItems={'center'} gap={2}>
        <Button
          startIcon={'Photos'}
          size="large"
          sx={{
            bgcolor: '#000',
            color: '#FFF',
            textTransform: 'none',
            borderRadius: 16,
            ':hover': {
              bgcolor: '#3a3a3a',
            },
            px: 2,
          }}
        >
          <Typography fontSize={14} color="#7F7F7F">
            {(pagination?.total || 0) / 1000 > 1
              ? ((pagination?.total || 0) / 1000).toFixed(2) + 'k'
              : pagination?.total}
          </Typography>
        </Button>
      </Stack>
      <ListViewItems items={items} column={3} />
      {/* <Box>
        <Grid container columnSpacing={2} boxSizing={'border-box'}>
          {arrItems.map((items, index) => (
            <Grid key={index} item xs={12} sm={6} md={4} rowSpacing={2}>
              <Grid container boxSizing={'border-box'} rowSpacing={2}>
                {items.map((item, index) => (
                  <Grid key={index} item xs={12}>
                    <ItemView item={item} />
                  </Grid>
                ))}
              </Grid>
            </Grid>
          ))}
        </Grid>
      </Box> */}
    </Stack>
  );
};
