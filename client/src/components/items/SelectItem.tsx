/* eslint-disable @next/next/no-img-element */
'use client';
import { genFilePath } from '@client/utils/file';
import { ItemResponse } from '@libs/openapi-generator/generated';
import {
  Avatar,
  Button,
  Checkbox,
  FormControlLabel,
  Stack,
  StackProps,
  Tooltip,
  Typography,
} from '@mui/material';
import React, { useState } from 'react';
import FileDownloadOutlinedIcon from '@mui/icons-material/FileDownloadOutlined';
import { useDownloadItemMutaion } from '@client/apis/hooks/items/useDownloadMutation';
import { saveAs } from 'file-saver';
import Link from 'next/link';
import { useAppDispatch } from '@client/stores';

interface Props extends StackProps {
  item: ItemResponse;
  idsSelected: number[];
  setIdsSelected: React.Dispatch<React.SetStateAction<number[]>>;
}
export const SelectItem = ({
  item,
  setIdsSelected,
  idsSelected,
  ...props
}: Props) => {
  const dispatch = useAppDispatch();
  const downloadItemMutation = useDownloadItemMutaion();
  const [hover, setHover] = useState<boolean>(false);
  const onDownload = async () => {
    await downloadItemMutation.mutateAsync({
      downloadDto: {
        id: item.id,
        slug: item.slug,
      },
    });
    saveAs(genFilePath(item.images?.medium), item.slug + '.jpg');
  };
  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    checked: boolean
  ) => {
    if (checked) {
      setIdsSelected([Number(e.target.value), ...idsSelected]);
    } else {
      setIdsSelected(idsSelected.filter((i) => i != Number(e.target.value)));
    }
  };
  return (
    <Stack
      width={1}
      bgcolor={'rgb(116, 139, 82)'}
      position={'relative'}
      onMouseMove={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <Stack zIndex={999} position={'absolute'} top={2} right={5}>
        <Tooltip title={'Select'}>
          <FormControlLabel
            label=""
            sx={{ bgcolor: 'transparent', borderRadius: 2 }}
            control={
              <Checkbox
                sx={{
                  color: '#FFF',
                  '&.Mui-checked': {
                    color: '#17c500',
                  },
                }}
                checked={idsSelected?.includes(item.id)}
                value={item.id}
                onChange={handleChange}
              />
            }
          />
        </Tooltip>
      </Stack>
      <Stack position={'relative'}>
        <img
          loading="lazy"
          src={genFilePath(item.images?.medium)}
          alt={item.alt}
          title={item.title}
        />
      </Stack>
      {hover && (
        <>
          <Stack
            component={Link}
            href={'/photos/' + item.slug}
            zIndex={1}
            position={'absolute'}
            top={0}
            right={0}
            left={0}
            height={1}
            width={1}
            sx={{
              background:
                'linear-gradient(180deg,rgba(0,0,0,.25),transparent 35%,transparent 65%,rgba(0,0,0,.25))',
            }}
            alignContent={'space-between'}
          ></Stack>
          <Stack width={1} position="absolute" bottom={0} right={0} zIndex={1}>
            <Stack
              flexDirection="row"
              alignItems="center"
              justifyContent="space-between"
              py={2}
              px={2}
            >
              <Stack flexDirection="row" alignItems="center" gap={1}>
                <Avatar src={genFilePath(item.author?.avatar)} />
                <Typography
                  fontSize={{ sm: 10, md: 14, lg: 16 }}
                  color={'#F7F7F7'}
                  fontWeight={500}
                >
                  {item.author?.name}
                </Typography>
              </Stack>
              <Stack
                sx={(theme) => ({
                  [theme.breakpoints.down('md')]: { display: 'none' },
                })}
              >
                <Button
                  onClick={onDownload}
                  variant="contained"
                  size="large"
                  sx={{
                    bgcolor: '#05A081',
                    borderRadius: 16,
                    textTransform: 'none',
                    ':hover': {
                      bgcolor: '#12b695',
                    },
                  }}
                  startIcon={<FileDownloadOutlinedIcon />}
                >
                  Download
                </Button>
              </Stack>
            </Stack>
          </Stack>
        </>
      )}
    </Stack>
  );
};
