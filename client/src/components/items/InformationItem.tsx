'use client';
import { genFilePath } from '@client/utils/file';
import { ItemFullResponse } from '@libs/openapi-generator/generated';
import {
  Avatar,
  Container,
  Grid,
  Stack,
  StackProps,
  Typography,
} from '@mui/material';
import React, { useState } from 'react';
import { CollectButton } from '../buttons/CollectButton';
import { ImageZoom } from './ZoomImage';
import { useAppDispatch } from '@client/stores';
import { pushItem } from '@client/stores/slices/collectionSlice';
import { useNotify } from '../notifications/hooks';
import { MoreInformationItemDialog } from '../dialogs/items/MoreInformationItemDialog';
import { RoundedButton } from '../buttons/RoundedButton';
import InforIcon from '@mui/icons-material/Info';
import Link from '../ui/link/Link';

interface InformationItemProps extends StackProps {
  item: ItemFullResponse;
}
export const InformationItem = ({ item, ...props }: InformationItemProps) => {
  const [open, setOpen] = useState<boolean>(false);
  const [disable, setDisable] = useState<boolean>(false);
  const dispatch = useAppDispatch();
  const { notify } = useNotify();
  const onCollect = () => {
    const { tags, ...rest } = item;
    dispatch(pushItem(rest));
    notify({ content: 'Add item to collection success' });
    setDisable(true);
  };
  return (
    <Stack width={1} gap={2}>
      <Container maxWidth={'xl'}>
        <Grid
          container
          boxSizing={'border-box'}
          columnSpacing={2}
          rowSpacing={2}
        >
          <Grid item xs={12} md={3}>
            <Stack width={1} flexDirection={'row'} gap={1}>
              <Avatar
                sx={{ height: 50, width: 50 }}
                src={genFilePath(item.author?.avatar)}
              />
              <Stack>
                <Typography
                  variant="h3"
                  fontWeight={500}
                  fontSize={20}
                  color="#2c343e"
                >
                  {item.author?.name}
                </Typography>
                <Typography
                  component={Link}
                  underline="none"
                  href={'/authors/' + item.author?.username}
                  color={'#7F7F7F'}
                >
                  View profile
                </Typography>
              </Stack>
            </Stack>
          </Grid>
          <Grid item xs={12} md={9}>
            <Stack
              width={1}
              flexDirection={'row'}
              gap={2}
              justifyContent={{ sm: 'flex-start', md: 'flex-end' }}
            >
              <CollectButton disabled={disable} onClick={onCollect} />
            </Stack>
          </Grid>
        </Grid>
      </Container>
      <ImageZoom item={item} />
      <Container maxWidth={'xl'}>
        <Stack flexDirection={'row'} justifyContent={'flex-end'}>
          <RoundedButton
            onClick={() => {
              setOpen(true);
            }}
            sx={{
              borderColor: '#7f7f7f',
              color: '#7f7f7f',
              ':hover': { color: '#55b0fa' },
            }}
            startIcon={<InforIcon />}
            variant="outlined"
          >
            More Information
          </RoundedButton>
        </Stack>
      </Container>
      {open && (
        <MoreInformationItemDialog
          item={item}
          open={open}
          onClose={() => {
            setOpen(false);
          }}
        />
      )}
    </Stack>
  );
};
