/* eslint-disable @next/next/no-img-element */
'use client';
import React, { useState } from 'react';
import { ItemFullResponse } from '@libs/openapi-generator/generated';
import { genFilePath } from '@client/utils/file';
import { Box, Stack } from '@mui/material';

interface Props {
  item: ItemFullResponse;
}
export const ImageZoom = ({ item }: Props) => {
  const [zoomLevel] = useState<number>(3);
  const [[magnifieWidth, magnifierHeight]] = useState<number[]>([150, 150]);
  const [showMagnifier, setShowMagnifier] = useState<boolean>(false);
  const [[x, y], setXY] = useState<number[]>([0, 0]);
  const [[imgWidth, imgHeight], setSize] = useState<number[]>([0, 0]);

  return (
    <Stack
      width={1}
      boxSizing={'border-box'}
      alignItems={'center'}
      position={'relative'}
    >
      <img
        loading="lazy"
        src={genFilePath(item.images?.large)}
        alt={item.alt}
        title={item.title}
        style={{
          cursor: showMagnifier ? 'none' : 'zoom-in',
          borderRadius: '8px',
          maxWidth: '100%',
          height: 'auto',
          objectFit: 'cover',
        }}
        onClick={() => setShowMagnifier(!showMagnifier)}
        onMouseLeave={() => {
          setShowMagnifier(false);
        }}
        onMouseEnter={(e) => {
          const elem = e.currentTarget;
          const { width, height } = elem.getBoundingClientRect();
          setSize([width, height]);
        }}
        onMouseMove={(e) => {
          const elem = e.currentTarget;
          const { top, left } = elem.getBoundingClientRect();
          const x = e.pageX - left - window.pageXOffset;
          const y = e.pageY - top - window.pageYOffset;
          setXY([x, y]);
        }}
      />
      <Box
        position={'absolute'}
        sx={{
          display: showMagnifier ? 'flex' : 'none',
          position: 'absolute',
          pointerEvents: 'none',
          height: `${magnifierHeight}px`,
          width: `${magnifieWidth}px`,
          top: `${y - magnifierHeight / 2}px`,
          left: `${x - magnifieWidth / 2}px`,
          opacity: '1',
          border: '1px solid lightgray',
          backgroundColor: 'transparent',
          backgroundImage: `url('${item.images?.origin?.path as string}')`,
          backgroundRepeat: 'no-repeat',
          backgroundSize: `${imgWidth * zoomLevel}px ${
            imgHeight * zoomLevel
          }px`,
          backgroundPositionX: `${-x * zoomLevel + magnifieWidth / 2}px`,
          backgroundPositionY: `${-y * zoomLevel + magnifierHeight / 2}px`,
        }}
      ></Box>
    </Stack>
  );
};
