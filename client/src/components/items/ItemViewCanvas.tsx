/* eslint-disable @next/next/no-img-element */
'use client';
import { genFilePath } from '@client/utils/file';
import { ItemResponse } from '@libs/openapi-generator/generated';
import { Stack, StackProps } from '@mui/material';
import React, { useEffect, useRef, useState } from 'react';

interface Props extends StackProps {
  item: ItemResponse;
}
export const ItemViewCanvas = ({ item, ...props }: Props) => {
  const imageRef = useRef<HTMLImageElement>(null);
  const [size, setSize] = useState<{ width: number; height: number }>({
    width: 0,
    height: 0,
  });
  useEffect(() => {
    if (imageRef.current) {
      setSize({
        width: imageRef.current.clientWidth,
        height: imageRef.current.clientHeight,
      });
    }
  }, [imageRef]);
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [render, setRender] = useState<boolean>(false);
  useEffect(() => {
    if (canvasRef.current) {
      if (canvasRef.current) {
        const canvas = canvasRef.current;
        const context = canvas.getContext('2d');
        const image = new Image();
        image.src = genFilePath(item.images?.medium);
        image.width = size.width;
        image.height = size.height;
        image.onload = () => {
          if (!context) {
            setRender(false);
            return;
          }
          context?.drawImage(image, 0, 0, image.width, image.height);
          context.font = `18px serif`;
          context.strokeText('Animals Collection', 10, 20);
          setRender(true);
        };
      }
    }
  }, [item.images?.medium?.path, size]);
  return (
    <Stack width={1} bgcolor={'rgb(116, 139, 82)'}>
      <img
        loading="lazy"
        ref={imageRef}
        src={genFilePath(item.images?.medium)}
        alt={item.alt}
        title={item.title}
      />
    </Stack>
  );
};
