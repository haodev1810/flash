'use client';
import { Video } from '@client/components/videos/Video';
import { ItemVideoResponse } from '@libs/openapi-generator/generated';
import { Box, ImageList, ImageListItem } from '@mui/material';
import React from 'react';

interface Props {
  items: ItemVideoResponse[];
  column: number;
}
export const ListViewVideo = ({ items, column }: Props) => {
  return (
    <Box>
      <ImageList variant="masonry" cols={column} gap={8}>
        {items.map((item, index) => (
          <ImageListItem key={index}>
            <Video video={item} />
          </ImageListItem>
        ))}
      </ImageList>
    </Box>
  );
};
