'use client';
import { ItemView } from '@client/components/items/ItemView';
import { ItemResponse } from '@libs/openapi-generator/generated';
import { Box, ImageList, ImageListItem } from '@mui/material';
import React from 'react';

interface Props {
  items: ItemResponse[];
  column: number;
}
export const ListViewItems = ({ items, column }: Props) => {
  return (
    <Box>
      <ImageList variant="masonry" cols={column} gap={8}>
        {items.map((item, index) => (
          <ImageListItem key={index}>
            <ItemView item={item} />
          </ImageListItem>
        ))}
      </ImageList>
    </Box>
  );
};
