'use client';
import { Stack } from '@mui/material';
import React from 'react';

export const Empty = () => {
  return (
    <Stack
      borderRadius={4}
      border={'1px solid #eaecf0'}
      bgcolor={'#FFF'}
    ></Stack>
  );
};
