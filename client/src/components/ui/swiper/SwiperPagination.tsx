'use client';
import React from 'react';
import { Swiper, SwiperProps } from 'swiper/react';
import 'swiper/css';
import { Pagination } from 'swiper/modules';
import 'swiper/css/navigation';

interface SwiperPaginationProps extends SwiperProps {
  children?: React.ReactNode;
}
export const SwiperPagination = ({
  children,
  ...props
}: SwiperPaginationProps) => {
  return (
    <Swiper
      slidesPerView={3}
      spaceBetween={5}
      pagination={{
        clickable: true,
      }}
      modules={[Pagination]}
      className="mySwiper"
      {...props}
      breakpoints={{
        600: {
          slidesPerView: 2,
          spaceBetween: 30,
        },
        // when window width is >= 640px
        900: {
          slidesPerView: 4,
          spaceBetween: 40,
        },
        1200: {
          slidesPerView: 12,
          spaceBetween: 40,
        },
      }}
    >
      {children}
    </Swiper>
  );
};
