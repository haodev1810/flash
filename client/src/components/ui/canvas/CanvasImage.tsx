'use client';
import Link from 'next/link';
import React, { useEffect, useRef, useState } from 'react';

interface CanvasProps {
  src: string;
  href: string;
  alt: string;
  maxWidth?: number;
  maxHeight?: number;
}
interface SizeProps {
  height: number;
  width: number;
}
export const CanvasImage = ({
  src,
  maxWidth = 800,
  maxHeight = 1200,
  ...props
}: CanvasProps) => {
  const myCanvas = useRef<HTMLCanvasElement>(null);
  const [size, setSize] = useState<SizeProps>({ height: 0, width: 0 });
  useEffect(() => {
    if (src && myCanvas.current) {
      if (myCanvas.current) {
        const canvas = myCanvas.current;
        const context = canvas.getContext('2d');
        const image = new Image();
        image.src = src;
        image.alt = props.alt;
        image.width = maxWidth;
        image.style.objectFit = 'cover';
        image.onload = () => {
          const width = image.width;
          const height = image.height;
          setSize({ width: width, height: height });
          if (!context) {
            return;
          }
          context?.drawImage(image, 0, 0, width, height);
          context.font = `18px serif`;
          context.strokeText('Animals Collection', 10, 20);
        };
      }
    }
  }, [src, size, props.alt]);
  return (
    <Link
      href={props.href}
      title={props.alt}
      style={{ textDecoration: 'none' }}
    >
      {size.width == 0 && <img alt={props.alt} src={src} />}
      <canvas ref={myCanvas} height={size.height} width={size.height}></canvas>
    </Link>
  );
};
