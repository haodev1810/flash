import { genFilePath } from '@client/utils/file';
import { CollectionResponse } from '@libs/openapi-generator/generated';
import {
  Avatar,
  ImageList,
  ImageListItem,
  ImageListProps,
  Stack,
  Typography,
} from '@mui/material';
import React from 'react';

interface Props extends Partial<ImageListProps> {
  collection: CollectionResponse;
}
export const CollectionViewMedium = ({
  collection,
  children,
  ...props
}: Props) => {
  return (
    <Stack
      position={'relative'}
      sx={{
        '& .CollectionHover': {
          display: 'none',
        },
        ':hover': {
          '& .CollectionHover': {
            display: 'flex',
          },
        },
      }}
    >
      <ImageList {...props}>
        <ImageListItem cols={2} rows={2}>
          <img
            src={genFilePath(collection.main?.images?.medium)}
            loading="lazy"
            alt={collection.main?.alt}
            style={{ objectFit: 'cover' }}
            title={collection.title}
          />
        </ImageListItem>
        <ImageListItem cols={1} rows={1}>
          <img
            src={genFilePath(collection.sub?.images?.medium)}
            loading="lazy"
            alt={collection.sub?.alt}
            style={{ objectFit: 'cover' }}
            title={collection.title}
          />
        </ImageListItem>
        <ImageListItem cols={1} rows={1}>
          <img
            src={genFilePath(collection.sub2?.images?.medium)}
            loading="lazy"
            alt={collection.sub2?.alt}
            style={{ objectFit: 'cover' }}
            title={collection.title}
          />
        </ImageListItem>
      </ImageList>
      <Stack>
        <Stack
          className="CollectionHover"
          width={1}
          position="absolute"
          bottom={5}
          left={0}
          zIndex={1}
        >
          <Stack
            flexDirection="row"
            alignItems="center"
            justifyContent="space-between"
            py={2}
            px={2}
          >
            <Stack flexDirection="row" alignItems="center" gap={1}>
              <Avatar src={genFilePath(collection.owner?.avatar)} />
              <Typography
                fontSize={{ sm: 10, md: 14, lg: 16 }}
                color={'#FFF'}
                fontWeight={500}
              >
                {collection.owner?.name}
              </Typography>
            </Stack>
          </Stack>
        </Stack>
      </Stack>
    </Stack>
  );
};
