'use client';
import { genFilePath } from '@client/utils/file';
import { ItemResponse } from '@libs/openapi-generator/generated';
import {
  Box,
  IconButton,
  ImageList,
  ImageListItem,
  ImageListProps,
  Typography,
} from '@mui/material';
import React, { useState } from 'react';
import AddToPhotosIcon from '@mui/icons-material/AddToPhotos';
import { ListItemBookmarkDialog } from '@client/components/dialogs/items/ListItemBookmarkDialog';

export enum TypeChange {
  Main = 1,
  Sub = 2,
  Sub2 = 3,
}

interface Props extends Partial<ImageListProps> {
  main?: ItemResponse | null;
  sub?: ItemResponse | null;
  sub2?: ItemResponse | null;
  items: ItemResponse[];
  onChangeImage?: (item: ItemResponse, type: TypeChange) => void;
}
export const CollectionCoverUpload = ({
  main,
  sub,
  sub2,
  children,
  items,
  onChangeImage,
  ...props
}: Props) => {
  const [open, setOpen] = useState<boolean>(false);
  const [type, setType] = useState<TypeChange>();
  const onClose = () => {
    setOpen(false);
  };
  const onChooseItem = (item: ItemResponse) => {
    if (type && onChangeImage) {
      onChangeImage(item, type);
    }
    onClose();
  };
  return (
    <>
      <ImageList
        sx={{
          maxWidth: 500,
          borderRadius: 4,
        }}
        rowHeight={150}
        variant="quilted"
        {...props}
      >
        <ImageListItem
          cols={2}
          rows={2}
          sx={{
            ':hover': {
              filter: 'brightness(.9)',
            },
          }}
          component={'div'}
          onClick={() => {
            setType(TypeChange.Main);
            setOpen(true);
          }}
        >
          {main ? (
            <img
              src={genFilePath(main?.images?.medium)}
              loading="lazy"
              alt={main?.alt}
              // style={{ objectFit: 'cover' }}
              title={main?.title}
            />
          ) : (
            <Box
              display={'flex'}
              flexDirection={'column'}
              width={1}
              height={300}
              alignItems={'center'}
              justifyContent={'center'}
              sx={{
                background: 'linear-gradient(#e66465, #1623b3)',
              }}
            >
              <IconButton>
                <AddToPhotosIcon htmlColor="#fff" />
              </IconButton>
              <Typography color={'#FFF'}>Chosse Image</Typography>
            </Box>
          )}
        </ImageListItem>
        <ImageListItem
          cols={1}
          rows={1}
          sx={{
            ':hover': {
              filter: 'brightness(.9)',
            },
          }}
          component={'div'}
          onClick={() => {
            setType(TypeChange.Sub);
            setOpen(true);
          }}
        >
          {sub ? (
            <img
              src={genFilePath(sub?.images?.medium)}
              loading="lazy"
              alt={sub?.alt}
              // style={{ objectFit: 'cover' }}
              title={sub?.title}
            />
          ) : (
            <Box
              display={'flex'}
              flexDirection={'column'}
              width={1}
              height={150}
              alignItems={'center'}
              justifyContent={'center'}
              sx={{
                background: 'linear-gradient(#eea1a1, #1623b3)',
              }}
            >
              <AddToPhotosIcon htmlColor="#FFF" />
              <Typography color={'#FFF'}>Chosse Image</Typography>
            </Box>
          )}
        </ImageListItem>
        <ImageListItem
          cols={1}
          rows={1}
          sx={{
            ':hover': {
              filter: 'brightness(.9)',
            },
          }}
          component={'div'}
          onClick={() => {
            setType(TypeChange.Sub2);
            setOpen(true);
          }}
        >
          {sub2 ? (
            <img
              src={genFilePath(sub2?.images?.medium)}
              loading="lazy"
              alt={sub2?.alt}
              // style={{ objectFit: 'cover' }}
              title={sub2?.title}
            />
          ) : (
            <Box
              display={'flex'}
              flexDirection={'column'}
              width={1}
              height={150}
              alignItems={'center'}
              justifyContent={'center'}
              sx={{
                background: 'linear-gradient(#f7b9b9, #1623b3)',
              }}
            >
              <IconButton>
                <AddToPhotosIcon htmlColor="#FFF" />
              </IconButton>
              <Typography color={'#FFF'}>Chosse Image</Typography>
            </Box>
          )}
        </ImageListItem>
      </ImageList>
      <ListItemBookmarkDialog
        onChooseItem={onChooseItem}
        items={items}
        open={open}
        onClose={onClose}
      />
    </>
  );
};
