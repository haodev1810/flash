import { genFilePath } from '@client/utils/file';
import { CollectionResponse } from '@libs/openapi-generator/generated';
import { ImageList, ImageListItem, ImageListProps } from '@mui/material';
import React from 'react';

interface Props extends Partial<ImageListProps> {
  collection: CollectionResponse;
}
export const CollectionViewSmall = ({
  collection,
  children,
  ...props
}: Props) => {
  return (
    <ImageList {...props}>
      <ImageListItem cols={2} rows={1}>
        <img
          src={genFilePath(collection.main?.images?.small)}
          loading="lazy"
          alt={collection.main?.alt}
          style={{ objectFit: 'cover' }}
          title={collection.title}
        />
      </ImageListItem>
      <ImageListItem cols={1} rows={1}>
        <img
          src={genFilePath(collection.sub?.images?.small)}
          loading="lazy"
          alt={collection.sub?.alt}
          style={{ objectFit: 'cover' }}
          title={collection.title}
        />
      </ImageListItem>
      <ImageListItem cols={1} rows={1}>
        <img
          src={genFilePath(collection.sub2?.images?.small)}
          loading="lazy"
          alt={collection.sub2?.alt}
          style={{ objectFit: 'cover' }}
          title={collection.title}
        />
      </ImageListItem>
    </ImageList>
  );
};
