'use client';
import React from 'react';
import { AppNotification } from './AppNotification';
import { useAppSelector } from '@client/stores';

export const NotificationGroup = () => {
  const notifications = useAppSelector(
    (state) => state.notification.notifications
  );
  return (
    <>
      {!!notifications.length &&
        notifications.map((notification, index) => (
          <AppNotification
            id={notification.id}
            severity={notification.type}
            content={notification.content}
            index={index}
            key={index}
          />
        ))}
    </>
  );
};
