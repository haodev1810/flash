import { useAppDispatch } from '@client/stores';
import {
  AppNotification,
  addNotification,
} from '@client/stores/slices/notificationSlice';
import { v4 } from 'uuid';
import { useCallback } from 'react';
import axios from 'axios';
import {
  AppApiErrorResponse,
  ValidationExceptionResponse,
} from '@libs/openapi-generator/generated/api';

export interface NotifyHookResult {
  notify: (props?: Partial<AppNotification>) => void;
  notifyError: (props?: Partial<AppNotification>) => void;
}

const findDeepError = (errors: ValidationExceptionResponse[]): string => {
  for (const error of errors) {
    if (error.errors.length) {
      return error.errors[0].message;
    } else {
      return findDeepError(error.children);
    }
  }
  return '';
};

export const useNotify = () => {
  const dispatch = useAppDispatch();
  const notify = useCallback<NotifyHookResult['notify']>(
    (props) => {
      dispatch(
        addNotification({
          id: props?.id ?? v4(),
          content: props?.content || 'Thành công',
          type: props?.type || 'success',
        })
      );
    },
    [dispatch]
  );

  const notifyError = useCallback<NotifyHookResult['notifyError']>(
    (props) => {
      let content = props?.content ?? 'Đã có lỗi xảy ra';
      if (props?.error && axios.isAxiosError(props.error)) {
        const response: AppApiErrorResponse | undefined =
          props.error.response?.data;
        if (response && response.error) {
          if (response.error.validationErrors.length) {
            content = findDeepError(response.error.validationErrors);
          } else {
            content = response.error.message;
          }
        }
      }
      dispatch(
        addNotification({
          id: props?.id ?? v4(),
          type: props?.type ?? 'error',
          content,
        })
      );
    },
    [dispatch]
  );
  return { notify, notifyError };
};
