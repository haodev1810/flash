import { useAppDispatch } from '@client/stores';
import { removeNotification } from '@client/stores/slices/notificationSlice';
import { Alert, AlertProps, Snackbar } from '@mui/material';
import React, { SyntheticEvent, useCallback } from 'react';

export interface AppNotificationProps
  extends Omit<AlertProps, 'sx' | 'ref' | 'defaultValue'> {
  content: string;
}

interface Props extends AppNotificationProps {
  id: string;
  index: number;
}
export const AppNotification = ({ id, content, index, ...props }: Props) => {
  const dispatch = useAppDispatch();
  const handleClose = useCallback(
    (event?: SyntheticEvent | Event, reason?: string) => {
      if (reason === 'clickaway') {
        return;
      }
      dispatch(removeNotification({ id }));
    },
    [dispatch, id]
  );
  const positionTop = 40 + 52 * index;
  return (
    <Snackbar
      open
      autoHideDuration={3000}
      anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
      sx={{
        top: positionTop + 'px !important',
      }}
      onClose={handleClose}
    >
      <Alert
        severity={props.severity}
        sx={{ width: '100%' }}
        onClose={handleClose}
      >
        {content}
      </Alert>
    </Snackbar>
  );
};
