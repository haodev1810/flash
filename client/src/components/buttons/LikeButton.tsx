'use client';
import { Button, ButtonProps } from '@mui/material';
import React from 'react';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';

interface Props extends ButtonProps {
  onClick?: () => void;
}
export const LikeButton = ({ onClick, ...props }: Props) => {
  return (
    <Button
      variant="outlined"
      color="primary"
      startIcon={<FavoriteBorderIcon />}
      onClick={onClick}
      {...props}
    >
      Like
    </Button>
  );
};
