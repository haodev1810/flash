'use client';
import React, { useEffect, useRef, useState } from 'react';
import { Box, BoxProps } from '@mui/material';
import styled from '@emotion/styled';
import clsx from 'clsx';
import { isNil, omitBy } from 'lodash';
import { LoginQueryParam } from '@libs/constants/auth';

const GoogleSignInButtonContainer = styled(Box)`
  width: fit-content;
` as typeof Box;

interface LoginProps extends BoxProps {
  queryParams?: Record<LoginQueryParam, string | null>;
}
interface LoginProps extends BoxProps {}

export const GoogleSignInButton = ({
  className,
  queryParams,
  ...props
}: LoginProps) => {
  const divRef = useRef(null);
  const [clientId] = useState(process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID);
  const [loginUri] = useState(process.env.NEXT_PUBLIC_GOOGLE_CALLBACK_URL);
  const params = new URLSearchParams(
    omitBy(queryParams, isNil) as Record<string, string>
  ).toString();
  useEffect(() => {
    const script = document.createElement('script');
    script.src = 'https://accounts.google.com/gsi/client';
    script.async = true;
    script.defer = true;

    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);
  return (
    <GoogleSignInButtonContainer
      {...props}
      id="g_id_onload"
      ref={divRef}
      className={clsx('g_id_signin', className)}
      data-client_id={clientId}
      data-login_uri={`${loginUri}${params ? `?${params}` : ''}`}
    />
  );
};
