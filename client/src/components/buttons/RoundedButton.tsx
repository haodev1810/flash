'use client';
import LoadingButton, { LoadingButtonProps } from '@mui/lab/LoadingButton';
import { FC, forwardRef } from 'react';
import styled from '@emotion/styled';

const RoundedButtonContainer = styled(LoadingButton)`
  border-radius: 100px;
  text-transform: none;
` as unknown as typeof LoadingButton;

export type RoundedButtonProps = LoadingButtonProps & {
  component?: unknown;
  href?: string;
  to?: unknown;
  target?: string;
};

// eslint-disable-next-line react/display-name
export const RoundedButton: FC<RoundedButtonProps> = forwardRef(
  ({ variant = 'contained', ...props }, ref) => {
    return <RoundedButtonContainer variant={variant} ref={ref} {...props} />;
  }
);
