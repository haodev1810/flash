'use client';
import { Button, ButtonProps } from '@mui/material';
import React from 'react';
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';

interface Props extends ButtonProps {
  onClick?: () => void;
}
export const DownloadButton = ({ onClick, ...props }: Props) => {
  return (
    <Button
      variant="contained"
      color="info"
      startIcon={<BookmarkBorderIcon />}
      onClick={onClick}
      {...props}
    >
      Download Free
    </Button>
  );
};
