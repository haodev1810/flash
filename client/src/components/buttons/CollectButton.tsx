'use client';
import { Button, ButtonProps } from '@mui/material';
import React from 'react';
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';

interface Props extends ButtonProps {
  onClick?: () => void;
}
export const CollectButton = ({ onClick, ...props }: Props) => {
  return (
    <Button
      variant="outlined"
      color="secondary"
      startIcon={<BookmarkBorderIcon />}
      onClick={onClick}
      {...props}
    >
      Collect
    </Button>
  );
};
