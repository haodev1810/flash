'use client';
import {
  Box,
  FormHelperText,
  Grid,
  ImageList,
  ImageListItem,
  Stack,
  TextField,
  Typography,
  FormControl,
  FormControlLabel,
  StackProps,
} from '@mui/material';
import React, { useEffect, useState } from 'react';
import {
  CollectionCoverUpload,
  TypeChange,
} from '../ui/collections/CollectionCoverUpload';
import { useForm, SubmitHandler, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import {
  CollectionResponse,
  ItemResponse,
} from '@libs/openapi-generator/generated';
import { RoundedButton } from '../buttons/RoundedButton';
import { MarkdownEditor } from '../ckeditor/MarkdownEditor';
import Switch from '@mui/material/Switch';
import { useNotify } from '../notifications/hooks';
import slugify from 'slugify';
import { useUpdateCollectionMutation } from '@client/apis/hooks/collections/useUpdateCollectionMutation';
import { ItemCollection } from '../items/ItemCollection';
import { usePopItemFromCollectionMutation } from '@client/apis/hooks/collections/usePopItemFromCollectionMutation';

const schema = yup
  .object({
    title: yup.string().required().max(255),
    slug: yup.string().required().max(255),
    description: yup.string().required(),
    public: yup.boolean().required().default(false),
    enable_download: yup.boolean().required().default(false),
    main_id: yup.number().required().positive().min(1),
    sub_id: yup.number().required().positive().min(1),
    sub2_id: yup.number().required().positive().min(1),
  })
  .required();
type FormData = yup.InferType<typeof schema>;
interface FormUpdateCollectionProps extends StackProps {
  collection: CollectionResponse;
  items: ItemResponse[];
}
export const FormUpdateCollection = ({
  collection,
  items,
  ...props
}: FormUpdateCollectionProps) => {
  const [listItems, setListItems] = useState<ItemResponse[]>(items);
  const updateCollection = useUpdateCollectionMutation();
  const popItemCollection = usePopItemFromCollectionMutation();
  const { notify, notifyError } = useNotify();
  const [main, setMain] = useState<ItemResponse | null>(collection.main);
  const [sub, setSub] = useState<ItemResponse | null>(collection.sub);
  const [sub2, setSub2] = useState<ItemResponse | null>(collection.sub2);
  const [idsSelected, setIdsSelected] = useState<number[]>([]);
  useEffect(() => {
    setListItems(items);
  }, [items]);
  const {
    register,
    setValue,
    control,
    formState: { errors, isValid },
    handleSubmit,
  } = useForm<FormData>({
    mode: 'onBlur',
    resolver: yupResolver(schema),
    defaultValues: {
      title: collection.title,
      slug: collection.slug,
      description: collection.description,
      main_id: collection?.main?.id,
      sub_id: collection?.sub?.id,
      sub2_id: collection.sub2?.id,
      public: collection.public,
      enable_download: collection.enable_download,
    },
  });
  const onChangeImage = (item: ItemResponse, type: TypeChange) => {
    if (item && type == TypeChange.Main) {
      setMain(item);
      setValue('main_id', item.id);
    }
    if (item && type == TypeChange.Sub) {
      setSub(item);
      setValue('sub_id', item.id);
    }
    if (item && type == TypeChange.Sub2) {
      setSub2(item);
      setValue('sub2_id', item.id);
    }
    setIdsSelected([...idsSelected, item.id]);
  };
  const onSubmit: SubmitHandler<FormData> = async (data) => {
    try {
      const response = await updateCollection
        .mutateAsync({
          id: collection.id,
          updateCollectionDto: {
            title: data.title,
            slug: data.slug,
            main_id: data.main_id,
            description: data.description,
            sub_id: data.sub_id,
            sub2_id: data.sub2_id,
            public: data.public,
            enable_download: data.enable_download,
          },
        })
        .then((res) => res.data);
      if (response) {
        notify({ content: 'Success' });
      }
    } catch (error) {
      notifyError({ error });
    }
  };
  const onDelete = async (item: ItemResponse) => {
    try {
      await popItemCollection.mutateAsync({
        popItemsDto: {
          collection_id: collection.id,
          itemIds: [item.id],
        },
      });
      setListItems(listItems.filter((i) => i.id != item.id));
    } catch (error) {
      notifyError({ error });
    }
  };
  return (
    <Stack
      width={1}
      gap={2}
      py={2}
      component={'form'}
      onSubmit={handleSubmit(onSubmit)}
    >
      <Stack>
        <Typography
          variant="h3"
          fontSize={30}
          textAlign={'center'}
          fontWeight={600}
        >
          Update collection
        </Typography>
      </Stack>
      <Grid container boxSizing={'border-box'} columnSpacing={2} rowSpacing={2}>
        <Grid item xs={12} md={6}>
          <Stack>
            <Typography>Title *</Typography>
            <Controller
              {...register('title')}
              control={control}
              render={({ field: { value, onChange }, fieldState }) => (
                <FormControl fullWidth>
                  <TextField
                    value={value}
                    onChange={(e) => {
                      onChange(e);
                      setValue(
                        'slug',
                        slugify(e.target.value, { lower: true })
                      );
                    }}
                    id="title"
                    name="title"
                    fullWidth
                    variant="filled"
                    error={!!errors.title}
                    helperText={errors.title?.message}
                  />
                </FormControl>
              )}
            />
          </Stack>
        </Grid>
        <Grid item xs={12} md={6}>
          <Stack>
            <Typography>Slug *</Typography>
            <FormControl fullWidth>
              <TextField
                {...register('slug')}
                required
                id="slug"
                name="slug"
                fullWidth
                variant="filled"
                error={!!errors.slug}
                helperText={errors.slug?.message}
                disabled
              />
            </FormControl>
          </Stack>
        </Grid>
        <Grid item xs={12} md={6}>
          <Stack>
            <Typography variant="h3" fontSize={24} fontWeight={600}>
              Cover
            </Typography>
            <CollectionCoverUpload
              items={items.filter(
                (i) => ![main?.id, sub?.id, sub2?.id].includes(i.id)
              )}
              main={main}
              sub={sub}
              sub2={sub2}
              onChangeImage={onChangeImage}
            />
          </Stack>
        </Grid>
        <Grid item xs={12} md={6}>
          <Stack>
            <Grid container>
              <Grid item xs={6}>
                <FormControl>
                  <Controller
                    {...register('public')}
                    control={control}
                    render={({ field: { value, onChange }, fieldState }) => (
                      <FormControlLabel
                        value={value}
                        checked={value}
                        onChange={(e, c) => {
                          onChange(c);
                        }}
                        control={<Switch />}
                        label="Public"
                      />
                    )}
                  />
                </FormControl>
              </Grid>
              <Grid item xs={6}>
                <FormControl>
                  <Controller
                    {...register('enable_download')}
                    control={control}
                    render={({ field: { value, onChange }, fieldState }) => (
                      <FormControlLabel
                        value={value}
                        checked={value}
                        onChange={(e, c) => {
                          onChange(c);
                        }}
                        control={<Switch />}
                        label="Download"
                      />
                    )}
                  />
                </FormControl>
              </Grid>
            </Grid>
          </Stack>
          <Stack>
            <Typography>Description</Typography>
            <FormControl>
              <Controller
                {...register('description')}
                control={control}
                render={({ field: { value, onChange }, fieldState }) => (
                  <>
                    <MarkdownEditor
                      onChangeData={onChange}
                      value={value || ''}
                      config={{
                        placeholder: 'Write any something...',
                      }}
                    />
                    <FormHelperText error={!!fieldState.error}>
                      {fieldState.error?.message}
                    </FormHelperText>
                  </>
                )}
              />
            </FormControl>
          </Stack>
        </Grid>
      </Grid>
      <Stack flexDirection={'row'} justifyContent={'flex-end'} gap={1}>
        <RoundedButton type="submit" disabled={!isValid}>
          Update Collection
        </RoundedButton>
      </Stack>
      <Stack>
        <Box>
          <ImageList variant="masonry" cols={3} gap={8}>
            {listItems.map((item, index) => (
              <ImageListItem key={index}>
                <ItemCollection onDelete={onDelete} item={item} />
              </ImageListItem>
            ))}
          </ImageList>
        </Box>
      </Stack>
    </Stack>
  );
};
