'use client';
import { useAppDispatch, useAppSelector } from '@client/stores';
import {
  Box,
  FormHelperText,
  Grid,
  ImageList,
  ImageListItem,
  Stack,
  TextField,
  Typography,
  FormControl,
  FormControlLabel,
} from '@mui/material';
import React, { useEffect, useState } from 'react';
import { SelectItem } from '../items/SelectItem';
import {
  CollectionCoverUpload,
  TypeChange,
} from '../ui/collections/CollectionCoverUpload';
import { useForm, SubmitHandler, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { ItemResponse } from '@libs/openapi-generator/generated';
import { RoundedButton } from '../buttons/RoundedButton';
import { MarkdownEditor } from '../ckeditor/MarkdownEditor';
import Switch from '@mui/material/Switch';
import { useCreateCollectionMutation } from '@client/apis/hooks/collections/useCreateCollectionMutation';
import { useNotify } from '../notifications/hooks';
import { useRouter } from 'next/navigation';
import { removeListItems } from '@client/stores/slices/collectionSlice';
import slugify from 'slugify';

const schema = yup
  .object({
    title: yup.string().required().max(255),
    slug: yup.string().required().max(255),
    description: yup.string(),
    public: yup.boolean().required().default(false),
    enable_download: yup.boolean().required().default(false),
    main_id: yup.number().required().positive().min(1),
    sub_id: yup.number().required().positive().min(1),
    sub2_id: yup.number().required().positive().min(1),
    itemIds: yup.array(yup.number().required()).required().nonNullable(),
  })
  .required();
type FormData = yup.InferType<typeof schema>;

export const FormCreateCollection = () => {
  const [isChooseAll, setIsChooseAll] = useState<boolean>(false);
  const dispatch = useAppDispatch();
  const router = useRouter();
  const { notify, notifyError } = useNotify();
  const createCollection = useCreateCollectionMutation();
  const favoriteItems = useAppSelector((state) => state.collection.items);
  const ids = useAppSelector((state) => state.collection.selectedIds);
  const [main, setMain] = useState<ItemResponse | null>();
  const [sub, setSub] = useState<ItemResponse | null>();
  const [sub2, setSub2] = useState<ItemResponse | null>();
  const [idsSelected, setIdsSelected] = useState<number[]>([]);
  useEffect(() => {
    setIdsSelected(ids);
  }, [ids]);
  const {
    register,
    setValue,
    control,
    formState: { errors, isValid },
    handleSubmit,
  } = useForm<FormData>({
    mode: 'onBlur',
    resolver: yupResolver(schema),
    defaultValues: {
      title: '',
      slug: '',
      description: '',
      main_id: 0,
      sub_id: 0,
      sub2_id: 0,
      public: false,
      enable_download: false,
      itemIds: ids || [],
    },
  });
  useEffect(() => {
    setValue('itemIds', idsSelected);
  }, [idsSelected, setValue]);
  const onChangeImage = (item: ItemResponse, type: TypeChange) => {
    if (item && type == TypeChange.Main) {
      setMain(item);
      setValue('main_id', item.id);
    }
    if (item && type == TypeChange.Sub) {
      setSub(item);
      setValue('sub_id', item.id);
    }
    if (item && type == TypeChange.Sub2) {
      setSub2(item);
      setValue('sub2_id', item.id);
    }
    setIdsSelected([...idsSelected, item.id]);
  };
  const onSelectAll = async () => {
    if (!isChooseAll) {
      setIdsSelected(favoriteItems.map((i) => i.id));
      setIsChooseAll(true);
    } else {
      setIdsSelected([]);
      setIsChooseAll(false);
    }
  };
  const onSubmit: SubmitHandler<FormData> = async (data) => {
    try {
      const response = await createCollection
        .mutateAsync({
          createCollectionDto: {
            title: data.title,
            slug: data.slug,
            main_id: data.main_id,
            description: data.description,
            sub_id: data.sub_id,
            sub2_id: data.sub2_id,
            public: data.public,
            enable_download: data.enable_download,
            itemIds: data.itemIds,
          },
        })
        .then((res) => res.data);
      if (response) {
        await dispatch(removeListItems([...data.itemIds]));
        notify({ content: 'Success' });
        router.push('/collections/me');
      }
    } catch (error) {
      notifyError({ error });
    }
  };
  return (
    <Stack
      width={1}
      gap={2}
      py={2}
      component={'form'}
      onSubmit={handleSubmit(onSubmit)}
    >
      <Stack>
        <Typography
          variant="h3"
          fontSize={30}
          textAlign={'center'}
          fontWeight={600}
        >
          Create a new collection
        </Typography>
      </Stack>
      <Grid container boxSizing={'border-box'} columnSpacing={2} rowSpacing={2}>
        <Grid item xs={12} md={6}>
          <Stack>
            <Typography>Title *</Typography>
            <FormControl fullWidth>
              <Controller
                {...register('title')}
                control={control}
                render={({ field: { value, onChange }, fieldState }) => (
                  <TextField
                    value={value}
                    onChange={(e) => {
                      onChange(e);
                      setValue(
                        'slug',
                        slugify(e.target.value, { lower: true })
                      );
                    }}
                    id="title"
                    name="title"
                    size="small"
                    fullWidth
                    variant="filled"
                    error={!!errors.title}
                    helperText={errors.title?.message}
                    sx={{
                      background:
                        'linear-gradient(rgba(247, 185, 185,0.8), rgba(22, 35, 179,0.4))',
                    }}
                  />
                )}
              />
            </FormControl>
          </Stack>
        </Grid>
        <Grid item xs={12} md={6}>
          <Stack>
            <Typography>Slug *</Typography>
            <FormControl fullWidth>
              <TextField
                {...register('slug')}
                required
                id="slug"
                name="slug"
                fullWidth
                variant="filled"
                size="small"
                error={!!errors.slug}
                helperText={errors.slug?.message}
                disabled
                sx={{
                  background:
                    'linear-gradient(rgba(247, 185, 185,0.8), rgba(22, 35, 179,0.4))',
                }}
              />
            </FormControl>
          </Stack>
        </Grid>
        <Grid item xs={12} md={6}>
          <Stack>
            <Typography variant="h3" fontSize={24} fontWeight={600}>
              Cover
            </Typography>
            <CollectionCoverUpload
              items={favoriteItems.filter(
                (i) => ![main?.id, sub?.id, sub2?.id].includes(i.id)
              )}
              main={main}
              sub={sub}
              sub2={sub2}
              onChangeImage={onChangeImage}
            />
          </Stack>
        </Grid>
        <Grid item xs={12} md={6}>
          <Stack>
            <Grid container>
              <Grid item xs={6}>
                <FormControl>
                  <Controller
                    {...register('public')}
                    control={control}
                    render={({ field: { value, onChange }, fieldState }) => (
                      <FormControlLabel
                        value={value}
                        onChange={(e, c) => {
                          onChange(c);
                        }}
                        control={<Switch />}
                        label="Public"
                      />
                    )}
                  />
                </FormControl>
              </Grid>
              <Grid item xs={6}>
                <FormControl>
                  <Controller
                    {...register('enable_download')}
                    control={control}
                    render={({ field: { value, onChange }, fieldState }) => (
                      <FormControlLabel
                        value={value}
                        onChange={(e, c) => {
                          onChange(c);
                        }}
                        control={<Switch />}
                        label="Download"
                      />
                    )}
                  />
                </FormControl>
              </Grid>
            </Grid>
          </Stack>
          <Stack>
            <Typography>Description</Typography>
            <FormControl>
              <Controller
                {...register('description')}
                control={control}
                render={({ field: { value, onChange }, fieldState }) => (
                  <Stack>
                    <MarkdownEditor
                      onChangeData={onChange}
                      value={value || ''}
                      config={{
                        placeholder: 'Write any something...',
                      }}
                    />
                    <FormHelperText error={!!fieldState.error}>
                      {fieldState.error?.message}
                    </FormHelperText>
                  </Stack>
                )}
              />
            </FormControl>
          </Stack>
        </Grid>
      </Grid>
      <Stack flexDirection={'row'} justifyContent={'flex-end'} gap={1}>
        <RoundedButton onClick={onSelectAll}>
          {' '}
          {isChooseAll ? 'Cancle' : 'Choose all'}
        </RoundedButton>
        <RoundedButton type="submit" disabled={!isValid}>
          Create Collection
        </RoundedButton>
      </Stack>
      <Stack>
        <Box>
          <ImageList variant="masonry" cols={3} gap={8}>
            {favoriteItems.map((item, index) => (
              <ImageListItem key={index}>
                <SelectItem
                  idsSelected={idsSelected}
                  setIdsSelected={setIdsSelected}
                  item={item}
                />
              </ImageListItem>
            ))}
          </ImageList>
        </Box>
      </Stack>
    </Stack>
  );
};
