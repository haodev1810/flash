/* eslint-disable @typescript-eslint/no-explicit-any */
'use client';
import React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import { useAppDispatch, useAppSelector } from '@client/stores';
import { closeLoginDialog } from '@client/stores/slices/loginSlice';
import { GoogleSignInButton } from '../buttons/GoogleLogin';

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="down" ref={ref} {...props} />;
});

export const FormLoginDialog = () => {
  const dispath = useAppDispatch();
  const open = useAppSelector((state) => state.loginChecking.open);
  return (
    <React.Fragment>
      {open && (
        <Dialog
          open={open}
          TransitionComponent={Transition}
          keepMounted
          onClose={() => {
            dispath(closeLoginDialog());
          }}
        >
          <GoogleSignInButton />
        </Dialog>
      )}
    </React.Fragment>
  );
};
