'use client';
import { Button, ButtonProps } from '@mui/material';
import React from 'react';
import LogoutIcon from '@mui/icons-material/Logout';

export const FormLogout = ({ ...props }: ButtonProps) => {
  return (
    <form method="POST" action={'/api/auth/logout'}>
      <Button
        type="submit"
        variant="text"
        startIcon={<LogoutIcon />}
        {...props}
      >
        Logout
      </Button>
    </form>
  );
};
