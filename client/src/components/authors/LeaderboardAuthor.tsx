import { Avatar, Grid, Stack, Typography } from '@mui/material';
import Image from 'next/image';
import React from 'react';
import Link from '../ui/link/Link';
import {
  LeaderboardResponse,
  StoredFileResponse,
} from '@libs/openapi-generator/generated';
import { genFilePath } from '@client/utils/file';

interface Props {
  index: number;
  ranked: LeaderboardResponse;
}
export const LeaderboardAuthor = ({ ranked, index }: Props) => {
  return (
    <Link href={'/authors/' + ranked.author.username} underline="none">
      <Grid container boxSizing={'border-box'}>
        <Grid item xs={12} md={5}>
          <Stack flexDirection={'row'} alignItems={'start'} gap={2}>
            <Typography
              variant="h1"
              fontSize={{ xs: 39, lg: 49 }}
              fontWeight={500}
              color={'#2C343E'}
              pr={4}
            >
              {index}
            </Typography>
            <Stack flexDirection={'row'} alignItems={'center'} gap={3}>
              <Avatar
                src={genFilePath(ranked.author.avatar_large)}
                sx={{ width: 80, height: 80, objectFit: 'cover' }}
              />
              <Stack>
                <Typography variant="h3" fontSize={23} color={'#2C343E'}>
                  {ranked.author.name}
                </Typography>
                <Typography variant="body1" color={'#7F7F7F'}>
                  {ranked.total_view} views
                </Typography>
              </Stack>
            </Stack>
          </Stack>
        </Grid>
        <Grid item xs={12} md={7}>
          <Stack flexDirection={'row'} gap={1}>
            {ranked.items.map((item, index) => (
              <Stack
                key={index}
                sx={{ ':hover': { filter: 'brightness(.8)' } }}
                position={'relative'}
              >
                <Image
                  src={genFilePath(item.images?.medium as StoredFileResponse)}
                  alt={item.alt}
                  unoptimized
                  width={180}
                  height={180}
                  style={{
                    objectFit: 'cover',
                    borderRadius: '8px',
                  }}
                  loading="lazy"
                />
                {index == 3 && (
                  <Stack
                    position={'absolute'}
                    top={0}
                    right={0}
                    bottom={0}
                    left={0}
                    alignItems={'center'}
                    justifyContent={'center'}
                    zIndex={2}
                    bgcolor={'rgba(0, 0, 0,0.7)'}
                    borderRadius={'8px'}
                  >
                    <Typography fontSize={49} color={'#FFF'} fontWeight={400}>
                      +{ranked.total}
                    </Typography>
                    <Typography fontSize={18} color={'#FFF'}>
                      See All Media
                    </Typography>
                  </Stack>
                )}
              </Stack>
            ))}
          </Stack>
        </Grid>
      </Grid>
    </Link>
  );
};
