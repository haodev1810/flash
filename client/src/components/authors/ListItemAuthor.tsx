'use client';
import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getListItems } from '@client/apis/hooks/items/useGetListItem';
import { useScrollToLoadMoreBody } from '@client/hooks/useScrollToLoadMoreBody';
import { ItemsApiListsRequest } from '@libs/openapi-generator/generated';
import { Stack } from '@mui/material';
import { useInfiniteQuery } from '@tanstack/react-query';
import React, { useEffect, useMemo } from 'react';
import { ListViewItems } from '../ui/list-view/ListViewItems';

interface Props {
  initQueryItems: ItemsApiListsRequest;
}
export const ListItemAuthor = ({ initQueryItems }: Props) => {
  const { shouldFetchNextPage } = useScrollToLoadMoreBody({
    threshold: 1000,
  });
  const { fetchNextPage, data } = useInfiniteQuery({
    queryKey: [QueryKeys.GetItemSimilar],
    queryFn: ({ pageParam }) =>
      getListItems({ ...initQueryItems, page: Number(pageParam) }),
    initialPageParam: 1,
    getNextPageParam: (lastPage) => lastPage.pagination.page + 1,
    getPreviousPageParam: (firstPage) => firstPage.pagination.page,
  });
  const items = useMemo(() => {
    return data?.pages.map((page) => page.data).flat() ?? [];
  }, [data?.pages]);
  useEffect(() => {
    if (shouldFetchNextPage) {
      fetchNextPage().catch();
    }
  }, [fetchNextPage, shouldFetchNextPage]);
  return (
    <Stack>
      <ListViewItems items={items} column={3} />
    </Stack>
  );
};
