/* eslint-disable @typescript-eslint/no-explicit-any */
'use client';
import React from 'react';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import { Avatar, Stack, Tooltip, Typography } from '@mui/material';
import { ItemResponse } from '@libs/openapi-generator/generated';
import { genFilePath } from '@client/utils/file';
import Image from 'next/image';

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="down" ref={ref} {...props} />;
});
interface Props extends DialogProps {
  open: boolean;
  onClose: () => void;
  item: ItemResponse;
}

export const MoreInformationItemDialog = ({
  open,
  onClose,
  item,
  ...props
}: Props) => {
  return (
    <React.Fragment>
      <Dialog
        BackdropProps={{
          sx: {
            bgcolor: 'rgba(0, 0, 0, 0.865)',
          },
        }}
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={onClose}
        {...props}
        PaperProps={{
          elevation: 1,
          sx: {
            bgcolor: '#000',
            minWidth: { xs: '100%', sm: 400, md: 600 },
          },
        }}
      >
        <Stack px={4} py={3} gap={3} position={'relative'}>
          <Stack order={1} flexDirection={'row'} gap={3} alignItems={'center'}>
            <Image
              unoptimized
              src={genFilePath(item.images?.medium)}
              alt={item.alt}
              width={120}
              height={120}
              style={{ objectFit: 'cover' }}
            />
            <Stack sx={{ wordBreak: 'break-word' }}>
              <Typography
                color={'#FFF'}
                variant="h3"
                fontSize={26}
                fontWeight={600}
              >
                {item.title}
              </Typography>
              <Typography color={'#BFBFBF'} variant="body2">
                Created at {new Date(item.created_at).toDateString()}
              </Typography>
              <Stack>
                <Typography color={'#BFBFBF'} variant="body2">
                  Description
                </Typography>
                <Typography color={'#BFBFBF'} variant="caption">
                  {item.description}
                </Typography>
              </Stack>
            </Stack>
          </Stack>
          <Stack order={2} flexDirection={'row'} flexWrap={'wrap'}>
            <Stack flexGrow={1}>
              <Typography color={'#BFBFBF'}>Views</Typography>
              <Typography color={'#FFF'} fontSize={21}>
                {item.views}
              </Typography>
            </Stack>
            <Stack flexGrow={1}>
              <Typography color={'#BFBFBF'}>Likes</Typography>
              <Typography color={'#FFF'} fontSize={21}>
                0
              </Typography>
            </Stack>
            <Stack flexGrow={1}>
              <Typography color={'#BFBFBF'}>Downloads</Typography>
              <Typography color={'#FFF'} fontSize={21}>
                {item.downloads}
              </Typography>
            </Stack>
            <Stack flexGrow={1}>
              <Typography color={'#BFBFBF'}>Author</Typography>
              <Tooltip title={item?.author?.name}>
                <Avatar src={genFilePath(item.author?.avatar)} />
              </Tooltip>
            </Stack>
          </Stack>
        </Stack>
      </Dialog>
    </React.Fragment>
  );
};
