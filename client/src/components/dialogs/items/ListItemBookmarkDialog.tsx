/* eslint-disable @typescript-eslint/no-explicit-any */
'use client';
import React from 'react';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import { Box, ImageList, ImageListItem, Stack } from '@mui/material';
import { ItemResponse } from '@libs/openapi-generator/generated';
import { genFilePath } from '@client/utils/file';

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="down" ref={ref} {...props} />;
});
interface Props extends DialogProps {
  open: boolean;
  onClose: () => void;
  items: ItemResponse[];
  onChooseItem: (item: ItemResponse) => void;
}

export const ListItemBookmarkDialog = ({
  open,
  onClose,
  items,
  onChooseItem,
  ...props
}: Props) => {
  return (
    <React.Fragment>
      <Dialog
        BackdropProps={{
          sx: {
            bgcolor: 'rgba(0, 0, 0, 0.865)',
          },
        }}
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={onClose}
        {...props}
        PaperProps={{
          elevation: 1,
          sx: {
            bgcolor: '#000',
            minWidth: { xs: '100%', sm: 400, md: 600 },
          },
        }}
      >
        <Stack px={4} py={3} gap={3} position={'relative'}>
          <Box>
            <ImageList variant="masonry" cols={3} gap={8}>
              {items.map((item, index) => (
                <ImageListItem
                  key={index}
                  component={'div'}
                  onClick={() => onChooseItem(item)}
                  sx={{ cursor: 'pointer' }}
                >
                  <img
                    loading="lazy"
                    src={genFilePath(item.images?.medium)}
                    alt={item.alt}
                    title={item.title}
                  />
                </ImageListItem>
              ))}
            </ImageList>
          </Box>
        </Stack>
      </Dialog>
    </React.Fragment>
  );
};
