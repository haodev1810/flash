/* eslint-disable @typescript-eslint/no-explicit-any */
'use client';
import React, { useMemo } from 'react';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import { Stack, Typography } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import { useGetListMyCollection } from '@client/apis/hooks/collections/useGetListMyCollection';
import { ThumbCollection } from './ThumbCollection';
import { useNotify } from '@client/components/notifications/hooks';
import { usePushItemToCollectionMutation } from '@client/apis/hooks/collections/usePushItemToCollectionMutation';
import { useRouter } from 'next/navigation';
import { CollectionResponse } from '@libs/openapi-generator/generated';
import slugify from 'slugify';
import { useAppDispatch } from '@client/stores';
import { setSelectedIds } from '@client/stores/slices/collectionSlice';

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="down" ref={ref} {...props} />;
});
interface Props extends DialogProps {
  open: boolean;
  onClose: () => void;
  itemIds: number[];
}
export const DialogCollection = ({
  open,
  onClose,
  itemIds,
  ...props
}: Props) => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const { notify, notifyError } = useNotify();
  const pushItemToCollection = usePushItemToCollectionMutation();
  const { data } = useGetListMyCollection({
    page: 1,
    limit: 15,
    image: 'small',
  });
  const collectons = useMemo(() => {
    return data?.data ?? [];
  }, [data?.data]);
  const onPushItems = async (collection: CollectionResponse) => {
    try {
      await pushItemToCollection.mutateAsync({
        pushItemsDto: {
          collection_id: collection.id,
          itemIds: itemIds,
        },
      });
      notify({ content: 'Add to collection success' });
      onClose();
      router.push('/collections/' + slugify(collection.slug));
    } catch (error) {
      notifyError({ error });
    }
  };
  return (
    <React.Fragment>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={onClose}
        {...props}
        PaperProps={{
          elevation: 1,
          sx: {
            p: 3,
            bgcolor: 'rgba(35, 59, 75, 0.502)',
            minWidth: { xs: '100%', sm: 400 },
          },
        }}
      >
        <Stack flexDirection={'row'} flexWrap={'wrap'} rowGap={2} columnGap={2}>
          <Stack
            sx={{
              ':hover': {
                bgcolor: '#1e323f',
                cursor: 'pointer',
              },
            }}
            component={'div'}
            onClick={async () => {
              await dispatch(setSelectedIds(itemIds));
              return router.push('/collections/create');
            }}
            boxShadow={'-moz-initial'}
            bgcolor={'#233b4b'}
            width={120}
            height={120}
            alignItems={'center'}
            justifyContent={'center'}
          >
            <AddIcon fontSize="large" htmlColor="#FFF" />
            <Typography letterSpacing={1} color={'#FFF'} textAlign={'center'}>
              New Collection
            </Typography>
          </Stack>
          {collectons.map((collection, index) => (
            <ThumbCollection
              onPushItem={onPushItems}
              key={index}
              collection={collection}
            />
          ))}
        </Stack>
      </Dialog>
    </React.Fragment>
  );
};
