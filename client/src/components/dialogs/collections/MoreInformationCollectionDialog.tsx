/* eslint-disable @typescript-eslint/no-explicit-any */
'use client';
import React from 'react';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import { Avatar, Stack, Switch, Tooltip, Typography } from '@mui/material';
import { CollectionResponse } from '@libs/openapi-generator/generated';
import { CollectionViewSmall } from '@client/components/ui/collections/CollectionViewSmall';
import { genFilePath } from '@client/utils/file';

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="down" ref={ref} {...props} />;
});
interface Props extends DialogProps {
  open: boolean;
  onClose: () => void;
  collection: CollectionResponse;
}

export const MoreInformationCollectionDialog = ({
  open,
  onClose,
  collection,
  ...props
}: Props) => {
  return (
    <React.Fragment>
      <Dialog
        BackdropProps={{
          sx: {
            bgcolor: 'rgba(0, 0, 0, 0.865)',
          },
        }}
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={onClose}
        {...props}
        PaperProps={{
          elevation: 1,
          sx: {
            bgcolor: '#000',
            minWidth: { xs: '100%', sm: 400, md: 600 },
          },
        }}
      >
        <Stack px={4} py={3} gap={3} position={'relative'}>
          <Stack order={1} flexDirection={'row'} gap={3} alignItems={'center'}>
            <CollectionViewSmall
              sx={{ maxWidth: 120, borderRadius: 4 }}
              collection={collection}
            />
            <Stack sx={{ wordBreak: 'break-word' }}>
              <Typography
                color={'#FFF'}
                variant="h3"
                fontSize={26}
                fontWeight={600}
              >
                {collection.title}
              </Typography>
              <Typography color={'#BFBFBF'} variant="body2">
                Created at {new Date(collection.created_at).toDateString()}
              </Typography>
              <Stack>
                <Typography color={'#BFBFBF'} variant="body2">
                  Description
                </Typography>
                <Typography color={'#BFBFBF'} variant="caption">
                  {collection.description}
                </Typography>
              </Stack>
            </Stack>
          </Stack>
          <Stack order={2} flexDirection={'row'} flexWrap={'wrap'}>
            <Stack flexGrow={1}>
              <Typography color={'#BFBFBF'}>Views</Typography>
              <Typography color={'#FFF'} fontSize={21}>
                {collection.views}
              </Typography>
            </Stack>
            <Stack flexGrow={1}>
              <Typography color={'#BFBFBF'}>Likes</Typography>
              <Typography color={'#FFF'} fontSize={21}>
                0
              </Typography>
            </Stack>
            <Stack flexGrow={1}>
              <Typography color={'#BFBFBF'}>Downloads</Typography>
              <Typography color={'#FFF'} fontSize={21}>
                {collection.downloads}
              </Typography>
            </Stack>
            <Stack flexGrow={1}>
              <Typography color={'#BFBFBF'}>Author</Typography>
              <Tooltip title={collection.owner?.name}>
                <Avatar src={genFilePath(collection.owner?.avatar)} />
              </Tooltip>
            </Stack>
            <Stack flexGrow={1}>
              <Typography color={'#BFBFBF'}>Public</Typography>
              <Switch
                sx={{ color: '#FFF' }}
                defaultChecked
                checked={collection.public}
              />
            </Stack>
          </Stack>
        </Stack>
      </Dialog>
    </React.Fragment>
  );
};
