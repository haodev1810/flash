'use client';
import { Divider, Stack, StackProps, Typography } from '@mui/material';
import React, { useState } from 'react';
import Image from 'next/image';
import truncate from 'lodash/truncate';
import { CollectionResponse } from '@libs/openapi-generator/generated';
import { genFilePath } from '@client/utils/file';

interface CollectionSmallProps extends StackProps {
  collection: CollectionResponse;
  onPushItem: (collection: CollectionResponse) => void;
}
export const ThumbCollection = ({
  collection,
  onPushItem,
}: CollectionSmallProps) => {
  const [hover, setHover] = useState<boolean>(false);
  return (
    <Stack
      sx={{ cursor: 'pointer' }}
      component={'div'}
      onMouseMove={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      onClick={() => onPushItem(collection)}
    >
      <Stack width={120} height={120} position={'relative'}>
        <Image
          unoptimized
          width={120}
          height={60}
          style={{
            borderTopRightRadius: 15,
            borderTopLeftRadius: 15,
            objectFit: 'cover',
          }}
          src={genFilePath(collection.main?.images?.small)}
          alt={collection.title}
        />
        <Divider sx={{ bgcolor: '#FFF', height: 2 }} />
        <Stack flexDirection={'row'} boxSizing={'border-box'}>
          <Image
            unoptimized
            width={60}
            height={60}
            src={genFilePath(collection.sub?.images?.small)}
            alt={collection.title}
            style={{
              borderBottomLeftRadius: 15,
              objectFit: 'cover',
            }}
          />
          <Divider sx={{ width: 2, bgcolor: '#FFF' }} />
          <Image
            unoptimized
            width={60}
            height={60}
            src={genFilePath(collection.sub2?.images?.small)}
            style={{ borderBottomRightRadius: 15, objectFit: 'cover' }}
            alt={collection.title}
          />
        </Stack>
        <Stack
          display={hover ? 'flex' : 'none'}
          bgcolor={'rgba(0, 6, 10, 0.702)'}
          zIndex={10}
          position={'absolute'}
          top={0}
          left={0}
          right={0}
          bottom={-3}
        >
          <Stack
            width={1}
            height={1}
            alignItems={'center'}
            justifyContent={'center'}
          >
            <Typography textAlign={'center'} color={'#FFF'}>
              Add To Collection
            </Typography>
          </Stack>
        </Stack>
      </Stack>
      <Stack>
        <Typography textAlign={'center'} color={'#fff'} fontWeight={500}>
          {truncate(collection.title, { length: 16, omission: '...' })}
        </Typography>
      </Stack>
    </Stack>
  );
};
