'use client';
import { Container, Stack } from '@mui/material';
import React, { PropsWithChildren } from 'react';
import { Header } from './Header';
import { Footer } from './Footer';
import { NotificationGroup } from '../notifications/NotificationGroup';
import { FormLoginDialog } from '../forms/FormLogin';

interface AppLayoutProp extends PropsWithChildren {
  custom?: boolean;
}
export const AppLayout = ({ children, custom = false }: AppLayoutProp) => {
  return (
    <Stack width={1} flex={1}>
      <Header custom={custom} />
      <section>
        <Stack
          sx={{
            minHeight: 'calc(100vh - 350px - 75px)',
          }}
        >
          <Container maxWidth="xl">{children}</Container>
        </Stack>
      </section>
      <Footer />
      <NotificationGroup />
      <FormLoginDialog />
    </Stack>
  );
};
