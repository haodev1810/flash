'use client';
import {
  Box,
  Chip,
  Container,
  Divider,
  FormControl,
  Grid,
  IconButton,
  Paper,
  Stack,
  TextField,
  Typography,
  Avatar,
} from '@mui/material';
import React, { useEffect, useRef, useState } from 'react';
import InputAdornment from '@mui/material/InputAdornment';
import InsertPhotoOutlinedIcon from '@mui/icons-material/InsertPhotoOutlined';
import SearchIcon from '@mui/icons-material/Search';
import { ClickAwayListener } from '@mui/base/ClickAwayListener';
import Link from 'next/link';
import { useAppDispatch, useAppSelector } from '@client/stores';
import { useParams, useRouter } from 'next/navigation';
import { Params } from 'next/dist/shared/lib/router/utils/route-matcher';
import slugify from 'slugify';
import {
  clearHistorySearch,
  pushDataSearch,
} from '@client/stores/slices/searchStorySlice';
import { useDebounce } from '@client/hooks/useDebounce';
import { QueryClient } from '@tanstack/react-query';
import { QueryKeys } from '@client/apis/hooks/QueryKeys';
import { getTags } from '@client/apis/hooks/tags';
import { TagsList200Response } from '@libs/openapi-generator/generated';
import { ListCollectionSmall } from '../collections/ListCollectionSmall';
import { useGetListTopics } from '@client/apis/hooks/topics/useGetListTopics';

interface QueryParams extends Params {
  query: string;
}

export const GlobalSearch = () => {
  const { data } = useGetListTopics({
    limit: 10,
    page: 1,
    trending: true,
  });
  const [resultQuery, setResultQuery] = useState<TagsList200Response>();
  const queryClient = new QueryClient();
  const [off, setOff] = useState<boolean>(true);
  const textRef = useRef<HTMLDivElement>(null);
  const dispatch = useAppDispatch();
  const router = useRouter();
  const [search, setSearch] = useState<string>();
  const debounceSearch = useDebounce(search, 500);
  const history = useAppSelector((state) => state.search);
  const params = useParams<QueryParams>();
  const handleClick = () => {
    setOff(false);
  };
  const handleClickAway = () => {
    setOff(true);
  };
  const onSearch = () => {
    if (debounceSearch) {
      dispatch(pushDataSearch({ searchCurrent: debounceSearch }));
      router.push('/searchs/' + slugify(debounceSearch));
    }
  };
  useEffect(() => {
    if (debounceSearch) {
      queryClient
        .fetchQuery({
          queryKey: [QueryKeys.GetTags],
          queryFn: () => getTags({ page: 1, limit: 5, query: debounceSearch }),
        })
        .then((res) => {
          if (res) {
            setResultQuery(res);
          }
        });
    }
  }, [debounceSearch]);
  useEffect(() => {
    if (textRef.current) {
      textRef.current.addEventListener('keypress', (key: KeyboardEvent) => {
        if (
          key.keyCode == 13 &&
          debounceSearch != null &&
          slugify(debounceSearch, { trim: true }) !== ''
        ) {
          dispatch(pushDataSearch({ searchCurrent: debounceSearch }));
          router.push('/searchs/' + slugify(debounceSearch));
        }
      });
    }
  }, [debounceSearch, textRef]);
  return (
    <ClickAwayListener onClickAway={handleClickAway}>
      <Box width={1} alignItems={'center'} position={'relative'}>
        <FormControl fullWidth>
          <TextField
            ref={textRef}
            size="small"
            sx={{
              bgcolor: !off ? '#FFF' : '#f7f7f7',
              borderTopRightRadius: !off ? 8 : 0,
              borderTopLeftRadius: !off ? 8 : 0,
              boxShadow: 'box-shadow: 0 8px 15px rgba(0,0,0,.04)',
              border: off ? 'none' : '1px solid #7F7F7F1A',
              '& fieldset': { border: 'none' },
              ':focus': {
                bgcolor: '#fff',
              },
            }}
            defaultValue={params?.query}
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            placeholder="Search any animals..."
            inputProps={{
              style: {
                color: '#7F7F7F',
                fontWeight: 500,
                fontSize: 18,
              },
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment
                  position="start"
                  sx={{
                    position: 'relative',
                    height: 1,
                    ':after': {
                      content: "''",
                      position: 'absolute',
                      width: '1px',
                      height: off ? '20px' : '40px',
                      right: 0,
                      bgcolor: off ? '#949494' : '#7F7F7F1A',
                    },
                  }}
                >
                  <Stack
                    flexDirection={'row'}
                    alignItems={'center'}
                    gap={1}
                    px={1}
                  >
                    <InsertPhotoOutlinedIcon />
                    <Typography
                      variant="body2"
                      color={'#7F7F7F'}
                      fontSize={18}
                      fontWeight={500}
                    >
                      Photos
                    </Typography>
                  </Stack>
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment
                  position="end"
                  sx={{
                    position: 'relative',
                    height: 1,
                    ':after': {
                      content: "''",
                      position: 'absolute',
                      width: '1px',
                      height: off ? '20px' : '40px',
                      left: 0,
                      bgcolor: off ? '#949494' : '#7F7F7F1A',
                    },
                  }}
                >
                  <IconButton onClick={onSearch} sx={{ bgcolor: 'none' }}>
                    <SearchIcon sx={{ ':hover': { color: 'green' } }} />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            onClick={handleClick}
          />
        </FormControl>
        {!off && (
          <Stack position={'absolute'} width={1} boxSizing={'border-box'}>
            <Paper
              sx={{
                zIndex: 999,
                flexDirection: 'column',
                width: 1,
                borderRadius: 0,
                borderBottomLeftRadius: 8,
                borderBottomRightRadius: 8,
                gap: 3,
              }}
              elevation={1}
            >
              <Container>
                <Stack py={2}>
                  {resultQuery?.data.map((tag, i) => (
                    <Typography
                      key={i}
                      href={'/searchs/' + slugify(tag.search_term)}
                      component={Link}
                      sx={{ textDecoration: 'none' }}
                      fontSize={18}
                      fontWeight={500}
                    >
                      {tag.search_term}
                    </Typography>
                  ))}
                </Stack>
              </Container>
              <Divider />
              {history.searchRecentlys.length > 0 && (
                <Container sx={{ py: 2 }}>
                  <Stack gap={1}>
                    <Stack
                      flexDirection={'row'}
                      alignItems={'center'}
                      justifyContent={'space-between'}
                    >
                      <Typography fontSize={20} fontWeight={500}>
                        Recent searches
                      </Typography>
                      <Box
                        component={'span'}
                        onClick={() => {
                          dispatch(clearHistorySearch());
                        }}
                        fontSize={16}
                        fontWeight={500}
                        color="#7f7f7f"
                        sx={{ cursor: 'pointer' }}
                      >
                        Clear
                      </Box>
                    </Stack>
                    <Grid container rowSpacing={2} columnSpacing={2}>
                      {history.searchRecentlys.map((search, i) => (
                        <Grid key={i} item xs={12} sm={6} md={2.5}>
                          <Chip
                            component={Link}
                            href={'/searchs/' + search}
                            variant="outlined"
                            sx={{
                              fontWeight: 500,
                              borderRadius: 2,
                              fontSize: 16,
                              color: '#7F7F7F',
                              p: 2,
                            }}
                            label={search}
                            deleteIcon={<SearchIcon />}
                            onDelete={() => {}}
                          />
                        </Grid>
                      ))}
                    </Grid>
                  </Stack>
                </Container>
              )}
              <Container sx={{ py: 2 }}>
                <ListCollectionSmall />
              </Container>
              <Container sx={{ py: 2 }}>
                <Stack gap={2}>
                  <Stack
                    flexDirection={'row'}
                    alignItems={'center'}
                    justifyContent={'space-between'}
                  >
                    <Typography fontSize={20} fontWeight={500}>
                      Trendings Topics
                    </Typography>
                  </Stack>
                  <Stack
                    flexDirection="row"
                    flexWrap="wrap"
                    rowGap={1}
                    columnGap={2}
                  >
                    {data?.data.map((topic, i) => (
                      <Chip
                        component={Link}
                        href={'/searchs/' + topic.slug}
                        key={i}
                        avatar={
                          <Avatar
                            src={
                              'https://source.unsplash.com/random/?' +
                              topic.slug.split('-').join(',')
                            }
                            alt=""
                          />
                        }
                        variant="outlined"
                        label={topic.title}
                        sx={{
                          fontWeight: 600,
                          borderRadius: 4,
                          py: 3,
                          cursor: 'pointer',
                        }}
                      />
                    ))}
                  </Stack>
                </Stack>
              </Container>
            </Paper>
          </Stack>
        )}
      </Box>
    </ClickAwayListener>
  );
};
