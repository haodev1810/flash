'use client';
import { Button, Container, Grid, Stack, Typography } from '@mui/material';
import React from 'react';
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import LanguageIcon from '@mui/icons-material/Language';
import { Logo } from './Logo';

export const Footer = () => {
  return (
    <footer>
      <Stack
        width={1}
        minHeight={350}
        bgcolor={'#000'}
        color={'#FFF'}
        boxSizing={'border-box'}
      >
        <Container maxWidth={'xl'} sx={{ pt: 6 }}>
          <Grid container columnSpacing={2} rowSpacing={2}>
            <Grid item xs={12} sm={6} md={3} lg={3}>
              <Stack gap={2}>
                <Logo />
                <Typography variant="h1" fontSize={18} fontWeight={400}>
                  Free photos shared by talented creators
                </Typography>
                <Stack gap={2}>
                  <Typography>My Social</Typography>
                  <Stack flexDirection="row" gap={1} alignItems="center">
                    <Typography>
                      <FacebookIcon fontSize="large" htmlColor="#FFF" />
                    </Typography>
                    <Typography>
                      <TwitterIcon fontSize="large" htmlColor="#FFF" />
                    </Typography>
                    <Typography>
                      <InstagramIcon fontSize="large" htmlColor="#FFF" />
                    </Typography>
                  </Stack>
                </Stack>
              </Stack>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Stack gap={3}>
                <Typography fontSize={23} fontWeight={500}>
                  FFlash
                </Typography>
                <Stack gap={1}>
                  <Typography fontSize={21} fontWeight={400}>
                    Free Stock Photos
                  </Typography>
                  <Typography fontSize={21} fontWeight={400}>
                    Free Photo
                  </Typography>
                  <Typography fontSize={21} fontWeight={400}>
                    Collections
                  </Typography>
                  <Typography fontSize={21} fontWeight={400}>
                    Popular searches
                  </Typography>
                  <Typography fontSize={21} fontWeight={400}>
                    Posts
                  </Typography>
                </Stack>
              </Stack>
            </Grid>
            <Grid item xs={12} sm={6} md={2}>
              <Stack gap={3}>
                <Typography fontSize={23} fontWeight={500}>
                  Company
                </Typography>
                <Stack gap={1}>
                  <Typography fontSize={21} fontWeight={400}>
                    About
                  </Typography>
                  <Typography fontSize={21} fontWeight={400}>
                    Blog
                  </Typography>
                  <Typography fontSize={21} fontWeight={400}>
                    FAQ
                  </Typography>
                  <Typography fontSize={21} fontWeight={400}>
                    Image
                  </Typography>
                </Stack>
              </Stack>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Stack gap={3}>
                <Typography fontSize={23} fontWeight={500}>
                  Free Stock Photos
                </Typography>
                <Stack flexDirection={'row'} flexWrap={'wrap'} gap={2}>
                  <Button
                    variant="outlined"
                    sx={{
                      color: '#F7F7F7',
                      fontWeight: 500,
                      borderRadius: 2,
                      borderColor: '#7F7F7F',
                      fontSize: 14,
                      textTransform: 'none',
                    }}
                  >
                    {' '}
                    Black and white photography
                  </Button>
                  <Button
                    variant="outlined"
                    sx={{
                      color: '#F7F7F7',
                      fontWeight: 500,
                      borderRadius: 2,
                      borderColor: '#7F7F7F',
                      fontSize: 14,
                      textTransform: 'none',
                    }}
                  >
                    {' '}
                    Happy birthday images
                  </Button>
                  <Button
                    variant="outlined"
                    sx={{
                      color: '#F7F7F7',
                      fontWeight: 500,
                      borderRadius: 2,
                      borderColor: '#7F7F7F',
                      fontSize: 14,
                      textTransform: 'none',
                    }}
                  >
                    {' '}
                    Animals Images
                  </Button>
                  <Button
                    variant="outlined"
                    sx={{
                      color: '#F7F7F7',
                      fontWeight: 500,
                      borderRadius: 2,
                      borderColor: '#7F7F7F',
                      fontSize: 14,
                      textTransform: 'none',
                    }}
                  >
                    {' '}
                    Dogs Images
                  </Button>
                </Stack>
              </Stack>
            </Grid>
          </Grid>
          <Grid container columnSpacing={2} rowSpacing={2}>
            <Grid item xs={12} md={3}>
              <Typography color={'#7F7F7F'}>© 2024 Animals</Typography>
            </Grid>
            <Grid item xs={12} md={6}>
              <Stack
                flexDirection="row"
                alignItems="center"
                gap={5}
                flexWrap={'wrap'}
              >
                <Typography fontSize={18} color={'#7F7F7F'}>
                  Terms of Use
                </Typography>
                <Typography fontSize={18} color={'#7F7F7F'}>
                  Privacy Policy
                </Typography>
                <Typography fontSize={18} color={'#7F7F7F'}>
                  License
                </Typography>
                <Typography fontSize={18} color={'#7F7F7F'} width={150}>
                  Imprint
                  <br />
                  Cookies Policy
                </Typography>
              </Stack>
            </Grid>
            <Grid item xs={12} md={3}>
              <Button
                sx={{ px: 3, bgcolor: '#7F7F7F', color: '#fff' }}
                startIcon={<LanguageIcon htmlColor="#FFF" />}
              >
                English
              </Button>
            </Grid>
          </Grid>
        </Container>
      </Stack>
    </footer>
  );
};
