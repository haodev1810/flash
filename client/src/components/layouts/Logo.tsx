import React from 'react';
import Link from '../ui/link/Link';

export const Logo = () => {
  return (
    <Link href={'/'}>
      <img src={'/assets/logo.png'} width={80} alt="Logo" />
    </Link>
  );
};
