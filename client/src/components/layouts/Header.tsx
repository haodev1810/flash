'use client';
import { Box, Container, Grid, Stack, Typography } from '@mui/material';
import React, { useMemo } from 'react';
import { GlobalSearch } from './GlobalSearch';
import { Menu } from '../dropdowns/menu/Menu';
import { Logo } from './Logo';
import { useGetListTopics } from '@client/apis/hooks/topics/useGetListTopics';
import Link from 'next/link';

interface PropsHeader {
  custom?: boolean;
}
export const Header = ({ custom = false }: PropsHeader) => {
  const { data } = useGetListTopics({
    limit: 10,
    page: 1,
    trending: true,
  });
  return (
    <header>
      <Box
        sx={{
          boxShadow: '0 1px 0 #f7f7f7',
          position: 'relative',
          background: custom
            ? 'linear-gradient(180deg,rgba(0,0,0,.8),transparent 100%,transparent 65%,rgba(0,0,0,.75))'
            : 'transparent',
        }}
        height={custom ? 500 : 60}
        py={1}
      >
        {custom && (
          <Stack
            height={500}
            position={'absolute'}
            top={0}
            left={0}
            right={0}
            bottom={0}
            zIndex={-1}
          >
            <img
              alt=""
              loading="lazy"
              height={500}
              src="https://source.unsplash.com/random"
              style={{
                zIndex: -2,
                objectFit: 'cover',
                overflowClipMargin: 'content-box',
              }}
            />
          </Stack>
        )}
        <Container maxWidth={'xl'} sx={{ height: 1 }}>
          <Stack
            height={1}
            width={1}
            flexDirection={custom ? 'column' : 'row'}
            alignItems={custom ? 'flex-start' : 'center'}
            // justifyContent={'space-between'}
          >
            <Grid
              container
              columnSpacing={2}
              alignItems={'center'}
              boxSizing={'border-box'}
              width={1}
            >
              <Grid item xs={10} md={2} alignItems={'center'}>
                <Logo />
              </Grid>
              {!custom && (
                <Grid
                  item
                  sx={(theme) => ({
                    [theme.breakpoints.down('md')]: { display: 'none' },
                  })}
                  md={6}
                  alignItems={'center'}
                >
                  <GlobalSearch />
                </Grid>
              )}
              <Grid
                item
                xs={2}
                md={custom ? 10 : 4}
                flexDirection={'row'}
                justifyContent={'flex-end'}
              >
                <Menu custom={custom} />
              </Grid>
            </Grid>
            {custom && (
              <Stack
                height={1}
                width={1}
                alignItems={'center'}
                justifyContent={'center'}
              >
                <Stack width={{ md: '80%', sm: '95%', lg: '70%' }} gap={3}>
                  <Typography
                    fontWeight={600}
                    variant="h3"
                    fontSize={{ xs: 16, lg: 30 }}
                    color={'#f7F7F7'}
                  >
                    The best free stock photos, royalty free images shared by
                    creators.
                  </Typography>
                  <GlobalSearch />
                  <Stack color={'#FFF'} fontWeight={600}>
                    <Typography>
                      Trendings:{' '}
                      {data?.data.map((topic, i) => (
                        <Link key={i} href={'/searchs/' + topic.slug}>
                          {topic.title}
                          {i + 1 < data.data.length && ','}
                        </Link>
                      ))}
                    </Typography>
                  </Stack>
                </Stack>
              </Stack>
            )}
          </Stack>
        </Container>
      </Box>
    </header>
  );
};
