'use client';
import { useAccountQuery } from '@client/apis/hooks/users';
import { useAppDispatch } from '@client/stores';
import { setAccount } from '@client/stores/slices/userSlice';
import React, { PropsWithChildren, useEffect } from 'react';

interface Props extends PropsWithChildren {
  isLogin: boolean;
}
const LoginProvider = ({ children }: PropsWithChildren) => {
  const dispatch = useAppDispatch();
  const accountQuery = useAccountQuery();
  if (accountQuery.error) {
    const form = document.createElement('form');
    form.action = '/api/auth/logout';
    form.method = 'POST';
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
  }
  const account = accountQuery.data?.data;
  useEffect(() => {
    if (account) {
      dispatch(setAccount(account));
    }
  }, [dispatch, account]);
  return <>{children}</>;
};
const NonLoginProvider = ({ children }: PropsWithChildren) => {
  return <>{children}</>;
};
export const AuthProvider = ({ isLogin, children }: Props) => {
  return (
    <>
      {isLogin ? (
        <LoginProvider>{children}</LoginProvider>
      ) : (
        <NonLoginProvider>{children}</NonLoginProvider>
      )}
    </>
  );
};
