'use client';
import React, { PropsWithChildren, useEffect } from 'react';
import { useAppDispatch } from '@client/stores';
import { decryptedSearchData } from '@client/stores/slices/searchStorySlice';
import { decryptedCollectionData } from '@client/stores/slices/collectionSlice';

export default function LocalStorageProvider({ children }: PropsWithChildren) {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(decryptedSearchData());
    dispatch(decryptedCollectionData());
  }, [dispatch]);
  return <>{children}</>;
}
