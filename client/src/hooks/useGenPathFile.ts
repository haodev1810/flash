'use client';
import { StoredFileResponse } from '@libs/openapi-generator/generated';
import { useEffect, useState } from 'react';

const baseURL = `${process.env.NEXT_PUBLIC_API_TELEGRAM}${process.env.NEXT_PUBLIC_TELEGRAM_BOT_TOKEN}/`;
export const genPathFileBlob = (file: StoredFileResponse) => {
  return [baseURL, file.path].join('')
  // return fetch([baseURL, file.path].join(''))
  //   .then((res) => {
  //     return res.blob();
  //   })
  //   .then((res) => {
  //     return URL.createObjectURL(res);
  //   });
};

// export const useGenPathFileBlob = (file: StoredFileResponse) => {
//   const [blob, setBlob] = useState<string>();
//   useEffect(() => {
//     genPathFileBlob(file).then((res) => setBlob(res));
//   }, [file]);
//   return blob;
// };
