'use client';
import { useAppDispatch } from '@client/stores';
import { useCallback } from 'react';
import { useIsLogin } from './useIsLogin';
import {
  closeLoginDialog,
  openLoginDialog,
} from '@client/stores/slices/loginSlice';

export const useLoginChecking = () => {
  const dispatch = useAppDispatch();
  const isLogin = useIsLogin();
  const check = useCallback(() => {
    if (!isLogin) {
      dispatch(openLoginDialog());
      return false;
    } else {
      return true;
    }
  }, [dispatch, isLogin]);
  const close = useCallback(() => {
    dispatch(closeLoginDialog());
  }, [dispatch]);
  return { check, close };
};
