'use client';
import { RefObject, useEffect, useRef } from 'react';
import { useRafState } from 'react-use';
import { off, on } from 'react-use/lib/misc/util';

interface ScrollToLoadMoreHookDependencies {
  threshold?: number;
}

interface ScrollToLoadMoreHookResult<T extends HTMLElement> {
  ref: RefObject<T>;
  shouldFetchNextPage: boolean;
}

export const useScrollToLoadMore = <T extends HTMLElement>({
  threshold = 50,
}: ScrollToLoadMoreHookDependencies = {}): ScrollToLoadMoreHookResult<T> => {
  const ref = useRef<T>(null);
  if (process.env.NODE_ENV === 'development') {
    if (typeof ref !== 'object' || typeof ref.current === 'undefined') {
      console.error('`useScrollToLoadMore` expects a single ref argument.');
    }
  }

  const [shouldFetchNextPage, setShouldFetchNextPage] =
    useRafState<boolean>(false);

  useEffect(() => {
    const handler = () => {
      if (ref.current) {
        setShouldFetchNextPage(
          ref.current.scrollHeight -
            ref.current.scrollTop -
            ref.current.offsetHeight <=
            threshold
        );
      }
    };

    if (ref.current) {
      on(ref.current, 'scroll', handler, {
        capture: false,
        passive: true,
      });
    }

    return () => {
      if (ref.current) {
        off(ref.current, 'scroll', handler);
      }
    };
  }, [ref, setShouldFetchNextPage, threshold]);

  return {
    shouldFetchNextPage,
    ref,
  };
};
