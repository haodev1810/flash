'use client';
import { useEffect } from 'react';
import { useRafState } from 'react-use';

interface ScrollToLoadMoreHookDependencies {
  threshold?: number;
}

interface ScrollToLoadMoreHookResult {
  shouldFetchNextPage: boolean;
}

export const useScrollToLoadMoreBody = ({
  threshold = 50,
}: ScrollToLoadMoreHookDependencies = {}): ScrollToLoadMoreHookResult => {
  const [shouldFetchNextPage, setShouldFetchNextPage] =
    useRafState<boolean>(false);
  const handleScroll = () => {
    setShouldFetchNextPage(
      document.documentElement.offsetHeight -
        window.innerHeight -
        document.documentElement.scrollTop <
        350 + threshold
    );
  };
  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, [setShouldFetchNextPage, threshold]);

  return {
    shouldFetchNextPage,
  };
};
