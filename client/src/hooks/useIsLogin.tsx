'use client';
import { useAppSelector } from '@client/stores';
import React from 'react';

export const useIsLogin = () => {
  const account = useAppSelector((state) => state.account.account);
  if (account) {
    return true;
  }
  return false;
};
