import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';
import searchReducer from './slices/searchStorySlice';
import notificationReducer from './slices/notificationSlice';
import userReducer from './slices/userSlice';
import collectionReducer from './slices/collectionSlice';
import loginReducer from './slices/loginSlice';

export const store = configureStore({
  reducer: {
    search: searchReducer,
    notification: notificationReducer,
    account: userReducer,
    collection: collectionReducer,
    loginChecking: loginReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
