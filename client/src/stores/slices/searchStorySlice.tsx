import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import * as CryptoJS from 'crypto-js';

export interface SearchState {
  searchRecentlys: string[];
  searchCurrent: string;
}

const initialState: SearchState = {
  searchRecentlys: [],
  searchCurrent: '',
};

export const searchStorySlice = createSlice({
  name: 'search-history',
  initialState: initialState,
  reducers: {
    pushDataSearch: (
      state,
      action: PayloadAction<Pick<SearchState, 'searchCurrent'>>
    ) => {
      const current = action.payload.searchCurrent;
      state.searchCurrent = current;
      if (!state.searchRecentlys.includes(current)) {
        state.searchRecentlys = [current, ...state.searchRecentlys];
      }
      const ciphertext = CryptoJS.AES.encrypt(
        JSON.stringify({ ...state }),
        process.env.NEXT_PUBLIC_PRIVATE_CRYPTO_KEY as string
      ).toString();
      localStorage.setItem('SEARCH', ciphertext);
    },
    clearHistorySearch: (state) => {
      state.searchRecentlys = [];
      const ciphertext = CryptoJS.AES.encrypt(
        JSON.stringify({ ...state }),
        process.env.NEXT_PUBLIC_PRIVATE_CRYPTO_KEY as string
      ).toString();
      localStorage.setItem('SEARCH', ciphertext);
    },
    decryptedSearchData: (state) => {
      const cipherText = localStorage.getItem('SEARCH') || '';
      if (cipherText) {
        const bytes = CryptoJS.AES.decrypt(
          cipherText,
          process.env.NEXT_PUBLIC_PRIVATE_CRYPTO_KEY as string
        );
        const decryptedData = JSON.parse(
          bytes.toString(CryptoJS.enc.Utf8)
        ) as SearchState;
        state.searchCurrent = decryptedData.searchCurrent;
        state.searchRecentlys = decryptedData.searchRecentlys;
      } else {
        state = initialState;
      }
    },
  },
});

export default searchStorySlice.reducer;
export const { pushDataSearch, clearHistorySearch, decryptedSearchData } =
  searchStorySlice.actions;
