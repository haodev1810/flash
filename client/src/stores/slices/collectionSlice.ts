import { ItemResponse } from '@libs/openapi-generator/generated';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import * as CryptoJS from 'crypto-js';

export interface CollectionItemState {
  items: ItemResponse[];
  selectedIds: number[];
}

const initialState: CollectionItemState = {
  items: [],
  selectedIds: [],
};

export const collectionSlice = createSlice({
  name: 'collection-item',
  initialState: initialState,
  reducers: {
    pushItem: (state, action: PayloadAction<ItemResponse>) => {
      const item = action.payload;
      const exitItem = state.items.map((i) => i.id).includes(item.id);
      if (!exitItem) {
        state.items = [item, ...state.items];
        const ciphertext = CryptoJS.AES.encrypt(
          JSON.stringify({ ...state }),
          process.env.NEXT_PUBLIC_PRIVATE_CRYPTO_KEY as string
        ).toString();
        localStorage.setItem('COLLECT', ciphertext);
      }
    },
    removeListItems: (state, action: PayloadAction<number[]>) => {
      const ids = action.payload;
      const itemsRemainder = state.items.filter((i) => !ids.includes(i.id));
      state.items = itemsRemainder;
      const ciphertext = CryptoJS.AES.encrypt(
        JSON.stringify({ ...state }),
        process.env.NEXT_PUBLIC_PRIVATE_CRYPTO_KEY as string
      ).toString();
      localStorage.setItem('COLLECT', ciphertext);
    },
    removeItem: (state, action: PayloadAction<Pick<ItemResponse, 'id'>>) => {
      const itemsRemainder = state.items.filter(
        (i) => i.id != action.payload.id
      );
      state.items = itemsRemainder;
      const ciphertext = CryptoJS.AES.encrypt(
        JSON.stringify({ ...state }),
        process.env.NEXT_PUBLIC_PRIVATE_CRYPTO_KEY as string
      ).toString();
      localStorage.setItem('COLLECT', ciphertext);
    },
    clear: (state) => {
      state.items = [];
      const ciphertext = CryptoJS.AES.encrypt(
        JSON.stringify({ ...state }),
        process.env.NEXT_PUBLIC_PRIVATE_CRYPTO_KEY as string
      ).toString();
      localStorage.setItem('COLLECT', ciphertext);
    },
    setSelectedIds: (state, action: PayloadAction<ItemResponse['id'][]>) => {
      state.selectedIds = action.payload;
    },
    decryptedCollectionData: (state) => {
      const cipherText = localStorage.getItem('COLLECT') || '';
      if (cipherText) {
        const bytes = CryptoJS.AES.decrypt(
          cipherText,
          process.env.NEXT_PUBLIC_PRIVATE_CRYPTO_KEY as string
        );
        const decryptedData = JSON.parse(
          bytes.toString(CryptoJS.enc.Utf8)
        ) as CollectionItemState;
        state.items = decryptedData.items;
      } else {
        state = initialState;
      }
    },
  },
});

export default collectionSlice.reducer;
export const {
  pushItem,
  clear,
  decryptedCollectionData,
  removeItem,
  removeListItems,
  setSelectedIds,
} = collectionSlice.actions;
