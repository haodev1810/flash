import { AccountResponse } from '@libs/openapi-generator/generated/api';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface UserState {
  account: AccountResponse | null;
}

const initialState: UserState = {
  account: null,
};

export const accountSlice = createSlice({
  name: 'account',
  initialState: initialState,
  reducers: {
    setAccount: (state, action: PayloadAction<AccountResponse>) => {
      state.account = action.payload;
    },
  },
});

export default accountSlice.reducer;
export const { setAccount } = accountSlice.actions;
