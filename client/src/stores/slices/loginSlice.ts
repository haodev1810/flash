import { createSlice } from '@reduxjs/toolkit';

export interface CollectionItemState {
  open: boolean;
}

const initialState: CollectionItemState = {
  open: false,
};

export const LoginSlice = createSlice({
  name: 'login-slice',
  initialState: initialState,
  reducers: {
    openLoginDialog: (state) => {
      state.open = true;
    },
    closeLoginDialog: (state) => {
      state.open = false;
    },
  },
});

export default LoginSlice.reducer;
export const { openLoginDialog, closeLoginDialog } = LoginSlice.actions;
