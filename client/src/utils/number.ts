export const numberConvert = (number: number, toFixed: number) => {
  if (number > Math.pow(10, 5)) {
    return (number / Math.pow(10, 5)).toFixed(toFixed) + 'M';
  }
  if (number > 1000) {
    return (number / 1000).toFixed(toFixed) + 'K';
  }
  if (number < 1000) {
    return number.toString();
  }
};
