import { StoredFileDriver } from '@libs/constants/entities/StoredFile';
import { StoredFileResponse } from '@libs/openapi-generator/generated';

export const genFilePath = (file?: StoredFileResponse | null) => {
  if (!file) return '';
  switch (file.driver) {
    case StoredFileDriver.Minio: {
      return [process.env.NEXT_PUBLIC_MINIO_PATH, file.path].join('/');
    }
    case StoredFileDriver.Cloudinary:
    case StoredFileDriver.Remote: {
      return file.path;
    }
    default: {
      return '';
    }
  }
};
