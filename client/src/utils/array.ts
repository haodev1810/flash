export const chunkArray = <T>(items: T[] | undefined, n: number): T[][] => {
  if (items) {
    const arrays: T[][] = [];
    for (let i = 0; i < n; i++) {
      // Sử dụng filter để tạo mảng con với các phần tử thỏa mãn điều kiện
      arrays[i] = items.filter((item, index) => {
        if (index % n === i) {
          return item;
        }
      });
    }
    return arrays;
  }
  return [];
};
