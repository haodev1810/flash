export const isValidStoryRedirectUrl = (redirectUrl?: string | null) => {
  if (!redirectUrl) return false;
  try {
    const url = new URL(redirectUrl);
    return url.origin === process.env.NEXT_PUBLIC_BASE_URL;
  } catch (e) {
    return false;
  }
};
