import { AuthGoogleApiFactory } from '@libs/openapi-generator/generated/api';
import type { NextApiRequest, NextApiResponse } from 'next';
import { GoogleCallbackData } from '@client/types/google';
import Cookies from 'cookies';
import { JwtCookieToken, LoginQueryParam } from '@libs/constants/auth';
import * as jwt from 'jsonwebtoken';
import { isValidStoryRedirectUrl } from '@client/utils/auth';

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const redirectUrlParam =
    (req.query[LoginQueryParam.redirectUrl] as string) ?? null;
  const redirectUrl = isValidStoryRedirectUrl(redirectUrlParam)
    ? redirectUrlParam
    : null;
  const referUrl = req.headers.referer || '/';
  if (req.method !== 'POST') {
    return res.redirect(405, '/405');
  }
  const data = req.body as GoogleCallbackData;
  if (typeof data !== 'object') {
    return res.redirect(400, '/400');
  }
  try {
    const googleAuthFactory = AuthGoogleApiFactory(
      undefined,
      process.env.NEXT_PUBLIC_API_BASE_URL
    );
    const response = await googleAuthFactory.login({
      credential: data.credential,
    });
    const cookies = new Cookies(req, res);
    const token = response.data.data.token;
    const decodedToken = jwt.decode(token);
    if (!decodedToken || typeof decodedToken === 'string') {
      return res.status(401).send('UnAuthenticated!');
    }
    cookies.set(JwtCookieToken, token, {
      httpOnly: true,
      sameSite: 'lax',
      maxAge: ((decodedToken.exp ?? 0) - (decodedToken.iat ?? 0)) * 1000,
      domain: process.env.COOKIE_DOMAIN || undefined,
    });
    if (redirectUrl) {
      return res.redirect(303, redirectUrl);
    } else {
      return res.redirect(303, referUrl);
    }
  } catch (e) {
    return res.redirect(401, '/401');
  }
};

export default handler;
