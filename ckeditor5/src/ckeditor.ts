/**
 * @license Copyright (c) 2014-2023, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

import { ClassicEditor } from '@ckeditor/ckeditor5-editor-classic';

import { Alignment } from '@ckeditor/ckeditor5-alignment';
import { Autoformat } from '@ckeditor/ckeditor5-autoformat';
import { Autosave } from '@ckeditor/ckeditor5-autosave';
import { Bold, Italic, Underline } from '@ckeditor/ckeditor5-basic-styles';
import { BlockQuote } from '@ckeditor/ckeditor5-block-quote';
import { CloudServices } from '@ckeditor/ckeditor5-cloud-services';
import type { EditorConfig } from '@ckeditor/ckeditor5-core';
import { Essentials } from '@ckeditor/ckeditor5-essentials';
import { FindAndReplace } from '@ckeditor/ckeditor5-find-and-replace';
import {
  FontBackgroundColor,
  FontColor,
  FontFamily,
  FontSize,
} from '@ckeditor/ckeditor5-font';
import { Heading } from '@ckeditor/ckeditor5-heading';
import { GeneralHtmlSupport } from '@ckeditor/ckeditor5-html-support';
import {
  AutoImage,
  Image,
  ImageCaption,
  ImageInsert,
  ImageResize,
  ImageStyle,
  ImageToolbar,
  ImageUpload,
  PictureEditing,
} from '@ckeditor/ckeditor5-image';
import { Indent } from '@ckeditor/ckeditor5-indent';
import { AutoLink, Link, LinkImage } from '@ckeditor/ckeditor5-link';
import { List } from '@ckeditor/ckeditor5-list';
import { Markdown } from '@ckeditor/ckeditor5-markdown-gfm';
import { MediaEmbed } from '@ckeditor/ckeditor5-media-embed';
import { Paragraph } from '@ckeditor/ckeditor5-paragraph';
import { PasteFromOffice } from '@ckeditor/ckeditor5-paste-from-office';
import { Style } from '@ckeditor/ckeditor5-style';
import {
  Table,
  TableCaption,
  TableCellProperties,
  TableColumnResize,
  TableProperties,
  TableToolbar,
} from '@ckeditor/ckeditor5-table';
import { TextTransformation } from '@ckeditor/ckeditor5-typing';
import { Undo } from '@ckeditor/ckeditor5-undo';
import { SpecialCharacters } from '@ckeditor/ckeditor5-special-characters';
import { SpecialCharactersEssentials } from '@ckeditor/ckeditor5-special-characters';

function SpecialCharactersEmoji(editor: any) {
  return editor.plugins.get('SpecialCharacters').addItems(
    'Emoji',
    [
      { title: 'smiley face', character: '😊' },
      { title: 'rocket', character: '🚀' },
      { title: 'wind blowing face', character: '🌬️' },
      { title: 'floppy disk', character: '💾' },
      { title: 'heart', character: '❤️' },
      { title: 'thumbs up', character: '👍' },
      { title: 'star', character: '⭐' },
      { title: 'coffee', character: '☕' },
      { title: 'cat face', character: '😺' },
      { title: 'dog face', character: '🐶' },
      { title: 'rainbow', character: '🌈' },
      { title: 'sunflower', character: '🌻' },
      { title: 'penguin', character: '🐧' },
      { title: 'moon', character: '🌙' },
      { title: 'banana', character: '🍌' },
      { title: 'computer', character: '💻' },
      { title: 'hamburger', character: '🍔' },
      { title: 'fire', character: '🔥' },
      { title: 'sunglasses', character: '😎' },
      { title: 'taco', character: '🌮' },
      { title: 'unicorn', character: '🦄' },
      { title: 'guitar', character: '🎸' },
      { title: 'cookie', character: '🍪' },
      { title: 'umbrella', character: '☔' },
      { title: 'shooting star', character: '🌠' },
      { title: 'fish', character: '🐟' },
      { title: 'pizza', character: '🍕' },
      { title: 'football', character: '⚽' },
      { title: 'book', character: '📚' },
      { title: 'microphone', character: '🎤' },
      { title: 'earth globe', character: '🌍' },
      { title: 'globe with meridians', character: '🌐' },
      { title: 'hourglass', character: '⌛' },
      { title: 'camera', character: '📷' },
      { title: 'television', character: '📺' },
      { title: 'cactus', character: '🌵' },
      { title: 'birthday cake', character: '🎂' },
      { title: 'alarm clock', character: '⏰' },
      { title: 'airplane', character: '✈️' },
      { title: 'basketball', character: '🏀' },
      { title: 'globe showing Europe-Africa', character: '🌍' },
      { title: 'clinking beer mugs', character: '🍻' },
      { title: 'musical notes', character: '🎶' },
      { title: 'bicycle', character: '🚲' },
      { title: 'sun with face', character: '🌞' },
      { title: 'rainbow flag', character: '🏳️‍🌈' },
      { title: 'film projector', character: '📽️' },
      { title: 'shopping cart', character: '🛒' },
    ],
    { label: 'Emoticons' }
  );
}

class Editor extends ClassicEditor {
  public static override builtinPlugins = [
    Alignment,
    AutoImage,
    AutoLink,
    Autoformat,
    Autosave,
    BlockQuote,
    Bold,
    CloudServices,
    Essentials,
    FindAndReplace,
    FontBackgroundColor,
    FontColor,
    FontFamily,
    FontSize,
    GeneralHtmlSupport,
    Heading,
    Image,
    ImageCaption,
    ImageInsert,
    ImageResize,
    ImageStyle,
    ImageToolbar,
    ImageUpload,
    Indent,
    Italic,
    Link,
    LinkImage,
    List,
    Markdown,
    MediaEmbed,
    Paragraph,
    PasteFromOffice,
    PictureEditing,
    Style,
    Table,
    TableCaption,
    TableCellProperties,
    TableColumnResize,
    TableProperties,
    TableToolbar,
    TextTransformation,
    Underline,
    Undo,
    SpecialCharacters,
    SpecialCharactersEmoji,
  ];

  public static override defaultConfig: EditorConfig = {
    toolbar: {
      items: [
        'undo',
        'redo',
        '|',
        'heading',
        'bold',
        'italic',
        'underline',
        'link',
        '|',
        'bulletedList',
        'numberedList',
        'outdent',
        'indent',
        'alignment',
        '|',
        'fontBackgroundColor',
        'fontFamily',
        'fontSize',
        'fontColor',
        'style',
        '|',
        'blockQuote',
        'insertTable',
        'imageInsert',
        '|',
        'findAndReplace',
        'specialCharacters',
      ],
    },
    language: 'en',
    image: {
      toolbar: [
        'imageTextAlternative',
        'toggleImageCaption',
        'imageStyle:inline',
        'imageStyle:block',
        'imageStyle:side',
        'linkImage',
      ],
    },
    table: {
      contentToolbar: [
        'tableColumn',
        'tableRow',
        'mergeTableCells',
        'tableCellProperties',
        'tableProperties',
      ],
    },
  };
}

export default Editor;
