import {
  isDefined,
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/postgresql';
import { ValidationArguments } from 'class-validator/types/validation/ValidationArguments';
import { Collections } from '@libs/entities/entities/Collection';

@ValidatorConstraint({ name: 'isCollectionEnableDownload', async: true })
@Injectable()
export class IsCollectionEnableDownloadValidator
  implements ValidatorConstraintInterface
{
  constructor(
    @InjectRepository(Collections)
    private readonly collectionRepository: EntityRepository<Collections>
  ) {}

  async validate(id?: Collections['id']): Promise<boolean> {
    if (!isDefined(id)) {
      return true;
    }
    const collection = await this.collectionRepository.findOne({ id: id });
    return collection.enable_download;
  }

  defaultMessage(validationArguments?: ValidationArguments): string {
    return `${validationArguments.property} không cho phép tải xuống.`;
  }
}

export const IsCollectionEnableDownload = (
  validationOptions?: ValidationOptions
) => {
  return (object: Record<string, any>, propertyName: string): void => {
    registerDecorator({
      name: 'isCollectionEnableDownload',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: validationOptions,
      validator: IsCollectionEnableDownloadValidator,
      async: true,
    });
  };
};
