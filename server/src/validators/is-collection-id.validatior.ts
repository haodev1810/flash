import {
  isDefined,
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/postgresql';
import { User } from '@libs/entities/entities/User';
import { ValidationArguments } from 'class-validator/types/validation/ValidationArguments';
import { Collections } from '@libs/entities/entities/Collection';

@ValidatorConstraint({ name: 'isCollectionId', async: true })
@Injectable()
export class IsCollectionIdValidator implements ValidatorConstraintInterface {
  constructor(
    @InjectRepository(Collections)
    private readonly collectionRepository: EntityRepository<Collections>
  ) {}

  async validate(id?: Collections['id']): Promise<boolean> {
    if (!isDefined(id)) {
      return true;
    }
    const collection = await this.collectionRepository.findOne({
      id,
    });
    return !!collection;
  }

  defaultMessage(validationArguments?: ValidationArguments): string {
    return `${validationArguments.property} không tồn tại.`;
  }
}

export const IsCollectionId = (validationOptions?: ValidationOptions) => {
  return (object: Record<string, any>, propertyName: string): void => {
    registerDecorator({
      name: 'isCollectionId',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: validationOptions,
      validator: IsCollectionIdValidator,
      async: true,
    });
  };
};
