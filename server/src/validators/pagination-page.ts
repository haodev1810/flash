import { ApiProperty } from '@nestjs/swagger';
import { BaseDto } from '@server/validators/base.dto';
import { Type } from 'class-transformer';
import { IsNumber, IsOptional, IsPositive } from 'class-validator';

export class PaginationPageDto extends BaseDto {
  @IsOptional()
  @ApiProperty({ default: 1 })
  @IsPositive()
  @IsNumber()
  @Type(() => Number)
  page: number;

  @IsOptional()
  @ApiProperty({ default: 30, maximum: 50 })
  @IsPositive()
  @IsNumber()
  @Type(() => Number)
  limit: number;
}
