import { Video } from '@libs/entities/entities/Video';
import { BaseResponseTransformer } from '@server/transformers/base-response.transformer';
import { VideoResponse } from '../responses/video.response';
import { StoredFileTransformer } from '@server/modules/stored-files/transformers/stored-files.transformer';

export class VideoTransformer extends BaseResponseTransformer {
  static toVideoResponse(video: Video): VideoResponse {
    return {
      ...BaseResponseTransformer.transformEntityTimestamps(video),
      thumbnail: StoredFileTransformer.toStordFileResponse(video.thumbnail),
      file: StoredFileTransformer.toStordFileResponse(video.file),
    };
  }
}
