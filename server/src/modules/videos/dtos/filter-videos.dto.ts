import { ApiProperty } from '@nestjs/swagger';
import { PaginationPageDto } from '@server/validators/pagination-page';
import { IsOptional } from 'class-validator';

export class FilterVideosDto extends PaginationPageDto {
  @IsOptional()
  @ApiProperty({ nullable: true, required: false })
  query: string;

  @IsOptional()
  @ApiProperty({ nullable: true, required: false })
  similar: string;

  @IsOptional()
  @ApiProperty({ nullable: true, required: false })
  author: string;
}
