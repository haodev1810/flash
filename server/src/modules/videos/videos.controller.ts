import { Controller, Get, Query } from '@nestjs/common';
import { VideosService } from './videos.service';
import { ApiOperation } from '@nestjs/swagger';
import { AppSwaggerTag } from '../app-swagger/app-swagger.constant';
import { FilterVideosDto } from './dtos/filter-videos.dto';
import { Public } from '../auth/guards/public.guard';
import { ApiErrorResponse, ApiPaginatedResponse } from '@libs/utils/swagger';
import { ItemVideoResponse } from './responses/item-video.response';
import { AppApiPaginatedResponse } from '@libs/utils/responses';
import { ItemTransformer } from '../items/transformers/item.transformer';

@Controller('videos')
export class VideosController {
  constructor(private readonly videosService: VideosService) {}

  @ApiOperation({ tags: [AppSwaggerTag.Videos] })
  @ApiPaginatedResponse(ItemVideoResponse)
  @ApiErrorResponse()
  @Get()
  @Public()
  async list(@Query() options: FilterVideosDto) {
    const result = await this.videosService.list(options);
    return AppApiPaginatedResponse.create(
      result.data.map(ItemTransformer.toItemVideoResponse),
      result.pagination
    );
  }
}
