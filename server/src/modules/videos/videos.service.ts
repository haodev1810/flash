import { Item } from '@libs/entities/entities/Item';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/postgresql';
import { Injectable, Scope } from '@nestjs/common';
import { FilterVideosDto } from './dtos/filter-videos.dto';
import { TypeItem } from '@libs/constants/entities/Item';
import { Pagination } from '@libs/utils/responses';

@Injectable({ scope: Scope.REQUEST })
export class VideosService {
  constructor(
    @InjectRepository(Item)
    private readonly itemRepository: EntityRepository<Item>
  ) {}

  async list(option: FilterVideosDto): Promise<{
    data: Item[];
    pagination: Pagination;
  }> {
    const [data, count] = await this.itemRepository.findAndCount(
      {
        type: TypeItem.VIDEO,
        video_id: { $exists: true },
      },
      {
        populate: ['video.file', 'video.thumbnail', 'author.avatar'],
        limit: option.limit,
        offset: (option.page - 1) * option.limit,
      }
    );
    return {
      data,
      pagination: {
        limit: option.limit,
        page: option.page,
        lastPage: Math.ceil(count / option.page),
        total: count,
      },
    };
  }
}
