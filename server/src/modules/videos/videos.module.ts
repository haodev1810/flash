import { MikroOrmModule } from '@mikro-orm/nestjs';
import { AbilityModule } from '../auth/ability/ability.module';
import { VideosController } from './videos.controller';
import { VideosService } from './videos.service';
import { Module } from '@nestjs/common';
import { User } from '@libs/entities/entities/User';
import { Video } from '@libs/entities/entities/Video';
import { StoredFile } from '@libs/entities/entities/StoredFile';
import { Images } from '@libs/entities/entities/Images';
import { Tag } from '@libs/entities/entities/Tag';
import { Item } from '@libs/entities/entities/Item';

@Module({
  imports: [
    AbilityModule,
    MikroOrmModule.forFeature([User, Video, StoredFile, Images, Tag, Item]),
  ],
  controllers: [VideosController],
  providers: [VideosService],
})
export class VideosModule {}
