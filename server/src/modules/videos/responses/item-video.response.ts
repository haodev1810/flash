import { ApiProperty } from '@nestjs/swagger';
import { AuthorResponse } from '@server/modules/authors/responses/author.response';
import { ItemBaseResponse } from '@server/modules/items/responses/base-item.response';
import { VideoResponse } from './video.response';

export class ItemVideoResponse extends ItemBaseResponse {
  @ApiProperty({ type: AuthorResponse })
  author: AuthorResponse;

  @ApiProperty({})
  video: VideoResponse;
}
