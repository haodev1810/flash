import { ApiProperty } from '@nestjs/swagger';
import { StoredFileResponse } from '@server/modules/stored-files/responses/stored-files.response';
import { BaseSerialKeyResponse } from '@server/responses/base-serial-key.response';

export class VideoResponse extends BaseSerialKeyResponse {
  @ApiProperty({ type: StoredFileResponse })
  thumbnail: StoredFileResponse;

  @ApiProperty({ type: StoredFileResponse })
  file: StoredFileResponse;
}
