import { User } from '@libs/entities/entities/User';
import { BaseResponseTransformer } from '@server/transformers/base-response.transformer';
import { AccountResponse } from '../responses/account.response';
import { StoredFileTransformer } from '@server/modules/stored-files/transformers/stored-files.transformer';
import { AccountBasicResponse } from '../responses/account-basic.response';
import { IdSubject } from '@libs/constants/abilities';
import { RoleTransformer } from '@server/modules/roles/transformers/role.transformer';

export class AccountTransformer extends BaseResponseTransformer {
  static toAccountBasicResponse(account: User): AccountBasicResponse {
    return {
      ...BaseResponseTransformer.transformEntityTimestamps(account),
      creator: null,
      updater: null,
      __typename: IdSubject.Users,
    };
  }

  static toAccountResponse(account: User): AccountResponse {
    return {
      ...this.toAccountBasicResponse(account),
      avatar: account.avatar
        ? StoredFileTransformer.toStordFileResponse(account.avatar)
        : null,
      role: account.role ? RoleTransformer.toRoleResponse(account.role) : null,
    };
  }
}
