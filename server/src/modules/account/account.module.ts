import { MikroOrmModule } from '@mikro-orm/nestjs';
import { AbilityModule } from '../auth/ability/ability.module';
import { AccountController } from './account.controller';
import { AccountService } from './account.service';
import { Module } from '@nestjs/common';
import { User } from '@libs/entities/entities/User';

@Module({
  imports: [AbilityModule, MikroOrmModule.forFeature([User])],
  controllers: [AccountController],
  providers: [AccountService],
})
export class AccountModule {}
