import { User } from '@libs/entities/entities/User';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityManager, EntityRepository } from '@mikro-orm/postgresql';
import { Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';

@Injectable({ scope: Scope.REQUEST })
export class AccountService {
  constructor(
    @InjectRepository(User)
    private readonly accountRepository: EntityRepository<User>,
    @Inject(REQUEST) private request: Request,
    private readonly em: EntityManager
  ) {}

  async account() {
    return await this.accountRepository.findOneOrFail(
      { id: this.request.user.id },
      {
        populate: ['avatar', 'role'],
      }
    );
  }
}
