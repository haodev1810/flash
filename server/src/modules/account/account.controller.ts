import { Controller, Get } from '@nestjs/common';
import { AccountService } from './account.service';
import { ApiOperation } from '@nestjs/swagger';
import { AppSwaggerTag } from '../app-swagger/app-swagger.constant';
import { ApiErrorResponse, ApiSuccessResponse } from '@libs/utils/swagger';
import { AccountResponse } from './responses/account.response';
import { AppApiSuccessResponse } from '@libs/utils/responses';
import { AccountTransformer } from './transformers/account.transformer';

@Controller('account')
export class AccountController {
  constructor(private readonly accountService: AccountService) {}

  @ApiOperation({ tags: [AppSwaggerTag.Account] })
  @ApiSuccessResponse(AccountResponse)
  @ApiErrorResponse()
  @Get()
  async account() {
    return AppApiSuccessResponse.create(
      AccountTransformer.toAccountResponse(await this.accountService.account())
    );
  }
}
