import { StoredFileResponse } from './../../stored-files/responses/stored-files.response';
import { IdSubject } from '@libs/constants/abilities';
import { ApiProperty } from '@nestjs/swagger';
import { AccountBasicResponse } from './account-basic.response';
import { RoleResponse } from '@server/modules/roles/responses/role.response';

export class AccountResponse extends AccountBasicResponse {
  @ApiProperty({ type: StoredFileResponse, nullable: true })
  avatar: StoredFileResponse;

  @ApiProperty({ type: RoleResponse, nullable: true })
  role: RoleResponse;

  @ApiProperty()
  created_at: string;

  @ApiProperty()
  updated_at: string;

  @ApiProperty({ enum: IdSubject, required: false, nullable: true })
  __typename?: IdSubject;
}
