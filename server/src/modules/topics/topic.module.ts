import { TopicService } from './topic.service';
import { TopicController } from './topic.controller';
import { Module } from '@nestjs/common';
import { AbilityModule } from '../auth/ability/ability.module';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { User } from '@libs/entities/entities/User';
import { Role } from '@libs/entities/entities/Role';
import { Topic } from '@libs/entities/entities/Topic';

@Module({
  imports: [AbilityModule, MikroOrmModule.forFeature([User, Role, Topic])],
  controllers: [TopicController],
  providers: [TopicService],
})
export class TopicModule {}
