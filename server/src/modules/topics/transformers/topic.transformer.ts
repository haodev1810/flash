import { BaseResponseTransformer } from '@server/transformers/base-response.transformer';
import { TopicResponse } from '../responses/topic.response';
import { IdSubject } from '@libs/constants/abilities';
import { Topic } from '@libs/entities/entities/Topic';

export class TopicTransformer extends BaseResponseTransformer {
  static toTopicResponse(topic: Topic): TopicResponse {
    return {
      ...BaseResponseTransformer.transformEntityTimestamps(topic),
      __typename: IdSubject.Topic,
    };
  }
}
