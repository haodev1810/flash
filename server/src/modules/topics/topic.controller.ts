import { Controller, Get, Query } from '@nestjs/common';
import { TopicService } from './topic.service';
import { ApiOperation } from '@nestjs/swagger';
import { AppSwaggerTag } from '../app-swagger/app-swagger.constant';
import { ApiErrorResponse, ApiPaginatedResponse } from '@libs/utils/swagger';
import { TopicResponse } from './responses/topic.response';
import { FilterTopicDto } from './dtos/filter-topic.dto';
import { AppApiPaginatedResponse } from '@libs/utils/responses';
import { TopicTransformer } from './transformers/topic.transformer';
import { Public } from '../auth/guards/public.guard';

@Controller('topics')
export class TopicController {
  constructor(private readonly topicService: TopicService) {}

  @ApiOperation({ tags: [AppSwaggerTag.Topic] })
  @ApiPaginatedResponse(TopicResponse)
  @ApiErrorResponse()
  @Get()
  @Public()
  async list(@Query() options: FilterTopicDto) {
    const result = await this.topicService.list(options);
    return AppApiPaginatedResponse.create(
      result.data.map(TopicTransformer.toTopicResponse),
      result.pagination
    );
  }
}
