import { ApiProperty } from '@nestjs/swagger';
import { PaginationPageDto } from '@server/validators/pagination-page';
import { Type } from 'class-transformer';
import { IsBoolean, IsOptional } from 'class-validator';

export class FilterTopicDto extends PaginationPageDto {
  @IsOptional()
  @ApiProperty({ type: 'boolean' })
  @IsBoolean()
  @Type(() => Boolean)
  trending: boolean;
}
