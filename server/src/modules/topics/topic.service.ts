import { Injectable, Scope } from '@nestjs/common';
import { FilterTopicDto } from './dtos/filter-topic.dto';
import { InjectRepository } from '@mikro-orm/nestjs';
import { Topic } from '@libs/entities/entities/Topic';
import { EntityManager, EntityRepository } from '@mikro-orm/postgresql';
import { Pagination } from '@libs/utils/responses';

@Injectable({ scope: Scope.REQUEST })
export class TopicService {
  constructor(
    @InjectRepository(Topic)
    private readonly topicRepository: EntityRepository<Topic>,
    private readonly em: EntityManager
  ) {}

  async list(
    options: FilterTopicDto
  ): Promise<{ data: Topic[]; pagination: Pagination }> {
    if (options.trending) {
      const query = `SELECT t.id, SUM(i.views) AS total_views
      FROM topics t
      JOIN topic_items ti ON t.id = ti.topic_id
      JOIN items i ON ti.item_id = i.id
      GROUP BY t.id, t.title
      ORDER BY total_views DESC
      LIMIT ${options.limit};`;
      const result = await this.em.execute(query);
      const topics = await Promise.all(
        result.map(async (i) => {
          return this.topicRepository.findOne({ id: i['id'] });
        })
      );
      return {
        data: topics,
        pagination: {
          limit: options.limit,
          page: options.page,
          lastPage: 1,
          total: 1,
        },
      };
    }
    const [data, count] = await this.topicRepository.findAndCount(
      {},
      {
        limit: options.limit,
        offset: (options.page - 1) * options.limit,
      }
    );
    return {
      data,
      pagination: {
        limit: options.limit,
        page: options.page,
        lastPage: Math.ceil(count / options.limit),
        total: count,
      },
    };
  }
}
