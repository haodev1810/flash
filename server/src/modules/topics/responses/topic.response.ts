import { ApiProperty } from '@nestjs/swagger';
import { BaseSerialKeyResponse } from '@server/responses/base-serial-key.response';

export class TopicResponse extends BaseSerialKeyResponse {
  @ApiProperty()
  title: string;

  @ApiProperty()
  slug: string;
}
