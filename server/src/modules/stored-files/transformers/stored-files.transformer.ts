import { StoredFile } from '@libs/entities/entities/StoredFile';
import { BaseResponseTransformer } from '@server/transformers/base-response.transformer';
import { StoredFileResponse } from '../responses/stored-files.response';
import { IdSubject } from '@libs/constants/abilities';

export class StoredFileTransformer extends BaseResponseTransformer {
  static toStordFileResponse(storedFile: StoredFile): StoredFileResponse {
    return {
      ...BaseResponseTransformer.transformEntityTimestamps(storedFile),
      driver: storedFile.driver,
      // creator: null,
      // updater: null,
      __typename: IdSubject.StoredFiles,
    };
  }
}
