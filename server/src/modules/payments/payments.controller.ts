import { AppSwaggerTag } from './../app-swagger/app-swagger.constant';
import { Controller, Post } from '@nestjs/common';
import { PaymentsService } from './payments.service';
import { ApiOperation } from '@nestjs/swagger';

@Controller('payments')
export class PaymentsController {
  constructor(private readonly paymentService: PaymentsService) {}

  @ApiOperation({ tags: [AppSwaggerTag.Payments] })
  @Post()
  async createPayment() {}
}
