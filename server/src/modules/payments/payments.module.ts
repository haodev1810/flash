import { MikroOrmModule } from '@mikro-orm/nestjs';
import { PaymentsController } from './payments.controller';
import { PaymentsService } from './payments.service';
import { Module } from '@nestjs/common';
import { User } from '@libs/entities/entities/User';
import { Role } from '@libs/entities/entities/Role';
import { AbilityModule } from '../auth/ability/ability.module';
import { StripeController } from './stripe/stripe.controller';
import { StripeService } from './stripe/stripe.service';

@Module({
  imports: [MikroOrmModule.forFeature([User, Role]), AbilityModule],
  controllers: [PaymentsController, StripeController],
  providers: [PaymentsService, StripeService],
})
export class PaymentsModule {}
