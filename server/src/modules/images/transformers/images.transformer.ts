import { Images } from '@libs/entities/entities/Images';
import { BaseResponseTransformer } from '@server/transformers/base-response.transformer';
import { ImagesResponse } from '../responses/images.response';
import { StoredFileTransformer } from '@server/modules/stored-files/transformers/stored-files.transformer';
import { IdSubject } from '@libs/constants/abilities';

export class ImagesTransformer extends BaseResponseTransformer {
  static toImagesReponse(images: Images): ImagesResponse {
    return {
      ...this.transformEntityTimestamps(images),
      small: images.small
        ? StoredFileTransformer.toStordFileResponse(images.small)
        : null,
      medium: images.medium
        ? StoredFileTransformer.toStordFileResponse(images.medium)
        : null,
      large: images.medium
        ? StoredFileTransformer.toStordFileResponse(images.large)
        : null,
      origin: images.origin
        ? StoredFileTransformer.toStordFileResponse(images.origin)
        : null,
      __typename: IdSubject.Images,
    };
  }
}
