import { ApiProperty } from '@nestjs/swagger';
import { StoredFileResponse } from '@server/modules/stored-files/responses/stored-files.response';
import { BaseSerialKeyResponse } from '@server/responses/base-serial-key.response';

export class ImagesResponse extends BaseSerialKeyResponse {
  @ApiProperty({ type: StoredFileResponse, nullable: true })
  small: StoredFileResponse | null;

  @ApiProperty({ type: StoredFileResponse, nullable: true })
  medium: StoredFileResponse | null;

  @ApiProperty({ type: StoredFileResponse, nullable: true })
  large: StoredFileResponse | null;

  @ApiProperty({ type: StoredFileResponse, nullable: true })
  origin: StoredFileResponse | null;
}
