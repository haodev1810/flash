import { VideosModule } from './../videos/videos.module';
import { TopicModule } from './../topics/topic.module';
import { PaymentsModule } from './../payments/payments.module';
import { CollectionsModule } from './../collections/collections.module';
import { AuthorsModule } from './../authors/authors.module';
import { ItemsModule } from './../items/items.module';
import { QueuePrefix } from '@libs/constants/queue';
import { CrawlsModule } from './../crawls/crawls.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MikroOrmModule } from '../mikro-orm/mikro-orm.module';
import { BullModule } from '@nestjs/bull';
import { JobsModule } from '../jobs/jobs.module';
import { AuthModule } from '../auth/auth.module';
import { AccountModule } from '../account/account.module';
import { AccountsModule } from '../accounts/accounts.module';
import { AbilityModule } from '../auth/ability/ability.module';
import { APP_FILTER, APP_GUARD } from '@nestjs/core';
import { JwtAuthGuard } from '../auth/app-auth.guard';
import { HttpErrorFilter } from '@server/filters/http-error.filter';
import { TagsModule } from '../tags/tags.module';
import { UploadsModule } from '../uploads/uploads.module';

@Module({
  imports: [
    VideosModule,
    TopicModule,
    PaymentsModule,
    AuthorsModule,
    ItemsModule,
    CrawlsModule,
    MikroOrmModule,
    BullModule.forRoot({
      redis: {
        host: process.env.REDIS_HOST,
        port: Number(process.env.FLASH_REDIS_PORT),
        password: process.env.FLASH_REDIS_PASSWORD,
      },
      prefix: QueuePrefix.Queue,
    }),
    JobsModule,
    UploadsModule,
    AuthModule,
    AccountModule,
    AccountsModule,
    AbilityModule,
    TagsModule,
    CollectionsModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    {
      provide: APP_FILTER,
      useClass: HttpErrorFilter,
    },
  ],
})
export class AppModule {}
