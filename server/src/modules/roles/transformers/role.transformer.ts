import { Role } from '@libs/entities/entities/Role';
import { BaseResponseTransformer } from '@server/transformers/base-response.transformer';
import { RoleResponse } from '@server/modules/roles/responses/role.response';
import { IdSubject } from '@libs/constants/abilities';

export class RoleTransformer extends BaseResponseTransformer {
  static toRoleResponse(role: Role): RoleResponse {
    return {
      ...BaseResponseTransformer.transformEntityTimestamps(role),
      // creator: null,
      // updater: null,
      __typename: IdSubject.Roles,
    };
  }
}
