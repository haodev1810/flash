import { Tag } from '@libs/entities/entities/Tag';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/postgresql';
import { Injectable } from '@nestjs/common';
import { TagQuery } from './dtos/query-tag.dto';
import { Pagination } from '@libs/utils/responses';

@Injectable()
export class TagsService {
  constructor(
    @InjectRepository(Tag)
    private readonly tagRepository: EntityRepository<Tag>
  ) {}
  async list(
    options: TagQuery
  ): Promise<{ data: Tag[]; pagination: Pagination }> {
    const limit = options.limit || 20;
    const page = options.page || 1;
    const [data, count] = await this.tagRepository.findAndCount(
      {
        $or: [
          {
            search_term: options.query,
          },
          {
            name: options.query,
          },
          { search_term: { $fulltext: options.query.split('-').join(' ') } },
          {
            name: { $fulltext: options.query.split('-').join(' ') },
          },
        ],
      },
      {
        limit,
        offset: (page - 1) * limit,
      }
    );
    return {
      data,
      pagination: {
        lastPage: Math.ceil(count / limit),
        limit: limit,
        page: page,
        total: count,
      },
    };
  }
}
