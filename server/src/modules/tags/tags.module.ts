import { TagsService } from './tags.service';
import { TagsController } from './tags.controller';
import { Module } from '@nestjs/common';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { User } from '@libs/entities/entities/User';
import { Tag } from '@libs/entities/entities/Tag';

@Module({
  imports: [MikroOrmModule.forFeature([User, Tag])],
  controllers: [TagsController],
  providers: [TagsService],
})
export class TagsModule {}
