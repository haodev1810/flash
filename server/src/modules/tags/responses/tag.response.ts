import { ApiProperty } from '@nestjs/swagger';
import { BaseSerialKeyResponse } from '@server/responses/base-serial-key.response';

export class TagResponse extends BaseSerialKeyResponse {
  @ApiProperty()
  name: string;

  @ApiProperty()
  search_term: string;
}
