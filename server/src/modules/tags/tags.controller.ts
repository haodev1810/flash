import { Controller, Get, Query } from '@nestjs/common';
import { TagsService } from './tags.service';
import { ApiOperation } from '@nestjs/swagger';
import { AppSwaggerTag } from '../app-swagger/app-swagger.constant';
import { ApiErrorResponse, ApiPaginatedResponse } from '@libs/utils/swagger';
import { TagResponse } from './responses/tag.response';
import { AppApiPaginatedResponse } from '@libs/utils/responses';
import { TagTransformer } from './transformers/tag.transformer';
import { TagQuery } from './dtos/query-tag.dto';
import { Public } from '../auth/guards/public.guard';

@Controller('tags')
export class TagsController {
  constructor(private readonly tagsService: TagsService) {}

  @ApiOperation({ tags: [AppSwaggerTag.Tags] })
  @ApiPaginatedResponse(TagResponse)
  @ApiErrorResponse()
  @Public()
  @Get('/')
  async list(@Query() options: TagQuery) {
    const result = await this.tagsService.list(options);
    return AppApiPaginatedResponse.create(
      result.data.map(TagTransformer.toTagResponse),
      result.pagination
    );
  }
}
