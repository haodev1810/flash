import { ApiProperty } from '@nestjs/swagger';
import { PaginationPageDto } from '@server/validators/pagination-page';
import { IsOptional } from 'class-validator';

export class TagQuery extends PaginationPageDto {
  @IsOptional()
  @ApiProperty({ required: false, nullable: true })
  query: string;
}
