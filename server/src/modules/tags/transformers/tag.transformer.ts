import { Tag } from '@libs/entities/entities/Tag';
import { BaseResponseTransformer } from '@server/transformers/base-response.transformer';
import { TagResponse } from '../responses/tag.response';
import { IdSubject } from '@libs/constants/abilities';

export class TagTransformer extends BaseResponseTransformer {
  static toTagResponse(tag: Tag): TagResponse {
    return {
      ...BaseResponseTransformer.transformEntityTimestamps(tag),
      __typename: IdSubject.Tags,
    };
  }
}
