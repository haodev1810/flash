import { BaseResponseTransformer } from '@server/transformers/base-response.transformer';
import { ItemBaseResponse } from '../responses/base-item.response';
import { Item } from '@libs/entities/entities/Item';
import { ItemResponse } from '../responses/item.response';
import { IdSubject } from '@libs/constants/abilities';
import { ImagesTransformer } from '@server/modules/images/transformers/images.transformer';
import { AuthorTransformer } from '@server/modules/authors/transformers/author.transformer';
import { ItemFullResponse } from '../responses/item-full.response';
import { TagTransformer } from '@server/modules/tags/transformers/tag.transformer';
import { ItemVideoResponse } from '@server/modules/videos/responses/item-video.response';
import { VideoTransformer } from '@server/modules/videos/transformer/video.transformer';

export class ItemTransformer extends BaseResponseTransformer {
  static toItemBaseResponse(item: Item): ItemBaseResponse {
    return {
      ...this.transformEntityTimestamps(item),
      __typename: IdSubject.Items,
    };
  }
  static toItemResponse(item: Item): ItemResponse {
    return {
      ...ItemTransformer.toItemBaseResponse(item),
      images: item.images
        ? ImagesTransformer.toImagesReponse(item.images)
        : null,
      author: item.author
        ? AuthorTransformer.toAuthorResponse(item.author)
        : null,
    };
  }

  static toItemFullResponse(item: Item): ItemFullResponse {
    return {
      ...ItemTransformer.toItemResponse(item),
      tags: item.tags.map(TagTransformer.toTagResponse),
    };
  }

  static toItemVideoResponse(item: Item): ItemVideoResponse {
    return {
      ...ItemTransformer.toItemBaseResponse(item),
      author: AuthorTransformer.toAuthorResponse(item.author),
      video: VideoTransformer.toVideoResponse(item.video),
    };
  }
}
