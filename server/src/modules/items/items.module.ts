import { AbilityModule } from './../auth/ability/ability.module';
import { Module } from '@nestjs/common';
import { ItemsController } from './items.controller';
import { ItemsService } from './items.service';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { User } from '@libs/entities/entities/User';
import { StoredFile } from '@libs/entities/entities/StoredFile';
import { Item } from '@libs/entities/entities/Item';
import { Images } from '@libs/entities/entities/Images';
import { Author } from '@libs/entities/entities/Author';
import { Tag } from '@libs/entities/entities/Tag';
import { TagItem } from '@libs/entities/entities/TagItem';
import { UploadsModule } from '../uploads/uploads.module';

@Module({
  imports: [
    MikroOrmModule.forFeature([
      User,
      StoredFile,
      Item,
      Images,
      Author,
      Tag,
      TagItem,
    ]),
    UploadsModule,
    AbilityModule,
  ],
  controllers: [ItemsController],
  providers: [ItemsService],
})
export class ItemsModule {}
