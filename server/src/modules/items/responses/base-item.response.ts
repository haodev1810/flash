import { ApiProperty } from '@nestjs/swagger';
import { BaseSerialKeyResponse } from '@server/responses/base-serial-key.response';

export class ItemBaseResponse extends BaseSerialKeyResponse {
  @ApiProperty()
  title: string;

  @ApiProperty()
  slug: string;

  @ApiProperty()
  alt: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  views: number;

  @ApiProperty()
  downloads: number;
}
