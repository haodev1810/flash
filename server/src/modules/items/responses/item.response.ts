import { ApiProperty } from '@nestjs/swagger';
import { ItemBaseResponse } from './base-item.response';
import { AuthorResponse } from '@server/modules/authors/responses/author.response';
import { ImagesResponse } from '@server/modules/images/responses/images.response';

export class ItemResponse extends ItemBaseResponse {
  @ApiProperty({ type: AuthorResponse, nullable: true })
  author: AuthorResponse | null;

  @ApiProperty({ type: ImagesResponse, nullable: true })
  images: ImagesResponse | null;
}
