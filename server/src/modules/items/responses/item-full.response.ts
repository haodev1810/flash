import { ApiProperty } from '@nestjs/swagger';
import { ItemResponse } from './item.response';
import { TagResponse } from '@server/modules/tags/responses/tag.response';

export class ItemFullResponse extends ItemResponse {
  @ApiProperty({ type: TagResponse, isArray: true, nullable: true })
  tags: TagResponse[];
}
