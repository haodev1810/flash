import { ApiProperty } from '@nestjs/swagger';
import { BaseDto } from '@server/validators/base.dto';
import { Type } from 'class-transformer';
import { IsDefined, IsNumber, IsOptional, IsPositive } from 'class-validator';

export class DownloadDto extends BaseDto {
  @ApiProperty()
  @IsOptional()
  @IsDefined()
  @IsNumber()
  @IsPositive()
  @Type(() => Number)
  id: number;

  @ApiProperty()
  @IsOptional()
  @IsDefined()
  slug: string;
}
