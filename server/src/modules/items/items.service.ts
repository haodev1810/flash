import { FilterImage } from '@libs/constants/filter';
import { FilterItemsDto } from './dtos/filter-items.dto';
import { Author } from '@libs/entities/entities/Author';
import { Images } from '@libs/entities/entities/Images';
import { Item } from '@libs/entities/entities/Item';
import { Tag } from '@libs/entities/entities/Tag';
import { TagItem } from '@libs/entities/entities/TagItem';
import { User } from '@libs/entities/entities/User';
import { Pagination } from '@libs/utils/responses';
import { FilterQuery, FindOptions } from '@mikro-orm/core';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityManager, EntityRepository } from '@mikro-orm/postgresql';
import { Inject, Injectable } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { StoredFile } from '@libs/entities/entities/StoredFile';
import { DownloadDto } from './dtos/download.dto';

@Injectable()
export class ItemsService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: EntityRepository<User>,
    @InjectRepository(Author)
    private readonly authorRepository: EntityRepository<Author>,
    @InjectRepository(Tag)
    private readonly tagRepository: EntityRepository<Tag>,
    @InjectRepository(Item)
    private readonly itemRepository: EntityRepository<Item>,
    @InjectRepository(Images)
    private readonly imagesRepository: EntityRepository<Images>,
    @InjectRepository(TagItem)
    private readonly tagItemRepository: EntityRepository<TagItem>,
    @InjectRepository(StoredFile)
    private readonly storedFileRepository: EntityRepository<StoredFile>,
    private readonly em: EntityManager,
    @Inject(REQUEST)
    private request: Request
  ) {}

  async list(
    options: FilterItemsDto
  ): Promise<{ data: Item[]; pagination: Pagination }> {
    const where: FilterQuery<Item> = {};
    const start = options.page > 1 ? 0 : 1;
    const filter: FindOptions<
      Item,
      | 'tags'
      | 'author.avatar'
      | 'images.small'
      | 'images.medium'
      | 'images.large'
      | 'author.name'
      | 'author.username'
      | 'author.avatar.driver'
      | 'images.small.driver'
      | 'images.medium.driver'
      | 'images.large.driver'
    > = {
      limit: options.limit,
      offset: (options.page - 1) * options.limit + start,
      orderBy: { views: 'DESC' },
    };
    if (options.author) {
      where.author = {
        username: options.author,
      };
    }
    if (options.similar) {
      const tags = await this.tagRepository.find(
        {
          items: {
            slug: options.similar,
          },
        },
        {}
      );
      const mapping = tags.map((i) => i.id);
      where.tag_items = {
        tag_id: { $in: mapping },
      };
    }
    if (options.query) {
      where.tags = {
        $or: [
          {
            search_term: { $fulltext: options.query.split('-').join(' ') },
          },
          {
            name: { $fulltext: options.query },
          },
        ],
      };
    }
    if (options.image) {
      switch (options.image) {
        case FilterImage.All: {
          filter.populate = [
            'images.small',
            'images.large',
            'images.medium',
            'author.avatar',
          ];
          break;
        }
        case FilterImage.Large: {
          filter.populate = ['images.large', 'author.avatar'];
          break;
        }
        case FilterImage.Medium: {
          filter.populate = ['images.medium', 'author.avatar', 'author.avatar'];
          break;
        }
        case FilterImage.Small: {
          filter.populate = ['images.small', 'author.avatar'];
          break;
        }
        default: {
          filter.populate = ['author.avatar'];
          break;
        }
      }
    }
    filter.fields = [
      'title',
      'slug',
      'alt',
      'views',
      'downloads',
      'aspect_ratio',
      'images.small',
      'images.large',
      'images.medium',
      'author.avatar',
      'author.name',
      'author.username',
    ];
    const [data, count] = await this.itemRepository.findAndCount(where, filter);
    return {
      data,
      pagination: {
        lastPage: Math.ceil(count / options.limit),
        limit: data.length,
        page: options.page,
        total: count,
      },
    };
  }

  async findOne(slug: string): Promise<Item> {
    const item = await this.itemRepository.findOneOrFail(
      {
        slug,
      },
      {
        populate: [
          'tags',
          'author.name',
          'author.username',
          'author.avatar',
          'author.avatar_large',
          'images.medium',
          'images.large',
          'images.small',
          'images.origin',
        ],
        fields: [
          'title',
          'slug',
          'alt',
          'downloads',
          'description',
          'views',
          'aspect_ratio',
          'tags.name',
          'tags.search_term',
          'images.small',
          'images.large',
          'images.medium',
          'images.origin',
          'author.avatar',
          'author.avatar_large',
          'author.name',
          'author.username',
          'tags.name',
          'tags.search_term',
        ],
      }
    );
    item.views = item.views + 1;
    await this.itemRepository.persistAndFlush(item);
    return item;
  }

  async download(data: DownloadDto): Promise<Item> {
    const item = await this.itemRepository.findOneOrFail({
      id: data.id,
      slug: data.slug,
    });
    item.downloads = item.downloads + 1;
    item.views = item.views + 1;
    await this.itemRepository.persistAndFlush(item);
    return item;
  }

  async update() {}

  async delete(id: number) {
    const item = await this.itemRepository.findOneOrFail({
      id,
    });
    await this.imagesRepository.removeAndFlush(item);
    return item;
  }
}
