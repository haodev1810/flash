import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ItemsService } from './items.service';
import { ApiOperation } from '@nestjs/swagger';
import { AppSwaggerTag } from '../app-swagger/app-swagger.constant';
import {
  ApiErrorResponse,
  ApiPaginatedResponse,
  ApiSuccessResponse,
} from '@libs/utils/swagger';
import {
  AppApiPaginatedResponse,
  AppApiSuccessResponse,
} from '@libs/utils/responses';
import { FilterItemsDto } from './dtos/filter-items.dto';
import { ItemResponse } from './responses/item.response';
import { ItemTransformer } from './transformers/item.transformer';
import { Public } from '../auth/guards/public.guard';
import { DownloadDto } from './dtos/download.dto';
import { ItemBaseResponse } from './responses/base-item.response';
import { Internal } from '../auth/guards/internal.guard';
import { AbilitiesGuard } from '../auth/ability/ability.guard';
import { ItemFullResponse } from './responses/item-full.response';

@UseGuards(AbilitiesGuard)
@Controller('items')
export class ItemsController {
  constructor(private readonly itemsService: ItemsService) {}

  @ApiOperation({ tags: [AppSwaggerTag.Items] })
  @ApiPaginatedResponse(ItemResponse)
  @ApiErrorResponse()
  @Public()
  @Get()
  async lists(@Query() options: FilterItemsDto) {
    const result = await this.itemsService.list(options);
    return AppApiPaginatedResponse.create(
      result.data.map(ItemTransformer.toItemResponse),
      result.pagination
    );
  }

  @ApiOperation({ tags: [AppSwaggerTag.Items] })
  @ApiSuccessResponse(ItemBaseResponse)
  @ApiErrorResponse()
  @Post('/download')
  @Internal()
  async download(@Body() data: DownloadDto) {
    return AppApiSuccessResponse.create(await this.itemsService.download(data));
  }

  @ApiOperation({ tags: [AppSwaggerTag.Items] })
  @ApiSuccessResponse(ItemFullResponse)
  @ApiErrorResponse()
  @Public()
  @Get('/:slug')
  async findOne(@Param('slug') slug: string) {
    return AppApiSuccessResponse.create(
      ItemTransformer.toItemFullResponse(await this.itemsService.findOne(slug))
    );
  }

  @ApiOperation({ tags: [AppSwaggerTag.Items] })
  @Delete('/:id')
  @Public()
  async delete(
    @Param(
      'id',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })
    )
    id: number
  ) {
    return await this.itemsService.delete(id);
  }
}
