import { ApiProperty } from '@nestjs/swagger';

export class DataJob {
  @ApiProperty()
  topic: string;

  @ApiProperty()
  driver: string;

  @ApiProperty()
  page: number;

  @ApiProperty()
  limit: number;
}

export class JobResponse {
  @ApiProperty()
  id: string;

  @ApiProperty({ type: DataJob })
  data: DataJob;

  @ApiProperty()
  finished?: string;

  @ApiProperty()
  created_at?: string;
}
