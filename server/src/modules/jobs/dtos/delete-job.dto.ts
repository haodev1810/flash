import { StatusJob } from '@libs/constants/jobs';
import { QueueName } from '@libs/constants/queue';
import { ApiProperty } from '@nestjs/swagger';
import { BaseDto } from '@server/validators/base.dto';
import { Type } from 'class-transformer';
import { IsDefined, IsNumber, IsOptional, IsPositive } from 'class-validator';

export class DeleteJobDto extends BaseDto {
  @IsOptional()
  @ApiProperty()
  @IsDefined()
  @IsNumber()
  @IsPositive()
  @Type(() => Number)
  jobId: number;
}

export class DeleteTotalJobDto extends BaseDto {
  @IsOptional()
  @ApiProperty({
    type: 'enum',
    enum: [
      StatusJob.Failed,
      StatusJob.Pending,
      StatusJob.Success,
      StatusJob.Working,
    ],
  })
  @IsDefined()
  status: StatusJob;

  @IsOptional()
  @ApiProperty({ type: 'enum', enum: [QueueName.BotCron] })
  @IsDefined()
  name: QueueName;
}
