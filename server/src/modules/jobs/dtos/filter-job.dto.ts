import { StatusJob } from '@libs/constants/jobs';
import { ApiProperty } from '@nestjs/swagger';
import { BaseDto } from '@server/validators/base.dto';
import { Type } from 'class-transformer';
import { IsNumber, IsOptional, IsPositive } from 'class-validator';

export class FilterJobDto extends BaseDto {
  @IsOptional()
  @ApiProperty({ default: 15 })
  @IsNumber()
  @IsPositive()
  @Type(() => Number)
  limit: number;

  @IsOptional()
  @ApiProperty({ default: 1 })
  @IsNumber()
  @IsPositive()
  @Type(() => Number)
  page: number;

  @IsOptional()
  @ApiProperty({
    enum: [
      StatusJob.Pending,
      StatusJob.Success,
      StatusJob.Failed,
      StatusJob.Working,
    ],
  })
  @Type(() => Number)
  status: StatusJob;
}
