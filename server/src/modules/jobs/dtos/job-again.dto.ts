import { QueueJobName, QueueName } from '@libs/constants/queue';
import { ApiProperty } from '@nestjs/swagger';
import { BaseDto } from '@server/validators/base.dto';
import { IsOptional } from 'class-validator';

export class JobRunAgainDto extends BaseDto {
  @IsOptional()
  @ApiProperty()
  jobId: number;

  @IsOptional()
  @ApiProperty({ type: 'enum', enum: [QueueName.BotCron] })
  jobName: QueueName;

  @IsOptional()
  @ApiProperty({
    type: 'enum',
    enum: [QueueJobName.CronPexel],
  })
  worker: QueueJobName;
}
