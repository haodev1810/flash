import { BullModule } from '@nestjs/bull';
import { JobsController } from './jobs.controller';
import { JobsService } from './jobs.service';
import { Module } from '@nestjs/common';
import { QueueName } from '@libs/constants/queue';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { User } from '@libs/entities/entities/User';
import { Role } from '@libs/entities/entities/Role';

@Module({
  imports: [
    BullModule.registerQueue({
      name: QueueName.BotCron,
    }),
    MikroOrmModule.forFeature([User, Role]),
  ],
  controllers: [JobsController],
  providers: [JobsService],
})
export class JobsModule {}
