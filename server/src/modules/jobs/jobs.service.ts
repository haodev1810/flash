import { QueueJobDataPexel, QueueName } from '@libs/constants/queue';
import { InjectQueue } from '@nestjs/bull';
import { BadRequestException, Injectable } from '@nestjs/common';
import { Job, Queue } from 'bull';
import { DeleteTotalJobDto } from './dtos/delete-job.dto';
import { StatusJob } from '@libs/constants/jobs';
import { getStatusQueueJob } from '@server/utils/libs/pexels';
import { FilterJobDto } from './dtos/filter-job.dto';
import { JobResponse } from './responses/job.response';
import { Pagination } from '@libs/utils/responses';

@Injectable()
export class JobsService {
  constructor(
    @InjectQueue(QueueName.BotCron)
    private readonly botCronQueue: Queue
  ) {}

  async listJobCron(
    options: FilterJobDto
  ): Promise<{ data: JobResponse[]; pagination: Pagination }> {
    let jobs: Job<QueueJobDataPexel>[] = [];
    const limit = options.limit > 50 ? 50 : options.limit;
    const start = (options.page - 1) * limit;
    const end = options.page * limit;
    let total = 0;
    switch (options.status) {
      case StatusJob.Failed: {
        jobs = await this.botCronQueue.getFailed(start, end);
        total = await this.botCronQueue.getFailedCount();
        break;
      }
      case StatusJob.Success: {
        jobs = await this.botCronQueue.getCompleted(start, end);
        total = await this.botCronQueue.getCompletedCount();
        break;
      }
      case StatusJob.Pending: {
        jobs = await this.botCronQueue.getWaiting(start, end);
        total = await this.botCronQueue.getWaitingCount();
        break;
      }
      case StatusJob.Working: {
        jobs = await this.botCronQueue.getActive(start, end);
        total = await this.botCronQueue.getActiveCount();
        break;
      }
      default: {
        return {
          data: [],
          pagination: {
            lastPage: 1,
            limit: 0,
            page: 0,
            total: 0,
          },
        };
      }
    }
    const data: JobResponse[] = [];
    for (const job of jobs) {
      data.push({
        id: job.id.toString(),
        data: job.data,
        created_at: new Date(job.timestamp).toISOString(),
        finished: job.finishedOn
          ? new Date(job.finishedOn).toISOString()
          : null,
      });
    }
    return {
      data,
      pagination: {
        lastPage: Math.ceil(total / limit),
        limit: limit,
        page: options.page,
        total: total,
      },
    };
  }

  async clean(data: DeleteTotalJobDto) {
    switch (data.name) {
      case QueueName.BotCron: {
        await this.cleanJob(data.status);
        break;
      }
      default: {
        throw new BadRequestException('Dữ liệu không hợp lệ');
      }
    }
  }

  async cleanJob(status: StatusJob) {
    const statusClean = getStatusQueueJob(status);
    if (statusClean) {
      await this.botCronQueue.clean(1000, statusClean);
    } else {
      throw new BadRequestException('Trạng thái không hợp lệ');
    }
  }
}
