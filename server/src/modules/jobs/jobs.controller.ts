import { ApiOperation } from '@nestjs/swagger';
import { JobsService } from './jobs.service';
import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { AppSwaggerTag } from '../app-swagger/app-swagger.constant';
import { ApiErrorResponse, ApiPaginatedResponse } from '@libs/utils/swagger';
import { AppApiPaginatedResponse } from '@libs/utils/responses';
import { FilterJobDto } from './dtos/filter-job.dto';
import { DeleteTotalJobDto } from './dtos/delete-job.dto';
import { JobResponse } from './responses/job.response';
import { Public } from '../auth/guards/public.guard';

@Controller('jobs')
export class JobsController {
  constructor(private readonly jobsService: JobsService) {}

  @ApiOperation({ tags: [AppSwaggerTag.Jobs] })
  @ApiPaginatedResponse(JobResponse)
  @ApiErrorResponse()
  @Get('/cron')
  async listTopicJobs(@Query() options: FilterJobDto) {
    const result = await this.jobsService.listJobCron(options);
    return AppApiPaginatedResponse.create(result.data, result.pagination);
  }

  @ApiOperation({ tags: [AppSwaggerTag.Jobs] })
  @Post('/clean')
  @Public()
  async clean(@Body() data: DeleteTotalJobDto) {
    await this.jobsService.clean(data);
  }

  // @ApiOperation({ tags: [AppSwaggerTag.Jobs] })
  // @Post('/run-again')
  // async runAgian(@Body() data: JobRunAgainDto) {
  //   await this.jobsService.runAgain(data);
  // }

  // @ApiOperation({ tags: [AppSwaggerTag.Jobs] })
  // @ApiPaginatedResponse(JobStoryResponse)
  // @ApiErrorResponse()
  // @Get('/story')
  // async listStoryJobs(@Query() options: FilterJobDto) {
  //   const result = await this.jobsService.listStoryJobs(options);
  //   return AppApiPaginatedResponse.create(result.data, result.pagination);
  // }
}
