import { PhotoResponse } from './photo.response';

export class FromResponse {
  id: number;
  is_bot: boolean;
  first_name: string;
  username: string;
}

export class ChatResponse {
  id: number;
  title: string;
  type: string;
}

export class VideoResponse {
  duration: number;
  width: number;
  height: number;
  file_name: string;
  mime_type: string;
  thumbnail: PhotoResponse;
  thumb: PhotoResponse;
  file_id: string;
  file_unique_id: string;
  file_size: number;
}

export class ResultResponse {
  message_id: number;
  from: FromResponse;
  chat: ChatResponse;
  date: number;
  video: VideoResponse;
}

export class TelegramUploadVideoResponse {
  ok: boolean;
  result: ResultResponse;
}
