import { ApiProperty } from '@nestjs/swagger';
import { BaseDto } from '@server/validators/base.dto';
import { IsOptional } from 'class-validator';

export class UploadDto extends BaseDto {
  @IsOptional()
  @ApiProperty()
  src: string;

  @IsOptional()
  @ApiProperty()
  name: string;
}
