import { StoredFileDriver } from '@libs/constants/entities/StoredFile';
import { StoredFile } from '@libs/entities/entities/StoredFile';
import { Options } from './uploads.interface';

export interface UploadInterface {
  uploadRemoteFile(
    url: string,
    name: string,
    options: Options,
    driver: StoredFileDriver
  ): Promise<StoredFile>;
  removeFile(storedFile: StoredFile): Promise<void>
}
