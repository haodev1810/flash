import { Folder } from './folder.config';

export interface Options {
  bucket?: string;
  folderPath?: Folder;
}
