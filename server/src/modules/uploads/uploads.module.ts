import { UploadsController } from './uploads.controller';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { UploadsService } from './uploads.service';
import { Module } from '@nestjs/common';
import { StoredFile } from '@libs/entities/entities/StoredFile';
import { User } from '@libs/entities/entities/User';
import { CloudinaryProvider } from './cloudinary/cloudinary.provider';
import { CloudinaryService } from './cloudinary/cloudinary.service';
import { MinioService } from './minio/minio.service';
import { TelegramService } from './telegram/telegram.service';
import { Video } from '@libs/entities/entities/Video';

@Module({
  imports: [MikroOrmModule.forFeature([StoredFile, User, Video])],
  controllers: [UploadsController],
  providers: [
    UploadsService,
    CloudinaryProvider,
    CloudinaryService,
    MinioService,
    TelegramService,
  ],
  exports: [
    CloudinaryProvider,
    CloudinaryService,
    MinioService,
    UploadsService,
    TelegramService,
  ],
})
export class UploadsModule {}
