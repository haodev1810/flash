import { Body, Controller, Post } from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { AppSwaggerTag } from '../app-swagger/app-swagger.constant';
import { UploadDto } from './dto/upload.dto';
import { TelegramService } from './telegram/telegram.service';
import { Public } from '../auth/guards/public.guard';

@Controller('uploads')
export class UploadsController {
  constructor(private readonly telegramService: TelegramService) {}

  @ApiOperation({ tags: [AppSwaggerTag.Upload] })
  @Post('/video')
  @Public()
  async uploadVideo(@Body() data: UploadDto) {
    return await this.telegramService.uploadVideoRemoteUrl(data.src, data.name);
  }
}
