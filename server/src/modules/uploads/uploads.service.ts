import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/postgresql';
import 'multer';
import { StoredFile } from '@libs/entities/entities/StoredFile';
import { Options } from './uploads.interface';
import { MinioService } from './minio/minio.service';
import { CloudinaryService } from './cloudinary/cloudinary.service';
import { StoredFileDriver } from '@libs/constants/entities/StoredFile';
import { UploadInterface } from './interface';

type UploadServiceProvider = MinioService | CloudinaryService;

@Injectable()
export class UploadsService implements UploadInterface {
  constructor(
    @InjectRepository(StoredFile)
    private readonly storedFileRepository: EntityRepository<StoredFile>,
    private readonly minioService: MinioService,
    private readonly cloudinaryService: CloudinaryService
  ) {}

  async uploadRemoteFile(
    url: string,
    name: string,
    options: Options,
    driver: StoredFileDriver
  ): Promise<StoredFile> {
    const uploadService = this.getServiceByDriver(driver);
    return await uploadService.uploadRemoteFile(url, name, options)
  }

  private getServiceByDriver(driver: StoredFileDriver): UploadServiceProvider {
    if (driver === StoredFileDriver.Cloudinary) {
      return this.cloudinaryService;
    } else if (driver === StoredFileDriver.Minio) {
      return this.minioService;
    }
  }

  async removeFile(storedFile: StoredFile): Promise<void>{

  }

}
