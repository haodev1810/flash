import { User } from '@libs/entities/entities/User';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityManager, EntityRepository } from '@mikro-orm/postgresql';
import { BadRequestException, Inject, Injectable, Scope } from '@nestjs/common';
import { FilterCollectionDto } from './dtos/filter-collection.dto';
import { Collections } from '@libs/entities/entities/Collection';
import { CollectionItem } from '@libs/entities/entities/CollectionItem';
import { Pagination } from '@libs/utils/responses';
import { CreateCollectionDto } from './dtos/create-collection.dto';
import slugify from 'slugify';
import { UpdateCollectionDto } from './dtos/update-collection.dto';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { FilterQuery, FindOptions, wrap } from '@mikro-orm/core';
import { PushItemsDto } from './dtos/push-items.dto';
import { PopItemsDto } from './dtos/pop-items.dto';
import { FilterImage } from '@libs/constants/filter';
import { AbilityFactory } from '../auth/ability/ability.factory';
import { IdAction } from '@libs/constants/abilities';
import { ForbiddenError } from '@casl/ability';
import { DownloadCollectionDto } from './dtos/download-collection.dto';
import { FilterImagesCollectionDto } from './dtos/filter-images-collection.dto';
import { Item } from '@libs/entities/entities/Item';

@Injectable({ scope: Scope.REQUEST })
export class CollectionsService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: EntityRepository<User>,
    @InjectRepository(Collections)
    private readonly collectionRepository: EntityRepository<Collections>,
    @InjectRepository(CollectionItem)
    private readonly collectionItemRepository: EntityRepository<CollectionItem>,
    @InjectRepository(Item)
    private readonly itemRepository: EntityRepository<Item>,
    private readonly em: EntityManager,
    @Inject(REQUEST) private request: Request,
    private readonly ability: AbilityFactory
  ) {}

  async list(options: FilterCollectionDto): Promise<{
    data: Collections[];
    pagination: Pagination;
  }> {
    const where: FilterQuery<Collections> = {};
    const filter: FindOptions<
      Collections,
      | 'owner.avatar'
      | 'owner.avatar'
      | 'owner'
      | 'owner.name'
      | 'main.images.large'
      | 'main.images.medium'
      | 'main.images.small'
      | 'sub.images.large'
      | 'sub.images.medium'
      | 'sub.images.small'
      | 'sub2.images.large'
      | 'sub2.images.medium'
      | 'sub2.images.small'
    > = {};
    if (options.query) {
      where.$or = [
        {
          title: { $fulltext: options.query },
        },
        {
          slug: { $fulltext: slugify(options.query, { lower: true }) },
        },
      ];
    }
    if (options.image) {
      switch (options.image) {
        case FilterImage.All: {
          filter.populate = [
            'main.images.large',
            'main.images.medium',
            'main.images.small',
            'sub.images.large',
            'sub.images.medium',
            'sub.images.small',
            'sub2.images.large',
            'sub2.images.medium',
            'sub2.images.small',
            'owner.avatar',
          ];
          break;
        }
        case FilterImage.Large: {
          filter.populate = [
            'main.images.large',
            'sub.images.large',
            'sub2.images.large',
            'owner.avatar',
            'owner',
          ];
          break;
        }
        case FilterImage.Medium: {
          filter.populate = [
            'main.images.medium',
            'sub.images.medium',
            'sub2.images.medium',
            'owner.avatar',
            'owner',
          ];
          break;
        }
        case FilterImage.Small: {
          filter.populate = [
            'main.images.small',
            'sub.images.small',
            'sub2.images.small',
            'owner.avatar',
            'owner',
          ];
          break;
        }
        default: {
          filter.populate = ['owner.avatar'];
          break;
        }
      }
    }
    filter.fields = [
      'title',
      'slug',
      'public',
      'enable_download',
      'downloads',
      'views',
      'main.images.large',
      'main.images.medium',
      'main.images.small',
      'sub.images.large',
      'sub.images.medium',
      'sub.images.small',
      'sub2.images.large',
      'sub2.images.medium',
      'sub2.images.small',
      'owner.avatar',
      'owner.name',
    ];
    where.public = true;
    const [data, count] = await this.collectionRepository.findAndCount(where, {
      ...filter,
      limit: options.limit,
      offset: (options.page - 1) * options.limit,
    });
    return {
      data,
      pagination: {
        page: options.page,
        limit: options.limit,
        lastPage: Math.ceil(count / options.limit),
        total: count,
      },
    };
  }

  async myCollection(options: FilterCollectionDto): Promise<{
    data: Collections[];
    pagination: Pagination;
  }> {
    const where: FilterQuery<Collections> = {};
    const filter: FindOptions<
      Collections,
      | 'owner.avatar'
      | 'owner.avatar.path'
      | 'owner'
      | 'owner.name'
      | 'main.images.large'
      | 'main.images.medium'
      | 'main.images.small'
      | 'sub.images.large'
      | 'sub.images.medium'
      | 'sub.images.small'
      | 'sub2.images.large'
      | 'sub2.images.medium'
      | 'sub2.images.small'
    > = {};
    if (options.query) {
      where.$or = [
        {
          title: { $fulltext: options.query },
        },
        {
          slug: { $fulltext: slugify(options.query, { lower: true }) },
        },
      ];
    }
    if (options.image) {
      switch (options.image) {
        case FilterImage.All: {
          filter.populate = [
            'main.images.large',
            'main.images.medium',
            'main.images.small',
            'sub.images.large',
            'sub.images.medium',
            'sub.images.small',
            'sub2.images.large',
            'sub2.images.medium',
            'sub2.images.small',
            'owner.avatar',
          ];
          break;
        }
        case FilterImage.Large: {
          filter.populate = [
            'main.images.large',
            'sub.images.large',
            'sub2.images.large',
            'owner.avatar',
            'owner',
          ];
          break;
        }
        case FilterImage.Medium: {
          filter.populate = [
            'main.images.medium',
            'sub.images.medium',
            'sub2.images.medium',
            'owner.avatar',
            'owner',
          ];
          break;
        }
        case FilterImage.Small: {
          filter.populate = [
            'main.images.small',
            'sub.images.small',
            'sub2.images.small',
            'owner.avatar',
            'owner',
          ];
          break;
        }
        default: {
          filter.populate = ['owner.avatar'];
          break;
        }
      }
    }
    filter.fields = [
      'title',
      'slug',
      'public',
      'enable_download',
      'downloads',
      'views',
      'main.images.large',
      'main.images.medium',
      'main.images.small',
      'sub.images.large',
      'sub.images.medium',
      'sub.images.small',
      'sub2.images.large',
      'sub2.images.medium',
      'sub2.images.small',
      'owner.avatar.path',
      'owner.name',
    ];
    where.owner_id = this.request.user.id;
    const [data, count] = await this.collectionRepository.findAndCount(where, {
      ...filter,
      limit: options.limit,
      offset: (options.page - 1) * options.limit,
    });
    return {
      data,
      pagination: {
        page: options.page,
        limit: options.limit,
        lastPage: Math.ceil(count / options.limit),
        total: count,
      },
    };
  }

  async findOne(slug: string) {
    const collection = await this.collectionRepository.findOneOrFail(
      { slug },
      {
        populate: [
          'owner.avatar',
          'main.images.large',
          'sub.images.large',
          'sub2.images.large',
          'main.images.medium',
          'sub.images.medium',
          'sub2.images.medium',
          'main.images.small',
          'sub.images.small',
          'sub2.images.small',
        ],
      }
    );
    collection.views = collection.views + 1;
    await this.collectionRepository.persistAndFlush(collection);
    return collection;
  }

  async listImagesCollections(
    slug: string,
    options: FilterImagesCollectionDto
  ): Promise<{
    data: Item[];
    pagination: Pagination;
  }> {
    const where: FilterQuery<Item> = {};
    const filter: FindOptions<
      Item,
      | 'tags'
      | 'author.avatar'
      | 'images.small'
      | 'images.medium'
      | 'images.large'
      | 'images.small'
      | 'images.large'
      | 'images.medium'
      | 'author.avatar'
      | 'author.name'
      | 'author.username'
    > = {};
    const page = options.page || 1;
    const limit = options.limit || 30;
    where.collections = { slug: slug };
    if (options.sort) {
      filter.orderBy = {
        views: options.sort,
      };
    }
    if (options.image) {
      switch (options.image) {
        case FilterImage.All: {
          filter.populate = [
            'images.small',
            'images.large',
            'images.medium',
            'author.avatar',
          ];
          break;
        }
        case FilterImage.Large: {
          filter.populate = ['images.large', 'author.avatar'];
          break;
        }
        case FilterImage.Medium: {
          filter.populate = ['images.medium', 'author.avatar'];
          break;
        }
        case FilterImage.Small: {
          filter.populate = ['images.small', 'author.avatar'];
          break;
        }
        default: {
          filter.populate = ['author.avatar'];
          break;
        }
      }
    }
    filter.fields = [
      'title',
      'slug',
      'alt',
      'views',
      'downloads',
      'aspect_ratio',
      'images.small',
      'images.large',
      'images.medium',
      'author.avatar',
      'author.name',
      'author.username',
    ];
    const [data, count] = await this.itemRepository.findAndCount(where, {
      ...filter,
      limit,
      offset: (page - 1) * limit,
    });
    return {
      data,
      pagination: {
        page,
        limit,
        lastPage: Math.ceil(count / limit),
        total: count,
      },
    };
  }

  async create(data: CreateCollectionDto) {
    let collection = await this.collectionRepository.findOne({
      slug: data.slug,
    });
    if (collection) {
      throw new BadRequestException('Title Collection is exits');
    }
    collection = this.collectionRepository.create({
      title: data.title,
      slug: slugify(data.slug),
      description: data.description,
      public: data.public,
      enable_download: data.enable_download,
      main_id: data.main_id,
      sub_id: data.sub_id,
      sub2_id: data.sub2_id,
      items: data.itemIds,
      owner_id: this.request.user.id,
      created_by: this.request.user.id,
      updated_by: this.request.user.id,
    });
    await this.collectionRepository.persistAndFlush(collection);
    return collection;
  }

  async update(id: number, data: UpdateCollectionDto): Promise<Collections> {
    const collection = await this.collectionRepository.findOneOrFail(id);
    const ability = this.ability.defineAbility(this.request.user);
    ForbiddenError.from(ability)
      .setMessage('Unauthorize update this collection')
      .throwUnlessCan(IdAction.Update, collection);
    wrap(collection).assign({
      ...data,
      updated_by: this.request.user.id,
    });
    await this.collectionRepository.persistAndFlush(collection);
    return collection;
  }

  async push(data: PushItemsDto): Promise<Collections> {
    const collection = await this.collectionRepository.findOneOrFail({
      id: data.collection_id,
    });
    const ability = this.ability.defineAbility(this.request.user);
    ForbiddenError.from(ability)
      .setMessage('Unauthorize add to this collection')
      .throwUnlessCan(IdAction.Push, collection);
    await collection.populate(['items']);
    const { items } = collection;
    const itemIds = items.map((i) => i.id);

    wrap(collection).assign({
      items: [...data.itemIds, ...itemIds],
    });
    await this.collectionRepository.persistAndFlush(collection);
    return collection;
  }

  async pop(data: PopItemsDto) {
    const collection = await this.collectionRepository.findOneOrFail({
      id: data.collection_id,
    });
    const ability = this.ability.defineAbility(this.request.user);
    ForbiddenError.from(ability)
      .setMessage('Unauthorize remove item in this collection')
      .throwUnlessCan(IdAction.Pop, collection);
    await collection.populate(['items']);
    const { items } = collection;
    const itemIds = items.map((i) => i.id);
    const remainder = itemIds.filter((i) => !data.itemIds.includes(i));
    wrap(collection).assign({
      items: [...remainder],
    });
    await this.collectionRepository.persistAndFlush(collection);
    return collection;
  }

  async download(data: DownloadCollectionDto): Promise<Collections> {
    const collection = await this.collectionRepository.findOneOrFail(
      {
        id: data.id,
      },
      {
        fields: [
          'id',
          'title',
          'slug',
          'items.slug',
          'items.images.large.path',
          'items.images.medium.path',
          'items.images.small.path',
        ],
      }
    );
    if (!collection.enable_download) {
      const ability = this.ability.defineAbility(this.request.user);
      ForbiddenError.from(ability)
        .setMessage('Unauthorize download this collection')
        .throwUnlessCan(IdAction.Pop, collection);
    }
    await collection.populate([
      'items.slug',
      'items.images.large',
      'items.images.medium',
      'items.images.small',
    ]);
    return collection;
  }

  async delete(id: number) {
    const collection = await this.collectionRepository.findOneOrFail({ id });
    const ability = this.ability.defineAbility(this.request.user);
    ForbiddenError.from(ability)
      .setMessage('Unauthorize delete this collection')
      .throwUnlessCan(IdAction.Pop, collection);
    await this.collectionRepository.removeAndFlush(collection);
    return collection;
  }
}
