import { FilterImage } from '@libs/constants/filter';
import { ApiProperty } from '@nestjs/swagger';
import { PaginationPageDto } from '@server/validators/pagination-page';
import { IsOptional } from 'class-validator';

export class FilterCollectionDto extends PaginationPageDto {
  @IsOptional()
  @ApiProperty({ nullable: true, required: false })
  query: string;

  @IsOptional()
  @ApiProperty({
    type: 'enum',
    enum: [
      FilterImage.All,
      FilterImage.Large,
      FilterImage.Medium,
      FilterImage.Small,
    ],
  })
  image: FilterImage;
}
