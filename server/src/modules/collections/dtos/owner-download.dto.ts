import { ApiProperty } from '@nestjs/swagger';
import { BaseDto } from '@server/validators/base.dto';
import { IsCollectionId } from '@server/validators/is-collection-id.validatior';
import { Type } from 'class-transformer';
import { IsNumber, IsOptional, IsPositive } from 'class-validator';

export class OwnerDownloadDto extends BaseDto {
  @IsOptional()
  @ApiProperty()
  @IsNumber()
  @IsPositive()
  @Type(() => Number)
  @IsCollectionId()
  id: number;
}
