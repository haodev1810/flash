import { ApiProperty } from '@nestjs/swagger';
import { BaseDto } from '@server/validators/base.dto';
import { IsCollectionId } from '@server/validators/is-collection-id.validatior';
import { Type } from 'class-transformer';
import { IsDefined, IsNumber, IsOptional, IsPositive } from 'class-validator';

export class PushItemsDto extends BaseDto {
  @IsOptional()
  @IsDefined()
  @IsNumber()
  @IsPositive()
  @Type(() => Number)
  @ApiProperty()
  @IsCollectionId()
  collection_id: number;

  @IsOptional()
  @ApiProperty({ type: 'number', isArray: true })
  itemIds: number[];
}
