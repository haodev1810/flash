import { ApiProperty } from '@nestjs/swagger';
import { BaseDto } from '@server/validators/base.dto';
import { IsDefined, IsOptional } from 'class-validator';

export class CreateCollectionDto extends BaseDto {
  @IsOptional()
  @IsDefined()
  @ApiProperty()
  title: string;

  @IsOptional()
  @ApiProperty({ nullable: true, required: false })
  description: string;

  @IsOptional()
  @IsDefined()
  @ApiProperty()
  slug: string;

  @IsOptional()
  @IsDefined()
  @ApiProperty()
  public: boolean;

  @IsOptional()
  @IsDefined()
  @ApiProperty()
  enable_download: boolean;

  @IsOptional()
  @IsDefined()
  @ApiProperty()
  main_id: number;

  @IsOptional()
  @IsDefined()
  @ApiProperty()
  sub_id: number;

  @IsOptional()
  @IsDefined()
  @ApiProperty()
  sub2_id: number;

  @IsOptional()
  @IsDefined()
  @ApiProperty({ isArray: true, type: 'number' })
  itemIds: number[];
}
