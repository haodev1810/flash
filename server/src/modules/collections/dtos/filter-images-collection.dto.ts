import { FilterImage, FilterSort } from '@libs/constants/filter';
import { ApiProperty } from '@nestjs/swagger';
import { PaginationPageDto } from '@server/validators/pagination-page';
import { IsOptional } from 'class-validator';

export class FilterImagesCollectionDto extends PaginationPageDto {
  @IsOptional()
  @ApiProperty({
    type: 'enum',
    enum: [FilterSort.DESC, FilterSort.ASC],
    default: FilterSort.DESC,
  })
  sort: FilterSort;

  @IsOptional()
  @ApiProperty({
    type: 'enum',
    enum: [
      FilterImage.All,
      FilterImage.Large,
      FilterImage.Medium,
      FilterImage.Small,
    ],
    default: FilterImage.Medium,
  })
  image: FilterImage;
}
