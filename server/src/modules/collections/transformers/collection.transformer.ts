import { Collections } from '@libs/entities/entities/Collection';
import { BaseResponseTransformer } from '@server/transformers/base-response.transformer';
import { CollectionResponse } from '../responses/collection.response';
import { AccountTransformer } from '@server/modules/account/transformers/account.transformer';
import { ItemTransformer } from '@server/modules/items/transformers/item.transformer';
import { CollectionFullResponse } from '../responses/collection-full.response';
import { IdSubject } from '@libs/constants/abilities';

export class CollectionTransformer extends BaseResponseTransformer {
  static toCollectionResponse(collection: Collections): CollectionResponse {
    return {
      ...BaseResponseTransformer.transformEntityTimestamps(collection),
      owner: collection.owner
        ? AccountTransformer.toAccountResponse(collection.owner)
        : null,
      main: collection.main
        ? ItemTransformer.toItemResponse(collection.main)
        : null,
      sub: collection.sub
        ? ItemTransformer.toItemResponse(collection.sub)
        : null,
      sub2: collection.sub2
        ? ItemTransformer.toItemResponse(collection.sub2)
        : null,
      __typename: IdSubject.Collections,
    };
  }

  static toCollectionFullResponse(
    collection: Collections
  ): CollectionFullResponse {
    return {
      ...CollectionTransformer.toCollectionResponse(collection),
      items: collection.items?.map(ItemTransformer.toItemResponse),
    };
  }
}
