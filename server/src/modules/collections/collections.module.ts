import { CollectionsService } from './collections.service';
import { CollectionsController } from './collections.controller';
import { Module } from '@nestjs/common';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { User } from '@libs/entities/entities/User';
import { Item } from '@libs/entities/entities/Item';
import { Collections } from '@libs/entities/entities/Collection';
import { CollectionItem } from '@libs/entities/entities/CollectionItem';
import { AbilityModule } from '../auth/ability/ability.module';
import { IsCollectionIdValidator } from '@server/validators/is-collection-id.validatior';
import { IsCollectionEnableDownloadValidator } from '@server/validators/is-collection-enable-download.validator';

@Module({
  imports: [
    MikroOrmModule.forFeature([User, Item, Collections, CollectionItem]),
    AbilityModule,
  ],
  controllers: [CollectionsController],
  providers: [
    CollectionsService,
    IsCollectionIdValidator,
    IsCollectionEnableDownloadValidator,
  ],
})
export class CollectionsModule {}
