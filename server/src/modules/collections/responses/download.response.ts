import { ApiProperty } from '@nestjs/swagger';
import { ItemResponse } from '@server/modules/items/responses/item.response';

export class DownloadResponse {
  @ApiProperty({ type: ItemResponse, isArray: true })
  items: ItemResponse[];
}
