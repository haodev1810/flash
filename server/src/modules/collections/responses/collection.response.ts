import { ApiProperty } from '@nestjs/swagger';
import { AccountResponse } from '@server/modules/account/responses/account.response';
import { ItemResponse } from '@server/modules/items/responses/item.response';
import { BaseSerialKeyResponse } from '@server/responses/base-serial-key.response';

export class CollectionResponse extends BaseSerialKeyResponse {
  @ApiProperty()
  title: string;

  @ApiProperty()
  slug: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  views: number;

  @ApiProperty()
  downloads: number;

  @ApiProperty()
  public: boolean;

  @ApiProperty()
  enable_download: boolean;

  @ApiProperty({ type: AccountResponse, nullable: true })
  owner: AccountResponse;

  @ApiProperty({ type: ItemResponse, nullable: true })
  main: ItemResponse;

  @ApiProperty({ type: ItemResponse, nullable: true })
  sub: ItemResponse;

  @ApiProperty({ type: ItemResponse, nullable: true })
  sub2: ItemResponse;
}
