import { ApiProperty } from '@nestjs/swagger';
import { CollectionResponse } from './collection.response';
import { ItemResponse } from '@server/modules/items/responses/item.response';

export class CollectionFullResponse extends CollectionResponse {
  @ApiProperty({ type: ItemResponse, isArray: true, nullable: true })
  items: ItemResponse[];
}
