import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CollectionsService } from './collections.service';
import { ApiOperation } from '@nestjs/swagger';
import { AppSwaggerTag } from '../app-swagger/app-swagger.constant';
import { Public } from '../auth/guards/public.guard';
import { AbilitiesGuard } from '../auth/ability/ability.guard';
import { FilterCollectionDto } from './dtos/filter-collection.dto';
import {
  ApiErrorResponse,
  ApiPaginatedResponse,
  ApiSuccessResponse,
} from '@libs/utils/swagger';
import { CollectionResponse } from './responses/collection.response';
import {
  AppApiPaginatedResponse,
  AppApiSuccessResponse,
} from '@libs/utils/responses';
import { CollectionTransformer } from './transformers/collection.transformer';
import { CreateCollectionDto } from './dtos/create-collection.dto';
import { UpdateCollectionDto } from './dtos/update-collection.dto';
import { PushItemsDto } from './dtos/push-items.dto';
import { PopItemsDto } from './dtos/pop-items.dto';
import {
  CheckPolicies,
  InsertCollectionPolicyHandler,
} from '../auth/ability/ability.decorator';
import { DownloadCollectionDto } from './dtos/download-collection.dto';
import { CollectionFullResponse } from './responses/collection-full.response';
import { FilterImagesCollectionDto } from './dtos/filter-images-collection.dto';
import { ItemResponse } from '../items/responses/item.response';
import { ItemTransformer } from '../items/transformers/item.transformer';

@UseGuards(AbilitiesGuard)
@Controller('collections')
export class CollectionsController {
  constructor(private readonly collectionService: CollectionsService) {}

  @ApiOperation({ tags: [AppSwaggerTag.Collections] })
  @ApiPaginatedResponse(CollectionResponse)
  @ApiErrorResponse()
  @Public()
  @Get()
  async list(@Query() options: FilterCollectionDto) {
    const result = await this.collectionService.list(options);
    return AppApiPaginatedResponse.create(
      result.data.map(CollectionTransformer.toCollectionResponse),
      result.pagination
    );
  }

  @ApiOperation({ tags: [AppSwaggerTag.Collections] })
  @ApiPaginatedResponse(CollectionResponse)
  @ApiErrorResponse()
  @Get('/my-collections')
  async myCollection(@Query() options: FilterCollectionDto) {
    const result = await this.collectionService.myCollection(options);
    return AppApiPaginatedResponse.create(
      result.data.map(CollectionTransformer.toCollectionResponse),
      result.pagination
    );
  }

  @ApiOperation({ tags: [AppSwaggerTag.Collections] })
  @ApiPaginatedResponse(CollectionResponse)
  @ApiErrorResponse()
  @Post('/')
  @CheckPolicies(new InsertCollectionPolicyHandler())
  async create(@Body() data: CreateCollectionDto) {
    return AppApiSuccessResponse.create(
      CollectionTransformer.toCollectionResponse(
        await this.collectionService.create(data)
      )
    );
  }

  @ApiOperation({ tags: [AppSwaggerTag.Collections] })
  @ApiPaginatedResponse(CollectionResponse)
  @ApiErrorResponse()
  @Post('/push')
  async push(@Body() data: PushItemsDto) {
    return AppApiSuccessResponse.create(
      CollectionTransformer.toCollectionResponse(
        await this.collectionService.push(data)
      )
    );
  }

  @ApiOperation({ tags: [AppSwaggerTag.Collections] })
  @ApiPaginatedResponse(CollectionResponse)
  @ApiErrorResponse()
  @Post('/pop')
  async pop(@Body() data: PopItemsDto) {
    return AppApiSuccessResponse.create(
      CollectionTransformer.toCollectionResponse(
        await this.collectionService.pop(data)
      )
    );
  }

  @ApiOperation({ tags: [AppSwaggerTag.Collections] })
  @ApiSuccessResponse(CollectionFullResponse)
  @ApiErrorResponse()
  @Post('/download')
  async download(@Body() data: DownloadCollectionDto) {
    return AppApiSuccessResponse.create(
      CollectionTransformer.toCollectionFullResponse(
        await this.collectionService.download(data)
      )
    );
  }

  @ApiOperation({ tags: [AppSwaggerTag.Collections] })
  @ApiSuccessResponse(CollectionResponse)
  @ApiErrorResponse()
  @Get('/:slug')
  @Public()
  async findOne(@Param('slug') slug: string) {
    return AppApiSuccessResponse.create(
      CollectionTransformer.toCollectionResponse(
        await this.collectionService.findOne(slug)
      )
    );
  }

  @ApiOperation({ tags: [AppSwaggerTag.Collections] })
  @ApiPaginatedResponse(ItemResponse)
  @ApiErrorResponse()
  @Get('/:slug/images')
  @Public()
  async listImagesCollection(
    @Param('slug') slug: string,
    @Query() options: FilterImagesCollectionDto
  ) {
    const result = await this.collectionService.listImagesCollections(
      slug,
      options
    );
    return AppApiPaginatedResponse.create(
      result.data.map(ItemTransformer.toItemResponse),
      result.pagination
    );
  }

  @ApiOperation({ tags: [AppSwaggerTag.Collections] })
  @ApiPaginatedResponse(CollectionResponse)
  @ApiErrorResponse()
  @Put('/:id')
  async update(
    @Param(
      'id',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })
    )
    id: number,
    @Body() data: UpdateCollectionDto
  ) {
    return AppApiSuccessResponse.create(
      CollectionTransformer.toCollectionResponse(
        await this.collectionService.update(id, data)
      )
    );
  }
}
