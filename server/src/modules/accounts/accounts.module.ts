import { AccountsService } from './accounts.service';
import { AccountsController } from './accounts.controller';
import { Module } from '@nestjs/common';
import { AbilityModule } from '../auth/ability/ability.module';

@Module({
  imports: [AbilityModule],
  controllers: [AccountsController],
  providers: [AccountsService],
})
export class AccountsModule {}
