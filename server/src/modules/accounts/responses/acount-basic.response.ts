import { ApiProperty } from '@nestjs/swagger';
import { IdSubject } from '@libs/constants/abilities';

export class AccountBasicResponse {
  @ApiProperty()
  id: number;

  @ApiProperty()
  name: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  created_at: string;

  @ApiProperty()
  updated_at: string;

  @ApiProperty({ type: AccountBasicResponse, nullable: true })
  creator: AccountBasicResponse | null;

  @ApiProperty({ type: AccountBasicResponse, nullable: true })
  updater: AccountBasicResponse | null;

  @ApiProperty({ enum: IdSubject, required: false, nullable: true })
  __typename?: IdSubject;
}
