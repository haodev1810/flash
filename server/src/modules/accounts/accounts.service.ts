/*
https://docs.nestjs.com/providers#services
*/

import { Injectable, Scope } from '@nestjs/common';

@Injectable({ scope: Scope.REQUEST })
export class AccountsService {
  constructor() {}
}
