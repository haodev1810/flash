import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { PexelsController } from './pexels/pexels.controller';
import { PexelsService } from './pexels/pexels.service';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { User } from '@libs/entities/entities/User';
import { Author } from '@libs/entities/entities/Author';
import { Tag } from '@libs/entities/entities/Tag';
import { Item } from '@libs/entities/entities/Item';
import { Images } from '@libs/entities/entities/Images';
import { TagItem } from '@libs/entities/entities/TagItem';
import { StoredFile } from '@libs/entities/entities/StoredFile';
import { QueueName } from '@libs/constants/queue';
import { Topic } from '@libs/entities/entities/Topic';
import { AbilityModule } from '../auth/ability/ability.module';
import { Video } from '@libs/entities/entities/Video';
import { UploadsModule } from '../uploads/uploads.module';

@Module({
  imports: [
    AbilityModule,
    UploadsModule,
    MikroOrmModule.forFeature([
      User,
      Author,
      Tag,
      Item,
      Images,
      TagItem,
      StoredFile,
      Topic,
      Video,
    ]),
    BullModule.registerQueue({
      name: QueueName.BotCron,
    }),
  ],
  controllers: [PexelsController],
  providers: [PexelsService],
})
export class CrawlsModule {}
