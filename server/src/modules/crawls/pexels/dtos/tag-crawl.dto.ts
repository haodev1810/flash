import { ApiProperty } from '@nestjs/swagger';
import { BaseDto } from '@server/validators/base.dto';
import { IsDefined, IsOptional } from 'class-validator';
import { StoredFileDriver } from '@libs/constants/entities/StoredFile';
import { TypeItem } from '@libs/constants/entities/Item';

export class TagCrawlDto extends BaseDto {
  @IsOptional()
  @IsDefined()
  @ApiProperty()
  topic: string;

  @IsOptional()
  @IsDefined()
  @ApiProperty({
    enum: [
      StoredFileDriver.Minio,
      StoredFileDriver.Cloudinary,
      StoredFileDriver.Telegram,
      StoredFileDriver.Tiktok,
    ],
  })
  driver: StoredFileDriver;

  @IsOptional()
  @IsDefined()
  @ApiProperty({ type: 'boolean', default: false })
  createTopic: boolean;

  @IsOptional()
  @IsDefined()
  @ApiProperty({
    enum: [TypeItem.IMAGE, TypeItem.VIDEO],
    default: TypeItem.IMAGE,
  })
  type: TypeItem;
}
