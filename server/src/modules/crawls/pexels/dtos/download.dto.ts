import { ApiProperty } from '@nestjs/swagger';
import { BaseDto } from '@server/validators/base.dto';
import { IsDefined, IsOptional } from 'class-validator';

export class DownloadDto extends BaseDto {
  @IsOptional()
  @IsDefined()
  @ApiProperty()
  url: string;
}
