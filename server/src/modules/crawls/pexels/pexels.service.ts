import { BadRequestException, Injectable } from '@nestjs/common';
import { TagCrawlDto } from './dtos/tag-crawl.dto';
import { InjectQueue } from '@nestjs/bull';
import {
  QueueJobDataPexel,
  QueueJobName,
  QueueName,
} from '@libs/constants/queue';
import { Queue } from 'bull';
import { PexelDataImageResponse } from './interfaces';
import { axiosInstance } from '@server/utils/libs/pexels';
import { InjectRepository } from '@mikro-orm/nestjs';
import { Topic } from '@libs/entities/entities/Topic';
import { EntityManager, EntityRepository } from '@mikro-orm/postgresql';
import slugify from 'slugify';
import { TypeItem } from '@libs/constants/entities/Item';
import { CrawlPexelVideoResponse } from './interfaces/pexel.respoonse';
import { Item } from '@libs/entities/entities/Item';
import { Images } from '@libs/entities/entities/Images';
import { Video } from '@libs/entities/entities/Video';
import { Author } from '@libs/entities/entities/Author';
import { TelegramService } from '@server/modules/uploads/telegram/telegram.service';
import { UploadsService } from '@server/modules/uploads/uploads.service';
import { StoredFileDriver } from '@libs/constants/entities/StoredFile';
import { TagItem } from '@libs/entities/entities/TagItem';
import { Tag } from '@libs/entities/entities/Tag';
import { DownloadDto } from './dtos/download.dto';
import * as fs from 'fs'
@Injectable()
export class PexelsService {
  constructor(
    @InjectRepository(Topic)
    private readonly topicRepository: EntityRepository<Topic>,
    @InjectRepository(Item)
    private readonly itemRepository: EntityRepository<Item>,
    @InjectRepository(Images)
    private readonly imagesRepository: EntityRepository<Images>,
    @InjectRepository(Video)
    private readonly videoRepository: EntityRepository<Video>,
    @InjectRepository(Author)
    private readonly authorRepository: EntityRepository<Author>,
    @InjectRepository(TagItem)
    private readonly tagItemRepository: EntityRepository<TagItem>,
    @InjectRepository(Tag)
    private readonly tagRepository: EntityRepository<Tag>,
    @InjectQueue(QueueName.BotCron)
    private readonly botCronQueue: Queue,
    private readonly telegramService: TelegramService,
    private readonly uploadService: UploadsService,
    private readonly em: EntityManager
  ) {}

  async crawler(data: TagCrawlDto) {
    if (data.type == TypeItem.IMAGE) {
      return await this.crawlImages(data);
    }
    if (data.type == TypeItem.VIDEO) {
      if (data.driver == StoredFileDriver.Telegram) {
        return await this.crawlVideos(data);
      } else {
        throw new BadRequestException('Driver is not accepted');
      }
    }
    throw new BadRequestException('Type file is not accepted');
  }

  async crawlImages(data: TagCrawlDto) {
    const baseUrl = process.env.PEXEL_API_END_POINT + '/photos';
    const limit = 24;
    let topic = await this.topicRepository.findOne({
      slug: slugify(data.topic, { lower: true, locale: 'vi' }),
    });
    if (data.createTopic && !topic) {
      topic = this.topicRepository.create({
        title: data.topic,
        slug: slugify(data.topic, { lower: true, locale: 'vi' }),
      });
      await this.topicRepository.persistAndFlush(topic);
    }
    const response = await axiosInstance
      .get<PexelDataImageResponse>(baseUrl, {
        params: {
          page: 1,
          per_page: limit,
          query: data.topic,
          orientation: 'all',
          size: 'all',
          color: 'all',
          seo_tags: true,
        },
      })
      .then((res) => res.data);
    if (response) {
      const pagination = response.pagination;
      for (let i = 1; i <= pagination.total_pages; i++) {
        const payload: QueueJobDataPexel = {
          topic: data.topic,
          driver: data.driver,
          page: i,
          limit: limit,
          type: TypeItem.IMAGE,
        };
        await this.botCronQueue.add(QueueJobName.CronPexel, payload, {
          delay: 1000,
        });
      }
    }
  }

  async crawlVideos(data: TagCrawlDto) {
    const baseUrl = process.env.PEXEL_API_END_POINT + '/videos';
    const limit = 10;
    let topic = await this.topicRepository.findOne({
      slug: slugify(data.topic, { lower: true, locale: 'vi' }),
    });
    if (data.createTopic && !topic) {
      topic = this.topicRepository.create({
        title: data.topic,
        slug: slugify(data.topic, { lower: true, locale: 'vi' }),
      });
      await this.topicRepository.persistAndFlush(topic);
    }
    const response = await axiosInstance
      .get<CrawlPexelVideoResponse>(baseUrl, {
        params: {
          page: 1,
          per_page: limit,
          query: data.topic,
          orientation: 'all',
          size: 'all',
          color: 'all',
          seo_tags: true,
        },
      })
      .then((res) => res.data);
    if (response) {
      const pagination = response.pagination;
      for (let i = 1; i <= pagination.total_pages; i++) {
        const payload: QueueJobDataPexel = {
          topic: data.topic,
          driver: data.driver,
          page: i,
          limit: limit,
          type: TypeItem.VIDEO,
        };
        await this.botCronQueue.add(QueueJobName.CronPexel, payload, {
          delay: 1000,
        });
      }
    }
  }

  async downloadImage(data:DownloadDto){
    const baseUrl = process.env.PEXEL_API_END_POINT + '/videos';
    const limit = 10;
    const response = await axiosInstance
      .get<CrawlPexelVideoResponse>(baseUrl, {
        params: {
          page: 1,
          per_page: limit,
          query: data.url,
          orientation: 'all',
          size: 'all',
          color: 'all',
          seo_tags: true,
        },
      })
      .then((res) => res.data);

    return response;
  }
}
