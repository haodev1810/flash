export interface PexelUser {
  id: number;
  first_name: string;
  last_name: string;
  slug: string;
  avatar: {
    small: string;
    medium: string;
  };
}
export interface PexelTag {
  name: string;
  search_term: string;
}
interface PexelImage {
  small: string;
  medium: string;
  large: string;
}
export interface PexelItemAttributes {
  id: number;
  slug: string;
  description: string;
  publish_at: string;
  title: string;
  aspect_ratio: number;
  license: string;
  published: boolean;
  user: PexelUser;
  tags: PexelTag[];
  image: PexelImage;
  alt: string;
}

export interface PexelItem {
  id: string;
  type: string;
  attributes: PexelItemAttributes;
}
interface PexelPaginateResponse {
  current_page: number;
  total_pages: number;
  total_results: number;
}
export interface PexelDataImageResponse {
  data: PexelItem[];
  pagination: PexelPaginateResponse;
}
