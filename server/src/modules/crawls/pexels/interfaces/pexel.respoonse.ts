export class PaginationPexelResponse {
  current_page: number;
  total_pages: number;
  total_results: number;
}
export class MetaResponse {
  searchable: boolean;
  policy: {
    delete: boolean;
  };
}
export class ThumbnailPexelResponse {
  small: string;
  medium: string;
  large: string;
}
export class VideoFileResponse {
  file_type: string;
  quality: string;
  width: number;
  height: number;
  fps: number;
  link: string;
  download_link: string;
}

export class VideoPexelResponse {
  src: string;
  preview_src: string;
  thumbnail: ThumbnailPexelResponse;
  download: string;
  download_link: string;
  video_files: VideoFileResponse[];
}

export class AvatarUserPexelResponse {
  small: string;
  medium: string;
}

export class TagPexelResponse {
  name: string;
  search_term: string;
}

export class UserPexelResponse {
  id: number;
  first_name: string;
  last_name: string;
  slug: string;
  username: string;
  location: string;
  avatar: AvatarUserPexelResponse;
  hero: boolean;
  following: boolean;
}

export class AtributesResponse {
  id: number;
  slug: string;
  description?: string;
  width: number;
  height: number;
  status: string;
  created_at: string;
  updated_at: string;
  publish_at: string;
  feed_at: null;
  title: string;
  aspect_ratio: number;
  license: string;
  published: boolean;
  starred: boolean;
  pending: boolean;
  user: UserPexelResponse;
  tags: TagPexelResponse[];
  liked: boolean;
  collection_ids: number[];
  donate_url: string;
  vimeo_id: string;
  video: VideoPexelResponse;
}
export class PexelFileVideoResponse {
  id: string;
  type: string;
  attributes: AtributesResponse;
  meta: MetaResponse;
}

export class CrawlPexelVideoResponse {
  data: PexelFileVideoResponse[];
  pagination: PaginationPexelResponse;
}
