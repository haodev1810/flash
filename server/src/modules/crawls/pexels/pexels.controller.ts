import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { PexelsService } from './pexels.service';
import { ApiOperation } from '@nestjs/swagger';
import { AppSwaggerTag } from '@server/modules/app-swagger/app-swagger.constant';
import { TagCrawlDto } from './dtos/tag-crawl.dto';
import { AbilitiesGuard } from '@server/modules/auth/ability/ability.guard';
import {
  CheckPolicies,
  ManageCronPolicyHandler,
} from '@server/modules/auth/ability/ability.decorator';
import { DownloadDto } from './dtos/download.dto';

@UseGuards(AbilitiesGuard)
@Controller('pexels')
export class PexelsController {
  constructor(private readonly pexelService: PexelsService) {}

  @ApiOperation({ tags: [AppSwaggerTag.Pexel] })
  @Post('/crawler')
  @CheckPolicies(new ManageCronPolicyHandler())
  async crawler(@Body() data: TagCrawlDto) {
    return await this.pexelService.crawler(data);
  }

  @ApiOperation({ tags: [AppSwaggerTag.Pexel] })
  @Post('/download')
  @CheckPolicies(new ManageCronPolicyHandler())
  async download(@Body() data: DownloadDto) {
    return await this.pexelService.downloadImage(data);
  }
}
