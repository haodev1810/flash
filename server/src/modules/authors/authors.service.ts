import { Author } from '@libs/entities/entities/Author';
import { Item } from '@libs/entities/entities/Item';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityManager, EntityRepository } from '@mikro-orm/postgresql';
import { BadRequestException, Injectable } from '@nestjs/common';
import { FilterLeaderboardDto } from './dtos/filter-leaderboard.dto';
import { Pagination } from '@libs/utils/responses';

@Injectable()
export class AuthorsService {
  constructor(
    @InjectRepository(Author)
    private readonly authorRepository: EntityRepository<Author>,
    @InjectRepository(Item)
    private readonly itemRepository: EntityRepository<Item>,
    private readonly em: EntityManager
  ) {}

  async listLeaderBoard(options: FilterLeaderboardDto): Promise<{
    data: {
      author: Author;
      items: Item[];
      total: number;
      total_view: number;
    }[];
    pagination: Pagination;
  }> {
    const page = Number(options.page);
    const limit = Number(options.limit);
    if (limit > 50) {
      throw new BadRequestException('limit too larage');
    }
    const countQuery = `SELECT COUNT(*) AS total
    FROM (
      SELECT a.id
      FROM authors a
      JOIN items i ON a.id = i.author_id
      GROUP BY a.id
      HAVING SUM(i.views) > 0
    ) AS subquery`;
    const countResult = await this.em.execute(countQuery);
    const total = countResult[0]['total'];
    const query = `SELECT a.id, SUM(i.views) AS total_views
    FROM authors a
    JOIN items i ON a.id = i.author_id
    GROUP BY a.id
    HAVING SUM(i.views) > 0
    ORDER BY total_views DESC
    LIMIT ${limit} OFFSET ${(page - 1) * limit}`;
    const rankedAuthors = await this.em.execute(query);
    const data = await Promise.all(
      rankedAuthors.map(async (item: { id: number; total_views: string }) => {
        const author = await this.authorRepository.findOne(
          { id: item.id },
          {
            fields: ['id', 'name', 'username', 'avatar.path', 'avatar_large'],
            populate: ['avatar', 'avatar_large'],
          }
        );
        const [items, count] = await this.itemRepository.findAndCount(
          {
            author_id: item.id,
          },
          {
            populate: ['images.medium'],
            fields: [
              'title',
              'slug',
              'alt',
              'images.small',
              'images.large',
              'images.medium',
            ],
            limit: 4,
            orderBy: {
              views: 'DESC',
            },
          }
        );
        return {
          author,
          items,
          total: count,
          total_view: Number(item.total_views),
        };
      })
    );
    return {
      data,
      pagination: {
        page,
        limit,
        total: Number(total),
        lastPage: Math.ceil(Number(total) / limit),
      },
    };
  }

  async profile(
    username: string
  ): Promise<{ total_item: number; total_view: number; author: Author }> {
    const author = await this.authorRepository.findOne(
      { username },
      {
        fields: ['id', 'name', 'username', 'avatar.path', 'avatar_large'],
        populate: ['avatar_large', 'avatar'],
      }
    );
    const query = await this.itemRepository
      .createQueryBuilder()
      .addSelect('sum(views),count(id)')
      .where({ author_id: author.id })
      .execute();
    return {
      author,
      total_item: query[0]['count'],
      total_view: query[0]['sum'],
    };
  }
}
