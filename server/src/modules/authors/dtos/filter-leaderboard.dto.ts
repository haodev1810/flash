import { ApiProperty } from '@nestjs/swagger';
import { PaginationPageDto } from '@server/validators/pagination-page';
import { IsOptional } from 'class-validator';

export enum TypeLeaderboardEnum {
  Recent = 'recent',
  AllTime = 'all_time',
}

export class FilterLeaderboardDto extends PaginationPageDto {
  @IsOptional()
  @ApiProperty({
    enum: [TypeLeaderboardEnum.Recent, TypeLeaderboardEnum.AllTime],
  })
  type: TypeLeaderboardEnum;
}
