import { ApiProperty } from '@nestjs/swagger';
import { StoredFileResponse } from '@server/modules/stored-files/responses/stored-files.response';
import { BaseSerialKeyResponse } from '@server/responses/base-serial-key.response';

export class AuthorResponse extends BaseSerialKeyResponse {
  @ApiProperty()
  name: string;

  @ApiProperty()
  username: string;

  @ApiProperty({ type: StoredFileResponse, nullable: true })
  avatar: StoredFileResponse | null;

  @ApiProperty({ type: StoredFileResponse, nullable: true })
  avatar_large: StoredFileResponse | null;
}
