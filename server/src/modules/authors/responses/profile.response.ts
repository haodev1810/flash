import { ApiProperty } from '@nestjs/swagger';
import { AuthorResponse } from './author.response';

export class ProfileAuthorResponse extends AuthorResponse {
  @ApiProperty()
  total_view: number;

  @ApiProperty()
  total_item: number;
}
