import { ApiProperty } from '@nestjs/swagger';
import { AuthorResponse } from './author.response';
import { ItemResponse } from '@server/modules/items/responses/item.response';

export class LeaderboardResponse {
  @ApiProperty({ type: AuthorResponse })
  author: AuthorResponse;

  @ApiProperty({ type: ItemResponse, isArray: true })
  items: ItemResponse[];

  @ApiProperty()
  total: number;

  @ApiProperty()
  total_view: number;
}
