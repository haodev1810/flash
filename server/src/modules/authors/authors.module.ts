import { MikroOrmModule } from '@mikro-orm/nestjs';
import { AuthorsController } from './authors.controller';
import { AuthorsService } from './authors.service';
import { Module } from '@nestjs/common';
import { User } from '@libs/entities/entities/User';
import { Author } from '@libs/entities/entities/Author';
import { AbilityModule } from '../auth/ability/ability.module';
import { Item } from '@libs/entities/entities/Item';
import { StoredFile } from '@libs/entities/entities/StoredFile';

@Module({
  imports: [
    MikroOrmModule.forFeature([User, Author, Item, StoredFile]),
    AbilityModule,
  ],
  controllers: [AuthorsController],
  providers: [AuthorsService],
})
export class AuthorsModule {}
