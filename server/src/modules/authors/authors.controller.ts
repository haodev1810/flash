import { ApiOperation, ApiProperty } from '@nestjs/swagger';
import { AuthorsService } from './authors.service';
import { Controller, Get, Param, Query } from '@nestjs/common';
import { AppSwaggerTag } from '../app-swagger/app-swagger.constant';
import { Public } from '../auth/guards/public.guard';
import { FilterLeaderboardDto } from './dtos/filter-leaderboard.dto';
import {
  ApiErrorResponse,
  ApiPaginatedResponse,
  ApiSuccessResponse,
} from '@libs/utils/swagger';
import { LeaderboardResponse } from './responses/leaderboard.repsonse';
import {
  AppApiPaginatedResponse,
  AppApiSuccessResponse,
} from '@libs/utils/responses';
import { AuthorTransformer } from './transformers/author.transformer';
import { ProfileAuthorResponse } from './responses/profile.response';

@Controller('authors')
export class AuthorsController {
  constructor(private readonly authorsService: AuthorsService) {}

  @ApiOperation({ tags: [AppSwaggerTag.Authors] })
  @ApiPaginatedResponse(LeaderboardResponse)
  @ApiErrorResponse()
  @Get('/leaderboard')
  @Public()
  async listLeaderboard(@Query() options: FilterLeaderboardDto) {
    const result = await this.authorsService.listLeaderBoard(options);
    return AppApiPaginatedResponse.create(
      result.data.map(AuthorTransformer.toLeaderboardResponse),
      result.pagination
    );
  }

  @ApiOperation({
    tags: [AppSwaggerTag.Authors],
  })
  @ApiSuccessResponse(ProfileAuthorResponse)
  @ApiErrorResponse()
  @Get('/:username')
  @Public()
  async profile(@Param('username') username: string) {
    return AppApiSuccessResponse.create(
      AuthorTransformer.toProfileResponse(
        await this.authorsService.profile(username)
      )
    );
  }
}
