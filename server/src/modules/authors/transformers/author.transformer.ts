import { Author } from '@libs/entities/entities/Author';
import { BaseResponseTransformer } from '@server/transformers/base-response.transformer';
import { AuthorResponse } from '../responses/author.response';
import { StoredFileTransformer } from '@server/modules/stored-files/transformers/stored-files.transformer';
import { IdSubject } from '@libs/constants/abilities';
import { Item } from '@libs/entities/entities/Item';
import { LeaderboardResponse } from '../responses/leaderboard.repsonse';
import { ItemTransformer } from '@server/modules/items/transformers/item.transformer';
import { ProfileAuthorResponse } from '../responses/profile.response';

export class AuthorTransformer extends BaseResponseTransformer {
  static toAuthorResponse(author: Author): AuthorResponse {
    return {
      ...this.transformEntityTimestamps(author),
      avatar: author.avatar
        ? StoredFileTransformer.toStordFileResponse(author.avatar)
        : null,
      avatar_large: author.avatar_large
        ? StoredFileTransformer.toStordFileResponse(author.avatar_large)
        : null,
      __typename: IdSubject.Authors,
    };
  }

  static toLeaderboardResponse(data: {
    author: Author;
    items: Item[];
    total: number;
    total_view: number;
  }): LeaderboardResponse {
    return {
      author: AuthorTransformer.toAuthorResponse(data.author),
      items: data.items.map(ItemTransformer.toItemResponse),
      total: data.total,
      total_view: data.total_view,
    };
  }

  static toProfileResponse(data: {
    author: Author;
    total_view: number;
    total_item: number;
  }): ProfileAuthorResponse {
    return {
      ...AuthorTransformer.toAuthorResponse(data.author),
      total_view: data.total_view,
      total_item: data.total_item,
    };
  }
}
