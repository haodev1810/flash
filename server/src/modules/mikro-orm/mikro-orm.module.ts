import { Logger, Module, OnModuleInit } from '@nestjs/common';
import { MikroOrmModule as OrmModule } from '@mikro-orm/nestjs';
import config from '../../mikro-orm.config';
import { MikroORM } from '@mikro-orm/core';
import { DatabaseSeeder } from '@libs/entities/seeders/DatabaseSeeder';
import { User } from '@libs/entities/entities/User';

@Module({
  imports: [
    OrmModule.forRoot(config),
    OrmModule.forFeature({
      entities: [User],
    }),
  ],
  exports: [OrmModule],
})
export class MikroOrmModule implements OnModuleInit {
  private readonly logger = new Logger(MikroOrmModule.name);

  async onModuleInit() {
    this.logger.log('Starting migration...');
    const orm = await MikroORM.init(config);

    try {
      const seeder = orm.getSeeder();
      await seeder.seed(DatabaseSeeder);
    } catch (e) {
      this.logger.error(e);
    }

    await orm.close(true);
    this.logger.log('Migration ended.');
  }
}
