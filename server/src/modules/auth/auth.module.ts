import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { Module } from '@nestjs/common';
import { GoogleController } from './google/google.controller';
import { GoogleService } from './google/google.service';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { AccountModule } from '../account/account.module';
import { AccountsModule } from '../accounts/accounts.module';
import { AbilityModule } from './ability/ability.module';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { User } from '@libs/entities/entities/User';
import { Role } from '@libs/entities/entities/Role';
import { UploadsModule } from '../uploads/uploads.module';
import { StoredFile } from '@libs/entities/entities/StoredFile';

@Module({
  imports: [
    AbilityModule,
    PassportModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET_KEY,
      signOptions: { expiresIn: '1d' },
    }),
    AccountModule,
    AccountsModule,
    MikroOrmModule.forFeature([User, Role, StoredFile]),
    UploadsModule,
  ],
  controllers: [AuthController, GoogleController],
  providers: [AuthService, GoogleService, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
