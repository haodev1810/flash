import { User } from '@libs/entities/entities/User';
import { EntityManager } from '@mikro-orm/postgresql';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AuthService {
  constructor(private readonly em: EntityManager) {}
  async getLoggedUser(id: User['id']): Promise<User> {
    const userRepository = this.em.getRepository(User);
    return await userRepository.findOneOrFail(
      {
        id,
        deleted_at: { $eq: null },
      },
      { populate: ['role', 'avatar'] }
    );
  }
}
