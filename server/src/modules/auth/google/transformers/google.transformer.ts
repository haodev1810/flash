import { BaseResponseTransformer } from '@server/transformers/base-response.transformer';
import { GoogleLoginResponse } from '../responses/google-login.response';

export class GoogleTransformer extends BaseResponseTransformer {
  static toGoogleLoginResponse(googleLoginResponse: {
    token: string;
  }): GoogleLoginResponse {
    return {
      ...googleLoginResponse,
    };
  }
}
