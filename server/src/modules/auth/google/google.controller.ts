import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { GoogleService } from './google.service';
import { GoogleLoginDto } from './dtos/google-login.dto';
import { OAuth2Client } from 'google-auth-library';
import { AppApiSuccessResponse } from '@libs/utils/responses';
import { GoogleTransformer } from './transformers/google.transformer';
import { Public } from '../guards/public.guard';
import { ApiOperation } from '@nestjs/swagger';
import { AppSwaggerTag } from '@server/modules/app-swagger/app-swagger.constant';
import { GoogleLoginResponse } from './responses/google-login.response';
import { ApiErrorResponse, ApiSuccessResponse } from '@libs/utils/swagger';
import { AbilitiesGuard } from '../ability/ability.guard';

@UseGuards(AbilitiesGuard)
@Controller('auth/google')
export class GoogleController {
  constructor(private readonly googleService: GoogleService) {}

  @Post()
  @ApiOperation({ tags: [AppSwaggerTag.AuthGoogle] })
  @ApiSuccessResponse(GoogleLoginResponse)
  @ApiErrorResponse()
  @Public()
  async login(@Body() loginDto: GoogleLoginDto) {
    const client = new OAuth2Client(process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID);
    const ticket = await client.verifyIdToken({
      idToken: loginDto.credential,
      audience: process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID,
    });
    const payload = ticket.getPayload();
    return AppApiSuccessResponse.create(
      GoogleTransformer.toGoogleLoginResponse(
        await this.googleService.login(payload)
      )
    );
  }
}
