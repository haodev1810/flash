import { User } from '@libs/entities/entities/User';
import { EntityManager } from '@mikro-orm/postgresql';
import { Injectable, Scope, UnauthorizedException } from '@nestjs/common';
import { TokenPayload } from 'google-auth-library';
import { JwtService } from '@nestjs/jwt';
import { Role } from '@libs/entities/entities/Role';
import { RoleType } from '@libs/constants/entities/Role';
import { Folder } from '@server/modules/uploads/folder.config';
import { StoredFileDriver } from '@libs/constants/entities/StoredFile';
import { UploadsService } from '../../uploads/uploads.service';

@Injectable({ scope: Scope.REQUEST })
export class GoogleService {
  constructor(
    private readonly em: EntityManager,
    private jwtService: JwtService,
    private readonly uploadService: UploadsService
  ) {}
  async login(payload: TokenPayload) {
    const userRepository = this.em.getRepository(User);
    const roleRepository = this.em.getRepository(Role);
    const role_user = await roleRepository.findOne({ type: RoleType.USER });
    let user = await userRepository.findOne({
      email: payload['email'],
    });
    await this.em.begin();
    try {
      if (!user) {
        const avatar = await this.uploadService.uploadRemoteFile(
          payload['picture'],
          payload.name,
          { folderPath: Folder.User },
          StoredFileDriver.Cloudinary
        );
        user = userRepository.create({
          name: payload['name'],
          email: payload['email'],
          google_id: payload['sub'],
          avatar_id: avatar.id,
          role_id: role_user.id,
        });
        await this.em.persistAndFlush(user);
      } else {
        user.google_id = payload['sub'];
        if (!user.avatar_id) {
          const avatar = await this.uploadService.uploadRemoteFile(
            payload['picture'],
            payload.name,
            { folderPath: Folder.User },
            StoredFileDriver.Cloudinary
          );
          user.avatar_id = avatar.id;
        }
        await this.em.persistAndFlush(user);
      }
      await this.em.commit();
    } catch (error) {
      await this.em.rollback();
      throw new UnauthorizedException();
    }
    const token = await this.jwtService.signAsync({ id: user.id });
    return { token };
  }
}
