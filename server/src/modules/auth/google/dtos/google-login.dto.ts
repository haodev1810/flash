import { ApiProperty } from '@nestjs/swagger';
import { BaseDto } from '@server/validators/base.dto';
import { IsDefined, IsOptional } from 'class-validator';

export class GoogleLoginDto extends BaseDto {
  @IsOptional()
  @ApiProperty({ required: true })
  @IsDefined()
  credential: string;
}
