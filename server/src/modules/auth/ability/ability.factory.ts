import { Ability, AbilityBuilder, AbilityClass } from '@casl/ability';
import { Injectable } from '@nestjs/common';
import { IdAction, IdSubject } from '@libs/constants/abilities';
import { RoleType } from '@libs/constants/entities/Role';
import { User } from '@libs/entities/entities/User';
import { Role } from '@libs/entities/entities/Role';
import { Collections } from '@libs/entities/entities/Collection';
import { Item } from '@libs/entities/entities/Item';

export type AppAbility = Ability<
  [IdAction, IdSubject | User | Collections | Item]
>;

@Injectable()
export class AbilityFactory {
  defineAbility(account: User) {
    const { can, cannot, build } = new AbilityBuilder(
      Ability as AbilityClass<AppAbility>
    );
    switch (account.role.type) {
      case RoleType.ADMIN: {
        can(IdAction.Manage, IdSubject.All);
        break;
      }
      case RoleType.USER: {
        can(IdAction.Read, IdSubject.All);
        can(IdAction.Insert, IdSubject.Collections);
        can(IdAction.Update, IdSubject.Collections, { owner_id: account.id });
        can(IdAction.Delete, IdSubject.Collections, { owner_id: account.id });
        can(IdAction.Push, IdSubject.Collections, { owner_id: account.id });
        can(IdAction.Pop, IdSubject.Collections, { owner_id: account.id });
        can(IdAction.Download, IdSubject.Collections, { owner_id: account.id });
        cannot(IdAction.Update, IdSubject.Users, ['email', 'name']);
        break;
      }
      default: {
        cannot(IdAction.Manage, IdSubject.All);
        break;
      }
    }
    return build({
      detectSubjectType: (item: unknown) => {
        switch (item.constructor?.name) {
          case User.name:
            return IdSubject.Users;
          case Role.name:
            return IdSubject.Roles;
          case Collections.name:
            return IdSubject.Collections;
          case Item.name:
            return IdSubject.Items;
        }
        return IdSubject.All;
      },
    });
  }
}
