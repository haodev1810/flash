import { Module } from '@nestjs/common';
import { AbilityFactory } from './ability.factory';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { User } from '@libs/entities/entities/User';
import { AbilitiesGuard } from './ability.guard';

@Module({
  imports: [MikroOrmModule.forFeature([User])],
  providers: [AbilityFactory, AbilitiesGuard],
  exports: [AbilityFactory, AbilitiesGuard],
})
export class AbilityModule {}
