/* eslint-disable no-unused-vars */
import { SetMetadata } from '@nestjs/common';

import { AppAbility } from './ability.factory';
import { IdAction, IdSubject } from '@libs/constants/abilities';

interface IPolicyHandler {
  handle(ability: AppAbility): boolean;
}

type PolicyHandlerCallback = (ability: AppAbility) => boolean;

export type PolicyHandler = IPolicyHandler | PolicyHandlerCallback;
export const CHECK_POLICIES_KEY = 'check_policy';
export const CheckPolicies = (...handlers: PolicyHandler[]) =>
  SetMetadata(CHECK_POLICIES_KEY, handlers);

export class ManageAllPolicyHandler implements IPolicyHandler {
  handle(ability: AppAbility): boolean {
    return ability.can(IdAction.Manage, IdSubject.All);
  }
}

export class ManageCronPolicyHandler implements IPolicyHandler {
  handle(ability: AppAbility): boolean {
    return ability.can(IdAction.Crawl, IdSubject.All);
  }
}

export class InsertAccountPolicyHandler implements IPolicyHandler {
  handle(ability: AppAbility): boolean {
    return ability.can(IdAction.Insert, IdSubject.Users);
  }
}

export class UpdateAccountPolicyHandler implements IPolicyHandler {
  handle(ability: AppAbility): boolean {
    return ability.can(IdAction.Update, IdSubject.Users);
  }
}

export class DeleteAccountPolicyHandler implements IPolicyHandler {
  handle(ability: AppAbility): boolean {
    return ability.can(IdAction.Delete, IdSubject.Users);
  }
}

export class InsertCollectionPolicyHandler implements IPolicyHandler {
  handle(ability: AppAbility): boolean {
    return ability.can(IdAction.Insert, IdSubject.Collections);
  }
}
