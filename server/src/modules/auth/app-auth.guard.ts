import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { IS_PUBLIC_KEY } from '@server/modules/auth/guards/public.guard';
import { IS_INTERNAL_KEY } from './guards/internal.guard';
import { InternalSecretHeader } from '@libs/constants/auth';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private reflector: Reflector) {
    super();
  }

  canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    const userAgent = request.headers['user-agent'];
    const isDevice =
      userAgent.includes('Mozilla') || userAgent.includes('Chrome');
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (isPublic) {
      return true;
    }
    const isInternal = this.reflector.getAllAndOverride<boolean>(
      IS_INTERNAL_KEY,
      [context.getHandler(), context.getClass()]
    );
    if (isInternal) {
      const internalSecret = request.headers?.[InternalSecretHeader];
      if (internalSecret === process.env.INTERNAL_SECRET && isDevice) {
        return true;
      }
    }
    return isDevice && super.canActivate(context);
  }

  handleRequest(err, user) {
    if (err || !user) {
      throw err || new UnauthorizedException();
    }
    return user;
  }
}
