import { ApiProperty } from '@nestjs/swagger';
import { IdSubject } from '@libs/constants/abilities';

export class BaseResponse {
  @ApiProperty()
  created_at: string;

  @ApiProperty()
  updated_at: string;

  // @ApiProperty({ nullable: true })
  // creator: null;

  // @ApiProperty({ nullable: true })
  // updater: null;
  // @ApiProperty({ type: AccountBasicResponse, nullable: true })
  // creator: AccountBasicResponse | null;

  // @ApiProperty({ type: AccountBasicResponse, nullable: true })
  // updater: AccountBasicResponse | null;

  @ApiProperty({ enum: IdSubject, required: false, nullable: true })
  __typename?: IdSubject;
}
