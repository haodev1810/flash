import { Logger, ValidationPipe, ValidationError } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app/app.module';
import { useContainer } from 'class-validator';
import { AppSwaggerModule } from './modules/app-swagger/app-swagger.module';
import { ValidationException } from './exceptions/validation.exception';
import { MikroORM, RequestContext } from '@mikro-orm/core';
import { InternalSecretHeader } from '@libs/constants/auth';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const orm = app.get(MikroORM);
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  app.enableCors({
    credentials: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    allowedHeaders: `Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, ${InternalSecretHeader}`,
    origin: process.env.NEXT_PUBLIC_BASE_URL,
  });
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      validationError: {
        target: false,
        value: false,
      },
      exceptionFactory: (errors: ValidationError[]) => {
        throw new ValidationException(errors);
      },
      whitelist: true,
    })
  );
  app.use((req, res, next) => {
    RequestContext.create(orm.em, next);
  });
  AppSwaggerModule.setup(app);
  const port = process.env.SERVER_PORT || 3333;
  app.enableShutdownHooks();
  await app.listen(port);
  Logger.log(
    `🚀 Application is running on: http://localhost:${port}/${globalPrefix}`
  );
}

bootstrap();
