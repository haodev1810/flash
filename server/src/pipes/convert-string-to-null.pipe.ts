import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class ConvertStringToNullPipe implements PipeTransform {
  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    return this.convertStringToNull(value);
  }

  private toValidate(metatype: any): boolean {
    return ![String, Array, Object].includes(metatype);
  }

  private convertStringToNull(value: any) {
    if (value === null || value === undefined) return value;
    if (typeof value === 'string') {
      if (value === '') return null;
      return value;
    }
    if (Array.isArray(value)) {
      return value.map((v) => this.convertStringToNull(v));
    } else if (typeof value === 'object') {
      for (const key of Object.keys(value)) {
        value[key] = this.convertStringToNull(value[key]);
      }
    }
    return value;
  }
}
