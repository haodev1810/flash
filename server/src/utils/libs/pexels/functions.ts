import { StatusJob } from '@libs/constants/jobs';
import axios, { AxiosRequestConfig } from 'axios';
import Bull from 'bull';

export interface CreateAxiosInstanceOptions {
  config?: AxiosRequestConfig;
}

export const createAxiosInstance = (options?: CreateAxiosInstanceOptions) => {
  return axios.create(options?.config);
};

export const axiosInstance = createAxiosInstance({
  config: {
    baseURL: process.env.PEXEL_API_END_POINT,
    withCredentials: true,
    headers: {
      Authorization: 'Bearer ' + process.env.PEXEL_BEARER_TOKEN,
      'Secret-Key': process.env.PEXEL_SECRET_KEY,
    },
  },
});

export const getStatusQueueJob = (status: StatusJob) => {
  let response: Bull.JobStatusClean | null;
  switch (status) {
    case StatusJob.Failed: {
      response = 'failed';
      break;
    }
    case StatusJob.Pending: {
      response = 'wait';
      break;
    }
    case StatusJob.Success: {
      response = 'completed';
      break;
    }
    case StatusJob.Working: {
      response = 'active';
      break;
    }
    default: {
      response = null;
    }
  }
  return response;
};
