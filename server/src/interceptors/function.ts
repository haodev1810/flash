import { Request } from 'express';
import { REQUEST_CONTEXT } from '@server/interceptors/request-user.interceptor';

const deepAssign = (object: any, key: string, value: any) => {
  const keys = Object.keys(object);
  for (const k of keys) {
    if (Array.isArray(object[k])) {
      for (const child of object[k]) {
        deepAssign(child, key, value);
      }
    } else if (object[k] && typeof object[k] === 'object') {
      object[k][key] = value;
    }
  }
  if (object && typeof object === 'object' && !Array.isArray(object)) {
    object[key] = value;
  }
};

export const assignRequestContext = (
  request: Request,
  type?: NonNullable<'query' | 'body' | 'param'>
) => {
  if (type && request[type]) {
    deepAssign(request[type], REQUEST_CONTEXT, {
      userId: request.user?.id,
      params: request.params,
    });
  }
};
