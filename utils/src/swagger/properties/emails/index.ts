import { ApiProperty, ApiPropertyOptions } from '@nestjs/swagger';
import { applyDecorators } from '@nestjs/common';

export const EmailProperty = (
  options: ApiPropertyOptions = {
    example: 'user@gmail.com',
  }
) => {
  return applyDecorators(
    ApiProperty({
      type: 'string',
      ...options,
    })
  );
};

export const EmailZinzaProperty = (
  options: ApiPropertyOptions = {
    example: 'user@zinza.com.vn',
    description: 'Email must be end with @zinza.com.vn',
  }
) => EmailProperty(options);
