import * as crypto from 'crypto';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const axios = require('axios');

export const getFileHash = (length = 16) => {
  return crypto
    .randomBytes(length)
    .toString('base64')
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=/g, '');
};

export const downloadFileFromUrl = async (url: string): Promise<Buffer> => {
  const response = await axios.get(url, {
    responseType: 'arraybuffer',
  });
  return Buffer.from(response.data, 'binary');
};
