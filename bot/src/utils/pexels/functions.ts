import axios, { AxiosRequestConfig } from 'axios';

export interface CreateAxiosInstanceOptions {
  config?: AxiosRequestConfig;
}

export const createAxiosInstance = (options?: CreateAxiosInstanceOptions) => {
  return axios.create(options?.config);
};

export const axiosInstance = createAxiosInstance({
  config: {
    baseURL: process.env.PEXEL_API_END_POINT,
    withCredentials: true,
    headers: {
      Authorization: 'Bearer ' + process.env.PEXEL_BEARER_TOKEN,
      'Secret-Key': process.env.PEXEL_SECRET_KEY,
    },
  },
});
