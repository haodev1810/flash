import { BadRequestException } from '@nestjs/common';
import { Injectable } from '@nestjs/common';
import { v2 as cloudinary } from 'cloudinary';
import { CloudinaryResponse } from './responses/cloudinary.response';
import streamifier from 'streamifier';
import { downloadFileFromUrl } from '@libs/utils/files';
import { Options } from '../uploads.interface';
import { StoredFile } from '@libs/entities/entities/StoredFile';
import { EntityRepository } from '@mikro-orm/postgresql';
import { InjectRepository } from '@mikro-orm/nestjs';
import { StoredFileDriver } from '@libs/constants/entities/StoredFile';

@Injectable()
export class CloudinaryService {
  constructor(
    @InjectRepository(StoredFile)
    private readonly storedFileRepository: EntityRepository<StoredFile>
  ) {}
  async uploadFile(file: Express.Multer.File): Promise<CloudinaryResponse> {
    return new Promise<CloudinaryResponse>((resolve, reject) => {
      const uploadStream = cloudinary.uploader.upload_stream(
        (error, result) => {
          if (error) return reject(error);
          resolve(result);
        }
      );

      streamifier.createReadStream(file.buffer).pipe(uploadStream);
    });
  }

  async uploadFileRemotUrl(
    url: string,
    name: string,
    options: Options
  ): Promise<StoredFile> {
    try {
      const file = await downloadFileFromUrl(url);
      const result = await new Promise<CloudinaryResponse>(
        (resolve, reject) => {
          const uploadStream = cloudinary.uploader.upload_stream(
            { folder: options.folderPath },
            (error, result) => {
              if (error) return reject(error);
              resolve(result);
            }
          );
          streamifier.createReadStream(file).pipe(uploadStream);
        }
      );
      if (result) {
        const storedFile = this.storedFileRepository.create({
          name: name,
          hash: result.public_id,
          path: result.url,
          key: result.public_id,
          driver: StoredFileDriver.Cloudinary,
        });
        await this.storedFileRepository.persistAndFlush(storedFile);
        return storedFile;
      }
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error);
    }
  }
}
