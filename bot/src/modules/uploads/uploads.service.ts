import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/postgresql';
import * as Minio from 'minio';
import 'multer';
import { StoredFile } from '@libs/entities/entities/StoredFile';
import { Options } from './uploads.interface';
import { MinioService } from './minio/minio.service';
import { CloudinaryService } from './cloudinary/cloudinary.service';
import { StoredFileDriver } from '@libs/constants/entities/StoredFile';

@Injectable()
export class UploadsService {
  constructor(
    @InjectRepository(StoredFile)
    private readonly storedFileRepository: EntityRepository<StoredFile>,
    private readonly minioService: MinioService,
    private readonly cloudinaryService: CloudinaryService
  ) {}

  async uploadRemoteFile(
    url: string,
    name: string,
    options: Options,
    driver: StoredFileDriver
  ): Promise<StoredFile> {
    switch (driver) {
      case StoredFileDriver.Minio: {
        return await this.minioService.uploadRemoteFile(url, name, options);
        break;
      }
      case StoredFileDriver.Cloudinary: {
        return await this.cloudinaryService.uploadFileRemotUrl(
          url,
          name,
          options
        );
        break;
      }
      case StoredFileDriver.Tiktok: {
        break;
      }
      case StoredFileDriver.Telegram: {
        break;
      }
      default: {
        throw new BadRequestException('Driver not working');
      }
    }
  }
}
