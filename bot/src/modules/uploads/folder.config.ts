export enum Folder {
  Large = 'large',
  Avatar = 'avatar',
  Medium = 'medium',
  Small = 'small',
  Author = 'authors',
  User = 'users',
}
