import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/postgresql';
import * as Minio from 'minio';
import 'multer';
import { StoredFile } from '@libs/entities/entities/StoredFile';
import { downloadFileFromUrl, getFileHash } from '@libs/utils/files';
import { Folder } from '../folder.config';
import { Options } from '../uploads.interface';
import { StoredFileDriver } from '@libs/constants/entities/StoredFile';

@Injectable()
export class MinioService {
  constructor(
    @InjectRepository(StoredFile)
    private readonly storedFileRepository: EntityRepository<StoredFile>
  ) {}

  private getClient(): Minio.Client {
    return new Minio.Client({
      endPoint: process.env.FLASH_MINIO_ENDPOINT,
      port: parseInt(process.env.FLASH_MINIO_API_PORT),
      useSSL: process.env.FLASH_MINIO_PROTOCOL === 'https',
      accessKey: process.env.FLASH_MINIO_SECRET_KEY,
      secretKey: process.env.FLASH_MINIO_SECRETKEY,
    });
  }

  async removeFile(storedFile: StoredFile): Promise<void> {
    const client = this.getClient();
    await client.removeObject(storedFile.key, storedFile.hash);
    await this.storedFileRepository.removeAndFlush(storedFile);
  }

  async uploadAvatar(url: string, name: string, options: Options = {}) {
    const file = await downloadFileFromUrl(url);
    const client = this.getClient();
    const hash = getFileHash();
    const key = `${Folder.Avatar}/${hash}.png`;
    await client.putObject(process.env.FLASH_MINIO_BUCKET, key, file);
    const filePath = `${process.env.FLASH_MINIO_BUCKET}/${key}`;
    const storedFile = this.storedFileRepository.create({
      name: name,
      path: filePath,
      hash: hash,
      key: key,
    });
    await this.storedFileRepository.persistAndFlush(storedFile);
    return storedFile;
  }

  async uploadRemoteFile(url: string, name: string, options: Options = {}) {
    const file = await downloadFileFromUrl(url);
    const client = this.getClient();
    const hash = getFileHash();
    const key = `${options.folderPath}/${hash}.jpg`;
    await client.putObject(process.env.FLASH_MINIO_BUCKET, key, file);
    const filePath = `${process.env.FLASH_MINIO_BUCKET}/${key}`;
    const storedFile = this.storedFileRepository.create({
      name: name,
      path: filePath,
      hash: hash,
      key: key,
      driver: StoredFileDriver.Minio,
    });
    await this.storedFileRepository.persistAndFlush(storedFile);
    return storedFile;
  }
}
