import { BadRequestException, Injectable } from '@nestjs/common';
import { telegramConst } from '@libs/constants/telegram/const';
import { InjectRepository } from '@mikro-orm/nestjs';
import {
  EntityManager,
  EntityRepository,
  Options,
} from '@mikro-orm/postgresql';
import { StoredFile } from '@libs/entities/entities/StoredFile';
import { downloadFileFromUrl } from '@libs/utils/files';
import FormData from 'form-data';
import axios from 'axios';
import { TelegramUploadPhotoResponse } from './responses/photo.response';
import { TelegramUploadVideoResponse } from './responses/video.response';
import { StoredFileDriver } from '@libs/constants/entities/StoredFile';
import { FileTeleResponse } from './responses/file.response';
import { Video } from '@libs/entities/entities/Video';

@Injectable()
export class TelegramService {
  constructor(
    @InjectRepository(StoredFile)
    private readonly storedFileRepository: EntityRepository<StoredFile>,
    @InjectRepository(Video)
    private readonly videoRepository: EntityRepository<Video>,
    private readonly em: EntityManager
  ) {}

  // async uploadFileRemotUrl(url: string, name: string, options?: Options) {
  //   try {
  //     const file = await downloadFileFromUrl(url);
  //     const formData = new FormData();
  //     const apiUpload = `${telegramConst.baseUrl}/bot${telegramConst.token}/sendPhoto`;
  //     const apiGetfile = `${telegramConst.baseUrl}/bot${telegramConst.token}/getFile`;
  //     formData.append('chat_id', telegramConst.chat);
  //     formData.append('photo', Buffer.from(file), { filename: name });
  //     const response = await axios
  //       .post<TelegramUploadPhotoResponse>(apiUpload, formData, {
  //         headers: {
  //           'Content-Type': 'multipart/form-data',
  //           ...formData.getHeaders(),
  //         },
  //       })
  //       .then((res) => res.data);
  //     if (response) {
  //       const images = await Promise.all(
  //         response.result.photo.map((image) => {
  //           return axios
  //             .get(apiGetfile, {
  //               params: {
  //                 file_id: image.file_id,
  //               },
  //             })
  //             .then((res) => res.data);
  //         })
  //       );
  //     }
  //   } catch (error) {
  //     throw new BadRequestException(error);
  //   }
  // }

  async uploadVideoRemoteUrl(
    url: string,
    name: string,
    option?: Options
  ): Promise<Video> {
    await this.em.begin();
    try {
      const file = await downloadFileFromUrl(url);
      const formData = new FormData();
      const apiUpload = `${telegramConst.baseUrl}/bot${telegramConst.token}/sendVideo`;
      const apiGetfile = `${telegramConst.baseUrl}/bot${telegramConst.token}/getFile`;
      formData.append('chat_id', telegramConst.chat);
      formData.append('video', file, { filename: name });
      const response = await axios
        .post<TelegramUploadVideoResponse>(apiUpload, formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
            ...formData.getHeaders(),
          },
        })
        .then((res) => res.data);
      if (response) {
        const videoResponse = response.result.video;
        const [videoPath, thumbPath] = await Promise.all(
          [videoResponse.file_id, videoResponse.thumb.file_id].map(
            (file_id) => {
              return axios
                .get<FileTeleResponse>(apiGetfile, {
                  params: {
                    file_id: file_id,
                  },
                })
                .then((res) => res.data);
            }
          )
        );
        const file = this.storedFileRepository.create({
          name: name,
          key: videoPath.result.file_unique_id,
          hash: videoPath.result.file_id,
          path: videoPath.result.file_path,
          driver: StoredFileDriver.Telegram,
        });
        await this.storedFileRepository.persistAndFlush(file);

        const thumb = this.storedFileRepository.create({
          name: name,
          key: thumbPath.result.file_unique_id,
          hash: thumbPath.result.file_id,
          path: thumbPath.result.file_path,
          driver: StoredFileDriver.Telegram,
        });
        await this.storedFileRepository.persistAndFlush(thumb);
        const video = this.videoRepository.create({
          file_id: file.id,
          thumbnail: thumb.id,
          file_size: videoPath.result.file_size,
          get_file: videoPath.result.file_id,
        });
        await this.videoRepository.persistAndFlush(video);
        await this.em.commit();
        return video;
      }
    } catch (error) {
      await this.em.rollback();
      console.log(error);
    }
  }
}
