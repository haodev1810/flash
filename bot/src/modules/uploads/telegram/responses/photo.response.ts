export class FromResponse {
  id: number;
  is_bot: boolean;
  first_name: string;
  username: string;
}

export class ChatResponse {
  id: number;
  title: string;
  type: string;
}

export class PhotoResponse {
  file_id: string;
  file_unique_id: string;
  file_size: number;
  width: number;
  height: number;
}

export class ResultResponse {
  message_id: number;
  from: FromResponse;
  chat: ChatResponse;
  date: number;
  photo: PhotoResponse[];
}

export class TelegramUploadPhotoResponse {
  ok: boolean;
  result: ResultResponse;
}
