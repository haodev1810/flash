import { InjectRepository } from '@mikro-orm/nestjs';
import { Injectable, Logger } from '@nestjs/common';
import { Author } from '@libs/entities/entities/Author';
import { Images } from '@libs/entities/entities/Images';
import { Item } from '@libs/entities/entities/Item';
import { Tag } from '@libs/entities/entities/Tag';
import { TagItem } from '@libs/entities/entities/TagItem';
import { User } from '@libs/entities/entities/User';
import { EntityManager, EntityRepository } from '@mikro-orm/postgresql';
import { UploadsService } from '@bot/modules/uploads/uploads.service';
import { Folder } from '@bot/modules/uploads/folder.config';
import { QueueJobDataPexel } from '@libs/constants/queue';
import { axiosInstance } from '@bot/utils/pexels';
import { StoredFile } from '@libs/entities/entities/StoredFile';
import { StoredFileDriver } from '@libs/constants/entities/StoredFile';
import { TopicItem } from '@libs/entities/entities/TopicItem';
import { Topic } from '@libs/entities/entities/Topic';
import slugify from 'slugify';
import { TypeItem } from '@libs/constants/entities/Item';
import {
  CrawlPexelVideoResponse,
  PexelFileVideoResponse,
  TagPexelResponse,
  UserPexelResponse,
} from './interfaces/video-pexel.response';
import { TelegramService } from '@bot/modules/uploads/telegram/telegram.service';
import {
  CrawlPexelImageResponse,
  ImagePexelResponse,
  PexelFileImageResponse,
} from './interfaces/image-pexel.response';
import { getFileHash } from '@libs/utils/files';

@Injectable()
export class PexelsService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: EntityRepository<User>,
    @InjectRepository(Author)
    private readonly authorRepository: EntityRepository<Author>,
    @InjectRepository(Tag)
    private readonly tagRepository: EntityRepository<Tag>,
    @InjectRepository(Item)
    private readonly itemRepository: EntityRepository<Item>,
    @InjectRepository(Images)
    private readonly imagesRepository: EntityRepository<Images>,
    @InjectRepository(StoredFile)
    private readonly storedFileRepository: EntityRepository<StoredFile>,
    @InjectRepository(TagItem)
    private readonly tagItemRepository: EntityRepository<TagItem>,
    @InjectRepository(TopicItem)
    private readonly topicItemRepository: EntityRepository<TopicItem>,
    @InjectRepository(Topic)
    private readonly topicRepository: EntityRepository<Topic>,
    private readonly uploadsService: UploadsService,
    private readonly telegramService: TelegramService,
    private readonly em: EntityManager
  ) {}
  private readonly logger = new Logger(PexelsService.name);

  async cron(data: QueueJobDataPexel) {
    if (data.type == TypeItem.IMAGE) {
      return await this.cronItemImageForPage(data);
    }
    if (data.type == TypeItem.VIDEO) {
      return await this.cronItemVideos(data);
    }
  }

  async cronItemImageForPage(data: QueueJobDataPexel) {
    const { page, limit, topic, driver } = data;
    const baseUrl = process.env.PEXEL_API_END_POINT;
    try {
      const response = await axiosInstance
        .get<CrawlPexelImageResponse>(baseUrl, {
          params: {
            page: page,
            per_page: limit,
            query: topic,
            orientation: 'all',
            size: 'all',
            color: 'all',
            seo_tags: true,
          },
        })
        .then((res) => res.data);
      const items = response.data;
      const exitTopic = await this.topicRepository.findOne({
        slug: slugify(topic, { lower: true, locale: 'vi' }),
      });
      for (const item of items) {
        const result = await this.cronItemImage(item, driver);
        if (exitTopic) {
          let topic_item = await this.topicItemRepository.findOne({
            topic_id: exitTopic.id,
            item_id: result.id,
          });
          if (!topic_item) {
            topic_item = this.topicItemRepository.create({
              topic: exitTopic,
              item: result,
            });
            await this.topicItemRepository.persistAndFlush(topic_item);
          }
        }
      }
    } catch (error) {
      console.error(error);
    }
  }

  async cronItemImage(
    item: PexelFileImageResponse,
    driver: StoredFileDriver
  ): Promise<Item> {
    const { attributes } = item;
    const { user, tags, image, ...rest } = attributes;
    let exitItem = await this.itemRepository.findOne(
      { slug: rest.slug },
      { fields: ['id', 'slug'] }
    );
    if (!exitItem) {
      try {
        const author = await this.saveAuthor(user, driver);
        const listTags = await this.saveTags(tags);
        const images = await this.saveImages(image, rest.slug, driver);
        exitItem = this.itemRepository.create({
          title: rest.title || rest.slug,
          slug: rest.slug,
          alt: rest.alt || rest.slug,
          description: rest.description,
          aspect_ratio: rest.aspect_ratio,
          author_id: author.id,
          image_id: images.id,
          publish_at: rest.publish_at,
          license: rest.license,
          public: rest.published,
          tags: listTags.map((i) => i.id),
          type: TypeItem.VIDEO,
        });
        await this.itemRepository.persistAndFlush(exitItem);
        return exitItem;
      } catch (error) {
        console.error(error);
      }
    } else {
      return exitItem;
    }
  }

  async cronItemVideos(data: QueueJobDataPexel) {
    const baseUrl = process.env.PEXEL_API_END_POINT + '/videos';
    const limit = data.limit;
    const response = await axiosInstance
      .get<CrawlPexelVideoResponse>(baseUrl, {
        params: {
          page: data.page,
          per_page: limit,
          query: data.topic,
          orientation: 'all',
          size: 'all',
          color: 'all',
          seo_tags: true,
        },
      })
      .then((res) => res.data);
    for (const item of response.data) {
      const exitItem = await this.itemRepository.findOne({
        slug: item.attributes.slug,
      });
      if (exitItem) {
        continue;
      }
      await this.saveItemVideo(item);
    }
  }

  async saveItemVideo(item: PexelFileVideoResponse) {
    try {
      const author = await this.saveAuthor(
        item.attributes.user,
        StoredFileDriver.Cloudinary
      );
      const tags = await this.saveTags(item.attributes.tags);
      const video = await this.telegramService.uploadVideoRemoteUrl(
        item.attributes.video.preview_src,
        item.attributes.slug
      );
      const newItem = await this.itemRepository.create({
        title: item.attributes.title,
        slug: item.attributes.slug,
        type: TypeItem.VIDEO,
        license: item.attributes.license,
        aspect_ratio: item.attributes.aspect_ratio,
        publish_at: item.attributes.publish_at,
        public: item.attributes.published,
        alt: item.attributes.title,
        description: item.attributes.description,
        author_id: author.id,
        video_id: video.id,
        tags: tags.map((i) => i.id),
      });
      await this.itemRepository.persistAndFlush(newItem);
    } catch (error) {
      console.log(error);
    }
  }

  async saveAuthor(
    user: UserPexelResponse,
    driver: StoredFileDriver
  ): Promise<Author> {
    try {
      let author = await this.authorRepository.findOne({
        username: user.slug,
      });
      if (author) {
        return author;
      }
      const name = [user.first_name, user.last_name].join('');
      const avatar = await this.uploadsService.uploadRemoteFile(
        user.avatar.small,
        name,
        { folderPath: Folder.Author },
        driver
      );
      author = this.authorRepository.create({
        name: name,
        username: user.slug,
        avatar_id: avatar.id,
        avatar_large_id: avatar.id,
      });
      await this.authorRepository.persistAndFlush(author);
      return author;
    } catch (error) {
      console.log(error);
    }
  }

  async saveTags(tags: TagPexelResponse[]) {
    const promises = tags.map(async (tag) => {
      let exitTag = await this.tagRepository.findOne({
        search_term: tag.search_term,
      });
      if (!exitTag) {
        exitTag = this.tagRepository.create({
          name: tag.name,
          search_term: tag.search_term,
        });
        await this.tagRepository.persistAndFlush(exitTag);
      }
      return exitTag;
    });
    const listTags = await Promise.all(promises);
    return listTags;
  }

  async saveImages(
    image: ImagePexelResponse,
    slug: string,
    driver: StoredFileDriver
  ) {
    await this.em.begin();
    try {
      const [small, medium, large] = await Promise.all([
        this.uploadsService.uploadRemoteFile(
          image.small,
          slug + '-small',
          { folderPath: Folder.Small },
          driver
        ),
        this.uploadsService.uploadRemoteFile(
          image.medium,
          slug + '-meidum',
          { folderPath: Folder.Medium },
          driver
        ),
        this.uploadsService.uploadRemoteFile(
          image.large,
          slug + '-large',
          { folderPath: Folder.Large },
          driver
        ),
      ]);
      const origin = this.storedFileRepository.create({
        name: slug + '-origin',
        hash: getFileHash(),
        path: image.large.split('?')[0] + '?date=' + Date.now().toString(),
        key: getFileHash(),
        driver: StoredFileDriver.Remote,
      });
      await this.storedFileRepository.persistAndFlush(origin);
      const images = this.imagesRepository.create({
        small_id: small.id,
        medium: medium.id,
        large_id: large.id,
        origin_id: origin.id,
      });
      await this.imagesRepository.persistAndFlush(images);
      await this.em.commit();
      return images;
    } catch (error) {
      await this.em.rollback();
      console.error(error);
    }
  }
}
