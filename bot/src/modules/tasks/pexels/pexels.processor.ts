import { Process, Processor } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';
import {
  QueueJobDataPexel,
  QueueJobName,
  QueueName,
} from '@libs/constants/queue';
import { PexelsService } from './pexels.service';

@Processor(QueueName.BotCron)
export class PexelProcessor {
  constructor(private readonly pexelsService: PexelsService) {}
  private readonly logger = new Logger(PexelProcessor.name);

  @Process({
    name: QueueJobName.CronPexel,
    concurrency: 1,
  })
  async handleJobCrawlStory(job: Job<QueueJobDataPexel>) {
    this.logger.debug(
      'Start crawling... ' + job.data.topic + '-page-' + job.data.page
    );
    await this.pexelsService.cron(job.data);
    this.logger.debug(
      'crawling completed ' + job.data.topic + '-page-' + job.data.page
    );
  }
}
