import { MikroOrmModule } from '@mikro-orm/nestjs';
import { TasksService } from './tasks.service';
import { Module } from '@nestjs/common';
import { User } from '@libs/entities/entities/User';
import { Author } from '@libs/entities/entities/Author';
import { Tag } from '@libs/entities/entities/Tag';
import { Item } from '@libs/entities/entities/Item';
import { Images } from '@libs/entities/entities/Images';
import { TagItem } from '@libs/entities/entities/TagItem';
import { UploadsModule } from '../uploads/uploads.module';
import { UploadsService } from '../uploads/uploads.service';
import { StoredFile } from '@libs/entities/entities/StoredFile';
import { BullModule } from '@nestjs/bull';
import { QueueName } from '@libs/constants/queue';
import { PexelsService } from './pexels/pexels.service';
import { PexelProcessor } from './pexels/pexels.processor';
import { Topic } from '@libs/entities/entities/Topic';
import { TopicItem } from '@libs/entities/entities/TopicItem';
import { Video } from '@libs/entities/entities/Video';

@Module({
  imports: [
    MikroOrmModule.forFeature([
      User,
      Author,
      Tag,
      Item,
      Images,
      TagItem,
      StoredFile,
      Topic,
      TopicItem,
      Video,
    ]),
    UploadsModule,
    BullModule.registerQueue({
      name: QueueName.BotCron,
    }),
  ],
  controllers: [],
  providers: [TasksService, UploadsService, PexelsService, PexelProcessor],
})
export class TasksModule {}
