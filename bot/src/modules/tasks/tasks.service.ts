import { Author } from '@libs/entities/entities/Author';
import { Images } from '@libs/entities/entities/Images';
import { Item } from '@libs/entities/entities/Item';
import { Tag } from '@libs/entities/entities/Tag';
import { TagItem } from '@libs/entities/entities/TagItem';
import { User } from '@libs/entities/entities/User';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityManager, EntityRepository } from '@mikro-orm/postgresql';
import { Injectable } from '@nestjs/common';
import { UploadsService } from '../uploads/uploads.service';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: EntityRepository<User>,
    @InjectRepository(Author)
    private readonly authorRepository: EntityRepository<Author>,
    @InjectRepository(Tag)
    private readonly tagRepository: EntityRepository<Tag>,
    @InjectRepository(Item)
    private readonly itemRepository: EntityRepository<Item>,
    @InjectRepository(Images)
    private readonly imagesRepository: EntityRepository<Images>,
    @InjectRepository(TagItem)
    private readonly tagItemRepository: EntityRepository<TagItem>,
    private readonly uploadsService: UploadsService,
    private readonly em: EntityManager
  ) {}

  // @Cron('0 * * * * *', { name: 'cron-image-pexels' })
  // async handleCronPexelsImage() {
  //   const limit = 24;
  //   const countItem = await this.itemRepository.count();
  //   const currentPage = Math.ceil(countItem / limit) + 1;
  //   const baseUrl = process.env.PEXEL_API_END_POINT;
  //   try {
  //     const response = await axiosInstance
  //       .get<DataResponse>(baseUrl, {
  //         params: {
  //           page: currentPage,
  //           per_page: limit,
  //           query: 'animals',
  //           orientation: 'all',
  //           size: 'all',
  //           color: 'all',
  //           seo_tags: true,
  //         },
  //       })
  //       .then((res) => res.data);
  //     const data = response.data;
  //     for (const item of data) {
  //       await this.cronItem(item as unknown as ItemResponse);
  //     }
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }

  // async cronItem(item: ItemResponse) {
  //   const { attributes } = item;
  //   const { user, tags, image, ...rest } = attributes;
  //   const exitItem = await this.itemRepository.findOne(
  //     { slug: rest.slug },
  //     { fields: ['id', 'slug'] }
  //   );
  //   if (!exitItem) {
  //     await this.em.begin();
  //     try {
  //       let author = await this.authorRepository.findOne({
  //         username: user.slug,
  //       });
  //       if (!author) {
  //         const name = [user.first_name, user.last_name].join(' ');
  //         console.log(name);
  //         throw Error();
  //         const avatar = await this.uploadsService.uploadRemoteFile(
  //           user.avatar.small,
  //           [name, 'small'].join(' '),
  //           { folderPath: MinioFolder.Author },
  //           this.em
  //         );
  //         const largeAvatar = await this.uploadsService.uploadRemoteFile(
  //           user.avatar.small,
  //           [name, 'large'].join(' '),
  //           { folderPath: MinioFolder.Author },
  //           this.em
  //         );
  //         author = this.authorRepository.create({
  //           name: name,
  //           username: user.slug,
  //           avatar_id: avatar.id,
  //           avatar_large_id: largeAvatar.id,
  //         });
  //         await this.authorRepository.persistAndFlush(author);
  //       }
  //       if (image) {
  //         const slug = rest.slug;
  //         const small = await this.uploadsService.uploadRemoteFile(
  //           image.small,
  //           slug + '-small',
  //           { folderPath: MinioFolder.Small },
  //           this.em
  //         );
  //         const medium = await this.uploadsService.uploadRemoteFile(
  //           image.medium,
  //           slug + '-medium',
  //           { folderPath: MinioFolder.Medium },
  //           this.em
  //         );
  //         const large = await this.uploadsService.uploadRemoteFile(
  //           image.large,
  //           slug + '-large',
  //           { folderPath: MinioFolder.Large },
  //           this.em
  //         );
  //         const images = this.imagesRepository.create({
  //           small_id: small.id,
  //           large_id: large.id,
  //           medium: medium.id,
  //         });
  //         await this.imagesRepository.persistAndFlush(images);
  //         const listTags: Tag[] = [];
  //         for (const tag of tags) {
  //           let exitTag = await this.tagRepository.findOne({
  //             search_term: tag.search_term,
  //           });
  //           if (!exitTag) {
  //             exitTag = this.tagRepository.create({
  //               name: tag.name,
  //               search_term: tag.search_term,
  //             });
  //             await this.tagRepository.persistAndFlush(exitTag);
  //           }
  //           listTags.push(exitTag);
  //         }
  //         const item = this.itemRepository.create({
  //           title: rest.title,
  //           slug: rest.slug,
  //           alt: rest.alt,
  //           description: rest.description,
  //           aspect_ratio: rest.aspect_ratio,
  //           author_id: author.id,
  //           image_id: images.id,
  //           publish_at: rest.publish_at,
  //           license: rest.license,
  //           public: rest.published,
  //           tags: listTags.map((i) => i.id),
  //         });
  //         await this.itemRepository.persistAndFlush(item);
  //       }
  //       await this.em.commit();
  //     } catch (error) {
  //       console.log(error);
  //       await this.em.rollback();
  //     }
  //   }
  // }
}
