export interface User {
  id: number;
  first_name: string;
  last_name: string;
  slug: string;
  avatar: {
    small: string;
    medium: string;
  };
}
export interface TagRespone {
  name: string;
  search_term: string;
}
interface ImageResponse {
  small: string;
  medium: string;
  large: string;
}
export interface Attributes {
  id: number;
  slug: string;
  description: string;
  publish_at: string;
  title: string;
  aspect_ratio: number;
  license: string;
  published: boolean;
  user: User;
  tags: TagRespone[];
  image: ImageResponse;
  alt: string;
}

export interface ItemResponse {
  id: string;
  type: string;
  attributes: Attributes;
}
interface PaginateResponse {
  current_page: number;
  total_pages: number;
  total_results: number;
}
export interface DataResponse {
  data: ImageResponse[];
  pagination: PaginateResponse;
}
