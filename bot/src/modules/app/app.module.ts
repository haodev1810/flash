import { QueuePrefix } from '@libs/constants/queue';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MikroOrmModule } from '../mikro-orm/mikro-orm.module';
import { ScheduleModule } from '@nestjs/schedule';
import { TasksModule } from '../tasks/tasks.module';
import { UploadsModule } from '../uploads/uploads.module';
import { BullModule } from '@nestjs/bull';

@Module({
  imports: [
    BullModule.forRoot({
      redis: {
        host: process.env.REDIS_HOST,
        port: Number(process.env.FLASH_REDIS_PORT),
        password: process.env.FLASH_REDIS_PASSWORD,
      },
      prefix: QueuePrefix.Queue,
    }),
    MikroOrmModule,
    ScheduleModule.forRoot(),
    TasksModule,
    UploadsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
