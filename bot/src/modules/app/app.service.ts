import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  async getData(): Promise<{ message: string }> {
    await fetch(
      'http://localhost:3333/api/items?page=1&limit=26&query=game&image=all'
    );
    return { message: 'Hello API' };
  }
}
