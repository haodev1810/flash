export enum FilterImage {
  All = 'all',
  Small = 'small',
  Medium = 'medium',
  Large = 'large',
}

export enum FilterSort {
  DESC = 'DESC',
  ASC = 'ASC',
}
