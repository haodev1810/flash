export enum StoredFileDriver {
  Minio = 'minio',
  Cloudinary = 'cloudinary',
  Telegram = 'telegram',
  Tiktok = 'tiktok',
  Remote = 'remote',
}
