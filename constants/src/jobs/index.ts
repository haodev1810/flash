export enum StatusJob {
  Pending = 1,
  Success = 2,
  Failed = 3,
  Working = 4,
}
