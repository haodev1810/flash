/* eslint-disable no-unused-vars */
export enum IdAction {
  Manage = 'manage',
  Insert = 'insert',
  Read = 'read',
  Update = 'update',
  Import = 'import',
  Delete = 'delete',
  Export = 'export',
  Crawl = 'crawl',
  Push = 'push',
  Pop = 'pop',
  Download = 'download',
}

export enum IdSubject {
  All = 'all',
  Users = 'users',
  Roles = 'roles',
  StoredFiles = 'stored_files',
  Crawl = 'crawls',
  Images = 'images',
  Items = 'items',
  Authors = 'authors',
  Tags = 'tags',
  Collections = 'collections',
  Topic = 'topics',
}
