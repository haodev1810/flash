export const JwtCookieToken =
  process.env['NEXT_PUBLIC_ACCESS_TOKEN'] || 'access_token_dev';
export const InternalSecretHeader = 'x-internal-secret';

export enum LoginQueryParam {
  redirectUrl = 'redirect_url',
  clientId = 'client_id',
  state = 'state',
}
