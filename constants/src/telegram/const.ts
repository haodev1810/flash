export const telegramConst = {
  baseUrl: String(process.env['BASE_API_TELEGRAM']),
  host: String(process.env['NEXT_PUBLIC_API_TELEGRAM']),
  chat: String(process.env['NEXT_PUBLIC_CHAT_GROUP_TELE_ID']),
  token: String(process.env['NEXT_PUBLIC_TELEGRAM_BOT_TOKEN']),
};
