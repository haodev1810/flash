import { TypeItem } from '../entities/Item';
import { StoredFileDriver } from '../entities/StoredFile';

export enum QueuePrefix {
  Queue = 'queue',
}

export enum QueueName {
  BotCron = 'BotCron',
}

export enum QueueJobName {
  CronPexel = 'cron_pexel',
}

export interface QueueJobDataPexel {
  topic: string;
  driver: StoredFileDriver;
  page: number;
  limit: number;
  type: TypeItem;
}
