import { MigrationWithTimestamps } from '../config/migration-with-timestamps';

export class Migration20240110100842 extends MigrationWithTimestamps {
  override async up(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.createTable('items', (tableBuilder) => {
      this.addSerialPrimaryColumn(tableBuilder);
      tableBuilder.string('title', 255).notNullable().index();
      tableBuilder.string('slug', 255).notNullable().unique();
      tableBuilder.string('alt', 255).notNullable();
      tableBuilder.text('description').nullable();
      tableBuilder.integer('views').defaultTo(0).nullable();
      tableBuilder.boolean('public').defaultTo(true).nullable();
      tableBuilder.float('aspect_ratio').nullable().defaultTo(1);
      tableBuilder.string('license', 255).nullable();
      tableBuilder
        .integer('author_id')
        .index()
        .nullable()
        .references('id')
        .inTable('authors');
      tableBuilder
        .integer('image_id')
        .index()
        .nullable()
        .references('id')
        .inTable('images');
      tableBuilder.string('publish_at', 30).nullable();
      this.addTimestampColumns(tableBuilder);
      this.addSoftDeleteColumns(tableBuilder);
      this.addActorColumns(tableBuilder);
    });
  }

  override async down(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.dropTable('items');
  }
}
