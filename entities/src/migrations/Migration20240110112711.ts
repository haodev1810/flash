import { MigrationWithTimestamps } from '../config/migration-with-timestamps';

export class Migration20240110112711 extends MigrationWithTimestamps {
  override async up(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.createTable('tag_items', (tableBuilder) => {
      tableBuilder
        .integer('tag_id')
        .notNullable()
        .index()
        .references('id')
        .inTable('tags');
      tableBuilder
        .integer('item_id')
        .notNullable()
        .index()
        .references('id')
        .inTable('items');
      tableBuilder.primary(['tag_id', 'item_id']);
      this.addActorColumns(tableBuilder);
      this.addTimestampColumns(tableBuilder);
      this.addSoftDeleteColumns(tableBuilder);
    });
  }

  override async down(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.dropTable('tag_items');
  }
}
