import { MigrationWithTimestamps } from '../config/migration-with-timestamps';
import { StoredFileDriver } from '@libs/constants/entities/StoredFile';

export class Migration20240219164658 extends MigrationWithTimestamps {
  override async up(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.alterTable('stored_files', (tableBuilder) => {
      tableBuilder
        .string('driver', 255)
        .notNullable()
        .defaultTo(StoredFileDriver.Minio);
    });
  }

  override async down(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.alterTable('stored_files', (tableBuilder) => {
      tableBuilder.dropColumn('driver');
    });
  }
}
