import { MigrationWithTimestamps } from '../config/migration-with-timestamps';
import { TypeItem } from '@libs/constants/entities/Item';

export class Migration20240222180935 extends MigrationWithTimestamps {
  override async up(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.alterTable('items', (tableBuilder) => {
      tableBuilder.string('type', 20).defaultTo(TypeItem.IMAGE);
      tableBuilder
        .integer('video_id')
        .nullable()
        .references('id')
        .inTable('videos');
    });
  }

  override async down(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.alterTable('items', (tableBuilder) => {
      tableBuilder.dropColumn('type');
      tableBuilder.dropColumn('video_id');
    });
  }
}
