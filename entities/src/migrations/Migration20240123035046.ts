import { MigrationWithTimestamps } from '../config/migration-with-timestamps';

export class Migration20240123035046 extends MigrationWithTimestamps {
  override async up(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.createTable('collections', (tableBuilder) => {
      this.addSerialPrimaryColumn(tableBuilder);
      tableBuilder.string('title', 255).notNullable().index();
      tableBuilder.string('slug', 255).notNullable();
      tableBuilder.integer('views').nullable().unsigned().defaultTo(0);
      tableBuilder.integer('downloads').nullable().unsigned().defaultTo(0);
      tableBuilder
        .integer('owner_id')
        .index()
        .references('id')
        .inTable('users')
        .notNullable();
      tableBuilder
        .integer('main_id')
        .index()
        .references('id')
        .inTable('items')
        .nullable();
      tableBuilder
        .integer('sub_id')
        .index()
        .references('id')
        .inTable('items')
        .nullable();
      tableBuilder
        .integer('sub2_id')
        .index()
        .references('id')
        .inTable('items')
        .nullable();
      tableBuilder.boolean('public').notNullable().defaultTo(false);
      tableBuilder.boolean('enable_download').notNullable().defaultTo(false);
      tableBuilder.text('description').nullable();
      this.addActorColumns(tableBuilder);
      this.addTimestampColumns(tableBuilder);
      this.addSoftDeleteColumns(tableBuilder);
      this.createUniqueIndex({
        tableName: 'collections',
        column: 'slug',
        hasSoftDelete: true,
      });
    });
  }

  override async down(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.dropTable('collections');
  }
}
