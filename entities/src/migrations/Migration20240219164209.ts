import { MigrationWithTimestamps } from '../config/migration-with-timestamps';

export class Migration20240219164209 extends MigrationWithTimestamps {
  override async up(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.createTable('topics', (tableBuilder) => {
      this.addSerialPrimaryColumn(tableBuilder);
      tableBuilder.string('title', 255).notNullable();
      tableBuilder.string('slug', 255).notNullable();
      this.addTimestampColumns(tableBuilder);
      this.addSoftDeleteColumns(tableBuilder);
      this.addActorColumns(tableBuilder);
    });
    await this.createUniqueIndex({ tableName: 'topics', column: 'slug' });
    await knex.schema.createTable('topic_items', (tableBuilder) => {
      tableBuilder
        .integer('topic_id')
        .notNullable()
        .index()
        .references('id')
        .inTable('topics');
      tableBuilder
        .integer('item_id')
        .notNullable()
        .index()
        .references('id')
        .inTable('items');
      tableBuilder.primary(['topic_id', 'item_id']);
      this.addActorColumns(tableBuilder);
      this.addTimestampColumns(tableBuilder);
      this.addSoftDeleteColumns(tableBuilder);
    });
  }

  override async down(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.dropTable('topic_items');
    await knex.schema.dropTable('topics');
  }
}
