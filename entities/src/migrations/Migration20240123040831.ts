import { MigrationWithTimestamps } from '../config/migration-with-timestamps';

export class Migration20240123040831 extends MigrationWithTimestamps {
  override async up(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.createTable('collection_items', (tableBuilder) => {
      tableBuilder
        .integer('collection_id')
        .notNullable()
        .index()
        .references('id')
        .inTable('collections');
      tableBuilder
        .integer('item_id')
        .notNullable()
        .index()
        .references('id')
        .inTable('items');
      tableBuilder.primary(['collection_id', 'item_id']);
      this.addActorColumns(tableBuilder);
      this.addTimestampColumns(tableBuilder);
      this.addSoftDeleteColumns(tableBuilder);
    });
  }

  override async down(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.dropTable('collection_items');
  }
}
