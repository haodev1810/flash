import { MigrationWithTimestamps } from '../config/migration-with-timestamps';

export class Migration20240110095616 extends MigrationWithTimestamps {
  override async up(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.createTable('images', (tableBuilder) => {
      this.addSerialPrimaryColumn(tableBuilder);
      tableBuilder
        .uuid('small_id')
        .index()
        .nullable()
        .references('id')
        .inTable('stored_files');
      tableBuilder
        .uuid('medium_id')
        .index()
        .nullable()
        .references('id')
        .inTable('stored_files');
      tableBuilder
        .uuid('large_id')
        .index()
        .nullable()
        .references('id')
        .inTable('stored_files');
      tableBuilder
        .uuid('origin_id')
        .index()
        .nullable()
        .references('id')
        .inTable('stored_files');
      this.addTimestampColumns(tableBuilder);
      this.addSoftDeleteColumns(tableBuilder);
      this.addActorColumns(tableBuilder);
    });
  }

  override async down(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.dropTable('images');
  }
}
