import { MigrationWithTimestamps } from '../config/migration-with-timestamps';

export class Migration20240114084843 extends MigrationWithTimestamps {
  override async up(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.alterTable('items', (tableBuilder) => {
      tableBuilder.integer('downloads').nullable().defaultTo(0);
    });
  }

  override async down(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.alterTable('items', (tableBuilder) => {
      tableBuilder.dropColumn('downloads');
    });
  }
}
