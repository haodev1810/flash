import { MigrationWithTimestamps } from '../config/migration-with-timestamps';

export class Migration20240110093648 extends MigrationWithTimestamps {
  override async up(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.createTable('authors', (tableBuilder) => {
      this.addSerialPrimaryColumn(tableBuilder);
      tableBuilder.string('name', 255).notNullable();
      tableBuilder.string('username', 255).notNullable().unique();
      tableBuilder
        .uuid('avatar_id')
        .index()
        .nullable()
        .references('id')
        .inTable('stored_files');
      tableBuilder
        .uuid('avatar_large_id')
        .index()
        .nullable()
        .references('id')
        .inTable('stored_files');
      this.addActorColumns(tableBuilder);
      this.addTimestampColumns(tableBuilder);
      this.addSoftDeleteColumns(tableBuilder);
    });
  }

  override async down(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.dropTable('authors');
  }
}
