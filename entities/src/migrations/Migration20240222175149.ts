import { MigrationWithTimestamps } from '../config/migration-with-timestamps';

export class Migration20240222175149 extends MigrationWithTimestamps {
  override async up(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.createTable('videos', (tableBuilder) => {
      this.addSerialPrimaryColumn(tableBuilder);
      tableBuilder
        .uuid('thumbnail_id')
        .nullable()
        .references('id')
        .inTable('stored_files');
      tableBuilder
        .uuid('file_id')
        .nullable()
        .references('id')
        .inTable('stored_files');
      tableBuilder.integer('file_size').defaultTo(0),
        tableBuilder.string('get_file', 255);
      this.addTimestampColumns(tableBuilder);
      this.addSoftDeleteColumns(tableBuilder);
      this.addActorColumns(tableBuilder);
    });
  }

  override async down(): Promise<void> {
    const knex = this.getKnexBuilder();
    await knex.schema.dropTable('videos');
  }
}
