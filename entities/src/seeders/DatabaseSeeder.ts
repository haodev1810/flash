import type { EntityManager } from '@mikro-orm/core';
import { Seeder } from '@mikro-orm/seeder';
import { Role } from '../entities/Role';
import { RoleType } from '@libs/constants/entities/Role';
import { User } from '../entities/User';

export class DatabaseSeeder extends Seeder {
  async run(em: EntityManager): Promise<void> {
    const roleRepository = em.getRepository(Role);
    const adminRole = await roleRepository.upsert({
      name: 'Admin',
      type: RoleType.ADMIN,
    });
    await roleRepository.upsert({
      name: 'Người dùng',
      type: RoleType.USER,
    });
    await roleRepository.flush();
    const accountRepository = em.getRepository(User);
    const nameAdmin = process.env['NAME_ADMIN'];
    const emailAdmin = process.env['EMAIL_ADMIN'];
    if (emailAdmin && nameAdmin) {
      let adminUser = await accountRepository.findOne({
        email: process.env['EMAIL_ADMIN'] as string,
      });
      if (!adminUser) {
        adminUser = accountRepository.create(
          {
            name: nameAdmin,
            email: emailAdmin,
            role_id: adminRole.id,
          },
          {
            persist: false,
          }
        );
        await accountRepository.persistAndFlush(adminUser);
      }
    }
  }
}
