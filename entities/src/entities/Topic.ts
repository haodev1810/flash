import {
  Collection,
  Entity,
  ManyToMany,
  OneToMany,
  Property,
} from '@mikro-orm/core';
import { Scalar } from '@libs/constants/interfaces/scalar';
import { BaseEntityWithSerialPrimaryKey } from '@libs/entities/entities/BaseEntityWithSerialPrimaryKey';
import { Item } from './Item';
import { TopicItem } from './TopicItem';

@Entity({ tableName: 'topics' })
export class Topic extends BaseEntityWithSerialPrimaryKey<Topic, 'id'> {
  @Property({ persist: false })
  get __visible(): Array<keyof this> {
    return ['id', 'title', 'slug', 'created_at', 'updated_at', 'deleted_at'];
  }

  @Property({ type: 'varchar', length: 255 })
  title!: Scalar['varchar'];

  @Property({ type: 'varchar', length: 255, unique: true })
  slug!: Scalar['varchar'];

  @OneToMany({
    entity: () => TopicItem,
    mappedBy: (topic_item) => topic_item.topic,
  })
  topic_items = new Collection<TopicItem>(this);

  @ManyToMany({
    entity: () => Item,
    inversedBy: 'topics',
    pivotEntity: () => TopicItem,
    nullable: true,
  })
  items = new Collection<Item>(this);
}
