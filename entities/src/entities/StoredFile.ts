import {
  Collection,
  Entity,
  ManyToOne,
  OneToMany,
  Property,
  Unique,
} from '@mikro-orm/core';
import { Scalar } from '@libs/constants/interfaces/scalar';
import { BaseEntityWithUuidPrimaryKey } from '@libs/entities/entities/BaseEntityWithUuidPrimaryKey';
import { User } from './User';
import { Author } from './Author';
import { Images } from './Images';
import { StoredFileDriver } from '@libs/constants/entities/StoredFile';
import { Video } from './Video';

@Entity({ tableName: 'stored_files' })
export class StoredFile extends BaseEntityWithUuidPrimaryKey<StoredFile, 'id'> {
  @Property({ persist: false })
  get __visible(): Array<keyof this> {
    return [
      'id',
      'name',
      'hash',
      'path',
      'driver',
      'created_by',
      'updated_by',
      'created_at',
      'updated_at',
      'deleted_at',
    ];
  }

  @Property({ type: 'text' })
  name!: Scalar['text'];

  @Unique()
  @Property({ type: 'text' })
  hash!: Scalar['text'];

  @Property({ type: 'text' })
  path!: Scalar['text'];

  @Property({ type: 'text' })
  key!: Scalar['text'];

  @Property({ type: 'varchar', length: 255, default: StoredFileDriver.Minio })
  driver!: StoredFileDriver;

  @OneToMany({
    entity: () => User,
    mappedBy: (user) => user.avatar,
  })
  user_avatar = new Collection<User>(this);

  @OneToMany({
    entity: () => Author,
    mappedBy: (author) => author.avatar,
  })
  author_avatar = new Collection<Author>(this);

  @OneToMany({
    entity: () => Author,
    mappedBy: (author) => author.avatar_large,
  })
  author_large_avatar = new Collection<Author>(this);

  @OneToMany({
    entity: () => Images,
    mappedBy: (image) => image.small,
  })
  images_small = new Collection<Images>(this);

  @OneToMany({
    entity: () => Images,
    mappedBy: (image) => image.medium,
  })
  images_medium = new Collection<Images>(this);

  @OneToMany({
    entity: () => Images,
    mappedBy: (image) => image.large,
  })
  images_large = new Collection<Images>(this);

  @OneToMany({
    entity: () => Images,
    mappedBy: (image) => image.origin,
  })
  images_origin = new Collection<Images>(this);

  @OneToMany({
    entity: () => Video,
    mappedBy: (video) => video.file,
  })
  videos = new Collection<Video>(this);

  @OneToMany({
    entity: () => Video,
    mappedBy: (video) => video.thumbnail,
  })
  video_thumbnails = new Collection<Video>(this);

  @ManyToOne({
    entity: () => User,
    nullable: true,
    inversedBy: (user) => user.created_stored_files,
    joinColumn: 'created_by',
  })
  creator!: User;

  @ManyToOne({
    entity: () => User,
    nullable: true,
    inversedBy: (user) => user.updated_stored_files,
    joinColumn: 'updated_by',
  })
  updater!: User;
}
