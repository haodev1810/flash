import {
  Collection,
  Entity,
  ManyToOne,
  OneToMany,
  Property,
} from '@mikro-orm/core';
import { Maybe, Scalar } from '@libs/constants/interfaces/scalar';
import { BaseEntityWithSerialPrimaryKey } from '@libs/entities/entities/BaseEntityWithSerialPrimaryKey';
import { StoredFile } from './StoredFile';
import { Item } from './Item';

@Entity({ tableName: 'videos' })
export class Video extends BaseEntityWithSerialPrimaryKey<Video, 'id'> {
  @Property({ persist: false })
  get __visible(): Array<keyof this> {
    return [
      'id',
      'thumbnail_id',
      'file_id',
      'file_size',
      'get_file',
      'created_at',
      'updated_at',
      'deleted_at',
    ];
  }

  @Property({ type: 'uuid', nullable: true })
  file_id!: Maybe<'uuid'>;

  @Property({ type: 'integer', nullable: true })
  thumbnail_id!: Maybe<'integer'>;

  @Property({ type: 'integer', default: 0 })
  file_size!: Scalar['integer'];

  @Property({ type: 'varchar', length: 255 })
  get_file!: Scalar['varchar'];

  @ManyToOne({
    entity: () => StoredFile,
    nullable: true,
    inversedBy: (storedFile) => storedFile.video_thumbnails,
    joinColumn: 'thumbnail_id',
  })
  thumbnail!: StoredFile;

  @ManyToOne({
    entity: () => StoredFile,
    nullable: true,
    inversedBy: (storedFile) => storedFile.videos,
    joinColumn: 'file_id',
  })
  file!: StoredFile;

  @OneToMany({
    entity: () => Item,
    mappedBy: (item) => item.video,
  })
  items = new Collection<Item>(this);
}
