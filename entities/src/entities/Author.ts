import {
  Collection,
  Entity,
  ManyToOne,
  OneToMany,
  Property,
  Unique,
} from '@mikro-orm/core';
import { Maybe, Scalar } from '@libs/constants/interfaces/scalar';
import { BaseEntityWithSerialPrimaryKey } from '@libs/entities/entities/BaseEntityWithSerialPrimaryKey';
import { StoredFile } from './StoredFile';
import { Item } from './Item';

@Entity({ tableName: 'authors' })
export class Author extends BaseEntityWithSerialPrimaryKey<Author, 'id'> {
  @Property({ persist: false })
  get __visible(): Array<keyof this> {
    return [
      'id',
      'name',
      'username',
      'avatar_id',
      'avatar_large_id',
      'created_at',
      'updated_at',
      'deleted_at',
    ];
  }

  @Property({ type: 'varchar', length: 255 })
  name!: Scalar['varchar'];

  @Unique()
  @Property({ type: 'varchar', length: 255 })
  username!: Scalar['varchar'];

  @Property({ type: 'uuid', nullable: true })
  avatar_id!: Maybe<'uuid'>;

  @Property({ type: 'uuid', nullable: true })
  avatar_large_id!: Maybe<'uuid'>;

  @ManyToOne({
    entity: () => StoredFile,
    nullable: true,
    inversedBy: (storedFile) => storedFile.author_avatar,
    joinColumn: 'avatar_id',
  })
  avatar!: StoredFile;

  @ManyToOne({
    entity: () => StoredFile,
    nullable: true,
    inversedBy: (storedFile) => storedFile.author_avatar,
    joinColumn: 'avatar_large_id',
  })
  avatar_large!: StoredFile;

  @OneToMany({
    entity: () => Item,
    mappedBy: (item) => item.author,
  })
  items = new Collection<Item>(this);
}
