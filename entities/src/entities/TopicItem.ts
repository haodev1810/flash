import { Entity, ManyToOne, PrimaryKeyType, Property } from '@mikro-orm/core';
import { BaseEntityWithCompositeKeys } from '@libs/entities/entities/BaseEntityWithCompositeKeys';
import { Scalar } from '@libs/constants/interfaces/scalar';
import { Item } from './Item';
import { Topic } from './Topic';

@Entity({ tableName: 'topic_items' })
export class TopicItem extends BaseEntityWithCompositeKeys<
  TopicItem,
  'topic_id' | 'item_id'
> {
  @Property({ persist: false })
  get __visible(): Array<keyof this> {
    return ['topic_id', 'item_id', 'created_at', 'updated_at', 'deleted_at'];
  }

  @Property({ type: 'integer', nullable: true })
  topic_id!: Scalar['integer'];

  @Property({ type: 'integer', nullable: true })
  item_id!: Scalar['integer'];

  [PrimaryKeyType]?: [number, number];

  @ManyToOne({
    primary: true,
    entity: () => Topic,
    inversedBy: (topic) => topic.topic_items,
    nullable: true,
  })
  topic!: Topic;

  @ManyToOne({
    primary: true,
    entity: () => Item,
    inversedBy: (item) => item.topic_items,
    nullable: true,
  })
  item!: Item;
}
