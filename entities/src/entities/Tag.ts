import {
  Collection,
  Entity,
  ManyToMany,
  OneToMany,
  Property,
} from '@mikro-orm/core';
import { Scalar } from '@libs/constants/interfaces/scalar';
import { BaseEntityWithSerialPrimaryKey } from '@libs/entities/entities/BaseEntityWithSerialPrimaryKey';
import { TagItem } from './TagItem';
import { Item } from './Item';

@Entity({ tableName: 'tags' })
export class Tag extends BaseEntityWithSerialPrimaryKey<Tag, 'id'> {
  @Property({ persist: false })
  get __visible(): Array<keyof this> {
    return [
      'id',
      'name',
      'search_term',
      'created_at',
      'updated_at',
      'deleted_at',
    ];
  }

  @Property({ type: 'varchar', length: 255 })
  name!: Scalar['varchar'];

  @Property({ type: 'varchar', length: 255 })
  search_term!: Scalar['varchar'];

  @OneToMany({
    entity: () => TagItem,
    mappedBy: (tag_item) => tag_item.tag,
  })
  tag_items = new Collection<TagItem>(this);

  @ManyToMany({
    entity: () => Item,
    inversedBy: 'tags',
    pivotEntity: () => TagItem,
    nullable: true,
  })
  items = new Collection<Item>(this);
}
