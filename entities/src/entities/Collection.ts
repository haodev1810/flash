import {
  Collection,
  Entity,
  ManyToMany,
  ManyToOne,
  OneToMany,
  Property,
  Unique,
} from '@mikro-orm/core';
import { Maybe, Scalar } from '@libs/constants/interfaces/scalar';
import { BaseEntityWithSerialPrimaryKey } from '@libs/entities/entities/BaseEntityWithSerialPrimaryKey';
import { User } from './User';
import { Item } from './Item';
import { CollectionItem } from './CollectionItem';

@Entity({ tableName: 'collections' })
export class Collections extends BaseEntityWithSerialPrimaryKey<
  Collections,
  'id'
> {
  @Property({ persist: false })
  get __visible(): Array<keyof this> {
    return [
      'id',
      'title',
      'slug',
      'description',
      'views',
      'public',
      'downloads',
      'enable_download',
      'owner_id',
      'main_id',
      'sub_id',
      'sub2_id',
      'created_at',
      'updated_at',
      'deleted_at',
    ];
  }

  @Property({ type: 'varchar', length: 255 })
  title!: Scalar['varchar'];

  @Unique()
  @Property({ type: 'varchar', length: 255 })
  slug!: Scalar['varchar'];

  @Property({ type: 'text', nullable: true })
  description!: Maybe<'text'>;

  @Property({ type: 'integer', nullable: true, default: 0 })
  views!: Maybe<'integer'>;

  @Property({ type: 'integer', nullable: true, default: 0 })
  downloads!: Maybe<'integer'>;

  @Property({ type: 'boolean', default: false })
  public!: Scalar['boolean'];

  @Property({ type: 'boolean', default: false })
  enable_download!: Scalar['boolean'];

  @Property({ type: 'integer', nullable: true })
  owner_id!: Maybe<'integer'>;

  @Property({ type: 'integer', nullable: true })
  main_id!: Maybe<'integer'>;

  @Property({ type: 'integer', nullable: true })
  sub_id!: Maybe<'integer'>;

  @Property({ type: 'integer', nullable: true })
  sub2_id!: Maybe<'integer'>;

  @ManyToOne({
    entity: () => User,
    nullable: true,
    inversedBy: (user) => user.collections,
    joinColumn: 'owner_id',
  })
  owner!: User;

  @ManyToOne({
    entity: () => Item,
    nullable: true,
    inversedBy: (item) => item.collection_mains,
    joinColumn: 'main_id',
  })
  main!: Item;

  @ManyToOne({
    entity: () => Item,
    nullable: true,
    inversedBy: (item) => item.collection_subs,
    joinColumn: 'sub_id',
  })
  sub!: Item;

  @ManyToOne({
    entity: () => Item,
    nullable: true,
    inversedBy: (item) => item.collection_sub2s,
    joinColumn: 'sub2_id',
  })
  sub2!: Item;

  @OneToMany({
    entity: () => CollectionItem,
    mappedBy: (collection_item) => collection_item.collection,
  })
  collection_items = new Collection<CollectionItem>(this);

  @ManyToMany({
    entity: () => Item,
    inversedBy: 'collections',
    pivotEntity: () => CollectionItem,
    nullable: true,
  })
  items = new Collection<Item>(this);
}
