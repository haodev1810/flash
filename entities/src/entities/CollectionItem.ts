import { Entity, ManyToOne, PrimaryKeyType, Property } from '@mikro-orm/core';
import { BaseEntityWithCompositeKeys } from '@libs/entities/entities/BaseEntityWithCompositeKeys';
import { Scalar } from '@libs/constants/interfaces/scalar';
import { Item } from './Item';
import { Collections } from './Collection';

@Entity({ tableName: 'collection_items' })
export class CollectionItem extends BaseEntityWithCompositeKeys<
  CollectionItem,
  'collection_id' | 'item_id'
> {
  @Property({ persist: false })
  get __visible(): Array<keyof this> {
    return [
      'collection_id',
      'item_id',
      'created_at',
      'updated_at',
      'deleted_at',
    ];
  }

  @Property({ type: 'integer', nullable: true })
  collection_id!: Scalar['integer'];

  @Property({ type: 'integer', nullable: true })
  item_id!: Scalar['integer'];

  [PrimaryKeyType]?: [number, number];

  @ManyToOne({
    primary: true,
    entity: () => Collections,
    inversedBy: (collection) => collection.collection_items,
    nullable: true,
  })
  collection!: Collections;

  @ManyToOne({
    primary: true,
    entity: () => Item,
    inversedBy: (item) => item.collection_items,
    nullable: true,
  })
  item!: Item;
}
