import {
  Collection,
  Entity,
  ManyToOne,
  OneToMany,
  Property,
} from '@mikro-orm/core';
import { Maybe } from '@libs/constants/interfaces/scalar';
import { BaseEntityWithSerialPrimaryKey } from '@libs/entities/entities/BaseEntityWithSerialPrimaryKey';
import { StoredFile } from './StoredFile';
import { Item } from './Item';
import { Video } from './Video';

@Entity({ tableName: 'images' })
export class Images extends BaseEntityWithSerialPrimaryKey<Images, 'id'> {
  @Property({ persist: false })
  get __visible(): Array<keyof this> {
    return [
      'id',
      'small_id',
      'medium_id',
      'large_id',
      'origin_id',
      'created_at',
      'updated_at',
      'deleted_at',
    ];
  }

  @Property({ type: 'uuid', nullable: true })
  small_id!: Maybe<'uuid'>;

  @Property({ type: 'uuid', nullable: true })
  medium_id!: Maybe<'uuid'>;

  @Property({ type: 'uuid', nullable: true })
  large_id!: Maybe<'uuid'>;

  @Property({ type: 'uuid', nullable: true })
  origin_id!: Maybe<'uuid'>;

  @ManyToOne({
    entity: () => StoredFile,
    nullable: true,
    inversedBy: (storedFile) => storedFile.images_small,
    joinColumn: 'small_id',
  })
  small!: StoredFile;

  @ManyToOne({
    entity: () => StoredFile,
    nullable: true,
    inversedBy: (storedFile) => storedFile.images_medium,
    joinColumn: 'medium_id',
  })
  medium!: StoredFile;

  @ManyToOne({
    entity: () => StoredFile,
    nullable: true,
    inversedBy: (storedFile) => storedFile.images_large,
    joinColumn: 'large_id',
  })
  large!: StoredFile;

  @ManyToOne({
    entity: () => StoredFile,
    nullable: true,
    inversedBy: (storedFile) => storedFile.images_origin,
    joinColumn: 'origin_id',
  })
  origin!: StoredFile;

  @OneToMany({
    entity: () => Item,
    mappedBy: (item) => item.images,
  })
  items = new Collection<Item>(this);
}
