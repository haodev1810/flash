import { Entity, ManyToOne, PrimaryKeyType, Property } from '@mikro-orm/core';
import { BaseEntityWithCompositeKeys } from '@libs/entities/entities/BaseEntityWithCompositeKeys';
import { Scalar } from '@libs/constants/interfaces/scalar';
import { Tag } from './Tag';
import { Item } from './Item';

@Entity({ tableName: 'tag_items' })
export class TagItem extends BaseEntityWithCompositeKeys<
  TagItem,
  'tag_id' | 'item_id'
> {
  @Property({ persist: false })
  get __visible(): Array<keyof this> {
    return ['tag_id', 'item_id', 'created_at', 'updated_at', 'deleted_at'];
  }

  @Property({ type: 'integer', nullable: true })
  tag_id!: Scalar['integer'];

  @Property({ type: 'integer', nullable: true })
  item_id!: Scalar['integer'];

  [PrimaryKeyType]?: [number, number];

  @ManyToOne({
    primary: true,
    entity: () => Tag,
    inversedBy: (tag) => tag.tag_items,
    nullable: true,
  })
  tag!: Tag;

  @ManyToOne({
    primary: true,
    entity: () => Item,
    inversedBy: (item) => item.tag_items,
    nullable: true,
  })
  item!: Item;
}
