import {
  Collection,
  Entity,
  ManyToOne,
  OneToMany,
  Property,
} from '@mikro-orm/core';
import { BaseEntityWithSerialPrimaryKey } from './BaseEntityWithSerialPrimaryKey';
import { Maybe, Scalar } from '@libs/constants/interfaces/scalar';
import { StoredFile } from './StoredFile';
import { Role } from './Role';
import { Collections } from './Collection';

@Entity({ tableName: 'users' })
export class User extends BaseEntityWithSerialPrimaryKey<User, 'id'> {
  @Property({ persist: false })
  get __visible(): Array<keyof this> {
    return [
      'id',
      'name',
      'google_id',
      'email',
      'role_id',
      'avatar_id',
      'created_by',
      'updated_by',
      'created_at',
      'updated_at',
      'deleted_at',
    ];
  }

  @Property({ type: 'varchar', length: 255 })
  name!: Scalar['varchar'];

  @Property({ type: 'varchar', length: 255 })
  email!: Scalar['varchar'];

  @Property({ type: 'varchar', length: 255, nullable: true })
  google_id!: Maybe<'varchar'>;

  @Property({ type: 'integer' })
  role_id!: Scalar['integer'];

  @Property({ type: 'uuid', nullable: true })
  avatar_id!: Maybe<'uuid'>;

  @ManyToOne({
    entity: () => User,
    nullable: true,
    inversedBy: (User) => User.created_accounts,
    joinColumn: 'created_by',
  })
  creator!: User;

  @ManyToOne({
    entity: () => User,
    nullable: true,
    inversedBy: (User) => User.updated_accounts,
    joinColumn: 'updated_by',
  })
  updater!: User;

  @ManyToOne({
    entity: () => Role,
    nullable: true,
    inversedBy: (role) => role.users,
    joinColumn: 'role_id',
  })
  role!: Role;

  @ManyToOne({
    entity: () => StoredFile,
    nullable: true,
    inversedBy: (storedFile) => storedFile.user_avatar,
    joinColumn: 'avatar_id',
  })
  avatar!: StoredFile;

  @OneToMany({
    entity: () => User,
    mappedBy: (User) => User.creator,
  })
  created_accounts = new Collection<User>(this);

  @OneToMany({
    entity: () => User,
    mappedBy: (User) => User.updater,
  })
  updated_accounts = new Collection<User>(this);

  @OneToMany({
    entity: () => StoredFile,
    mappedBy: (StoredFile) => StoredFile.creator,
  })
  created_stored_files = new Collection<StoredFile>(this);

  @OneToMany({
    entity: () => StoredFile,
    mappedBy: (StoredFile) => StoredFile.updater,
  })
  updated_stored_files = new Collection<StoredFile>(this);

  @OneToMany({
    entity: () => Collections,
    mappedBy: (collections) => collections.owner,
  })
  collections = new Collection<Collections>(this);
}
