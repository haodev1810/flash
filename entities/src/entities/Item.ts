import {
  Collection,
  Entity,
  ManyToMany,
  ManyToOne,
  OneToMany,
  Property,
  Unique,
} from '@mikro-orm/core';
import { Maybe, Scalar } from '@libs/constants/interfaces/scalar';
import { BaseEntityWithSerialPrimaryKey } from '@libs/entities/entities/BaseEntityWithSerialPrimaryKey';
import { Author } from './Author';
import { Images } from './Images';
import { TagItem } from './TagItem';
import { Tag } from './Tag';
import { Collections } from './Collection';
import { CollectionItem } from './CollectionItem';
import { TopicItem } from './TopicItem';
import { Topic } from './Topic';
import { TypeItem } from '@libs/constants/entities/Item';
import { Video } from './Video';

@Entity({ tableName: 'items' })
export class Item extends BaseEntityWithSerialPrimaryKey<Item, 'id'> {
  @Property({ persist: false })
  get __visible(): Array<keyof this> {
    return [
      'id',
      'title',
      'slug',
      'description',
      'views',
      'public',
      'downloads',
      'type',
      'aspect_ratio',
      'license',
      'publish_at',
      'author_id',
      'image_id',
      'video_id',
      'alt',
      'created_at',
      'updated_at',
      'deleted_at',
    ];
  }

  @Property({ type: 'varchar', length: 255 })
  title!: Scalar['varchar'];

  @Unique()
  @Property({ type: 'varchar', length: 255 })
  slug!: Scalar['varchar'];

  @Property({ type: 'varchar', length: 255 })
  alt!: Scalar['varchar'];

  @Property({ type: 'varchar', length: 20, default: TypeItem.IMAGE })
  type!: TypeItem;

  @Property({ type: 'text', nullable: true })
  description!: Maybe<'text'>;

  @Property({ type: 'integer', nullable: true, default: 0 })
  views!: Maybe<'integer'>;

  @Property({ type: 'integer', nullable: true, default: 0 })
  downloads!: Maybe<'integer'>;

  @Property({ type: 'boolean', nullable: true, default: true })
  public!: Maybe<'boolean'>;

  @Property({ type: 'float', nullable: true, default: 1 })
  aspect_ratio!: Maybe<'float'>;

  @Property({ type: 'varchar', length: 255, nullable: true })
  license!: Maybe<'varchar'>;

  @Property({ type: 'varchar', nullable: true, length: 30 })
  publish_at!: Maybe<'varchar'>;

  @Property({ type: 'integer', nullable: true })
  author_id!: Maybe<'integer'>;

  @Property({ type: 'integer', nullable: true })
  image_id!: Maybe<'integer'>;

  @Property({ type: 'integer', nullable: true })
  video_id!: Maybe<'integer'>;

  @ManyToOne({
    entity: () => Author,
    nullable: true,
    inversedBy: (author) => author.items,
    joinColumn: 'author_id',
  })
  author!: Author;

  @ManyToOne({
    entity: () => Images,
    nullable: true,
    inversedBy: (images) => images.items,
    joinColumn: 'image_id',
  })
  images!: Images;

  @OneToMany({
    entity: () => TagItem,
    mappedBy: (tag_item) => tag_item.item,
  })
  tag_items = new Collection<TagItem>(this);

  @ManyToMany({
    entity: () => Tag,
    mappedBy: 'items',
    pivotEntity: () => TagItem,
    nullable: true,
  })
  tags = new Collection<Tag>(this);

  @OneToMany({
    entity: () => Collections,
    mappedBy: (collections) => collections.main,
  })
  collection_mains = new Collection<Collections>(this);

  @OneToMany({
    entity: () => Collections,
    mappedBy: (collections) => collections.sub,
  })
  collection_subs = new Collection<Collections>(this);

  @OneToMany({
    entity: () => Collections,
    mappedBy: (collections) => collections.sub2,
  })
  collection_sub2s = new Collection<Collections>(this);

  @OneToMany({
    entity: () => CollectionItem,
    mappedBy: (tag_item) => tag_item.item,
  })
  collection_items = new Collection<CollectionItem>(this);

  @ManyToMany({
    entity: () => Collections,
    mappedBy: 'items',
    pivotEntity: () => CollectionItem,
    nullable: true,
  })
  collections = new Collection<Collections>(this);

  @OneToMany({
    entity: () => TopicItem,
    mappedBy: (topic_item) => topic_item.item,
  })
  topic_items = new Collection<TopicItem>(this);

  @ManyToMany({
    entity: () => Topic,
    mappedBy: 'items',
    pivotEntity: () => TopicItem,
    nullable: true,
  })
  topics = new Collection<Topic>(this);

  @ManyToOne({
    entity: () => Video,
    nullable: true,
    inversedBy: (video) => video.items,
    joinColumn: 'video_id',
  })
  video!: Video;
}
